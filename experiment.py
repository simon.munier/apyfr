import os
import numpy as np
import pandas as pd
from netCDF4 import Dataset, num2date
from datetime import datetime
from glob import glob
from .maps import map_applis
from .time_series import time_series_piezo, time_series_spli, time_series_spli_rea_ps, add_logo
from .misc import annee_mois, default_map_applis, available_kwargs, nargout, to_date
from .spli import *
from .piezometre import read_piezos


class Experiment(object):

    #===============================================================================

    def __init__(self,directory,options_file=None):
        '''
        Initialisation d'un instance Experiment
        '''
        self.directory = directory
        self.exp = os.path.basename(directory)
        self.options_file = options_file
        if options_file is None:
            f = glob(directory+'/options*.py')
            if len(f)>0: self.options_file = f[0]
            self.type_exp=None
        if self.options_file is not None:
            exec(open(self.options_file).read())
            self.applis = locals()['applis']
            self.exp = locals()['exp']
            self.type_exp = locals()['type_exp']
            if self.type_exp=='PSE':
              self.type_ps = locals()['type_ps']
              self.nb_ensemble = locals()['nb_ensemble']
              self.nb_days = locals()['nb_days']
            if 'outputs_day_nc' in locals(): self.outputs_day_nc = locals()['outputs_day_nc']
            if 'outputs_mon_nc' in locals(): self.outputs_mon_nc = locals()['outputs_mon_nc']
            if 'outputs_day_txt' in locals(): self.outputs_day_txt = locals()['outputs_day_txt']
            if 'outputs_mon_txt' in locals(): self.outputs_mon_txt = locals()['outputs_mon_txt']
        else:
            print('Warning: no options file found.')
        self.data = {}
        self.codes_piezos = {}


    #===============================================================================

    def concatenate(self,EXP_other,applis=None):
      '''
      Concaténation de deux instances Experiment
      '''
      if applis is None: applis = self.applis
      if isinstance(applis,str):
        applis = default_map_applis.copy() if applis=='def' else [applis]
      dat = pd.DatetimeIndex(np.unique(np.concatenate((self.dat,EXP_other.dat))))
      for var in self.data:
        for appli in applis:
          data  = pd.DataFrame(np.empty((len(dat),self.data[var][appli].shape[1])),index=dat)
          data1 = pd.DataFrame(self.data[var][appli],index=self.dat)
          data2 = pd.DataFrame(EXP_other.data[var][appli],index=EXP_other.dat)
          data.loc[data1.index] = data1.values
          data.loc[data2.index] = data2.values
          self.data[var][appli] = data.values
      self.dat = dat


    #===============================================================================

    def _get_timesteps(self,dat,ts=None,period=None):
      if period is not None:
        ts = np.where((dat>=to_date(period[0]))*(dat<=to_date(period[1])))[0]
      elif ts is None: ts = slice(0,len(dat))
      else:
        if isinstance(ts,str): ts = datetime.strptime(ts,'%Y-%m-%d')
        if isinstance(ts,datetime): ts = [i for i,t in enumerate(dat) if t==ts]
        if '__len__' not in dir(ts): ts = [ts]
      return ts

    def _read_results_file(self,res_file,var,ts=None,period=None,MV=np.nan):
      '''
      Lecture d'un fichier de résultats
      '''
      with Dataset(res_file) as nc:
        dat = num2date(nc['time'][:],nc['time'].units,only_use_cftime_datetimes=False)
        dat = pd.DatetimeIndex(dat)
        ts = self._get_timesteps(dat,ts,period)
        dat = dat[ts]
        if var in nc.variables.keys():
          data = nc[var][ts,:].filled(MV)
        else:
          print('Attention : Variable {} absente des résultats'.format(var))
          data = None
        codes_piezos = nc.variables['codes_piezos'][:] if "piezos" in var else None
      return dat,data,codes_piezos

    def read_results(self,var,dt='mon',ts=None,period=None,applis=None,file_suffix='',MV=np.nan):
      '''
      Lecture des résultats d'une expérience
      '''
      if applis is None: applis = self.applis
      if isinstance(applis,str):
        applis = default_map_applis.copy() if applis=='def' else [applis]
      data = {}
      codes_piezos = {}
      for appli in applis:
        if self.type_exp!='PSE':
          res_file = self.directory+'/'+appli+'_'+dt+'.nc'+file_suffix
          dat,data[appli],codes_piezos[appli] = self._read_results_file(res_file,var,ts=ts,period=period,MV=MV)
        else:
          for iens in range(self.nb_ensemble):
            sens = str(iens + (1993 if self.type_ps=='PCLIM' else 0) )
            res_file = self.directory+'/'+self.exp+'_'+sens+'/'+appli+'_'+dt+'.nc'+file_suffix
            dat,tmp,codes_piezos[appli] = self._read_results_file(res_file,var,ts=ts,period=period,MV=MV)
            if iens==0: data[appli] = np.zeros((self.nb_ensemble,)+tmp.shape)
            data[appli][iens] = tmp
      self.dat = dat
      self.data[var] = data
      if "piezos" in var: self.codes_piezos = codes_piezos
      return


    #===============================================================================

    def read_piezos(self,dt='mon',period=None,applis=None,pzo_list=None,**kwargs):
      '''
      Lecture des observations piézométriques
      '''
      if 'dat' not in dir(self):
        raise Exception('Warning: simulation results must be read before observations.')
      if period is None: period = self.dat[[0,-1]]
      if applis is None: applis = self.applis
      if isinstance(applis,str):
        applis = default_map_applis.copy() if applis=='def' else [applis]
      if pzo_list is None: pzo_list = self.codes_piezos
      if any(~np.isin(applis,list(pzo_list.keys()))):
        raise Exception('Some applications are not in pzo_list.')
      if 'obs' not in dir(self): self.obs = {}
      var = 'h_piezos'
      if var not in self.obs: self.obs[var] = {}
      for appli in applis:
        obs = read_piezos(pzo_list[appli],date_range=period,dt=dt,**kwargs).values
        self.obs[var][appli] = obs
      return


    #===============================================================================

    def compute_stats(self):
      '''
      Calcul des statistiques d'un expérience
      '''
      pass


    #===============================================================================

    def compute_spli(self,var,applis=None,obs=False,dir_cdf=None,
                     period_ref=['1990-08-01','2020-07-31'],parallel=True,**kwargs):
      '''
      Calcul des SPLI
      '''
      if 'h_' not in var:
        raise Exception('Computing SPLI is only available for piezometric level.')
      data = self.obs if obs else self.data
      if var not in data:
        raise Exception('Please read piezometric level before computing SPLI.')
      if applis is None: applis = self.applis
      if isinstance(applis,str):
        applis = default_map_applis.copy() if applis=='def' else [applis]
      if dir_cdf is not None and not os.path.exists(dir_cdf): os.makedirs(dir_cdf)
      if parallel:
        fun_cdf,fun_spli = compute_monthly_CDF_parallel,compute_monthly_SPLI_parallel
      else:
        fun_cdf,fun_spli = compute_monthly_CDF,compute_monthly_SPLI
      var_spli = var.replace('h_','spli_')
      if var_spli not in data: data[var_spli] = {}
      for app in applis:
        cdf,ncfile,compute_cdf = None,None,True
        if dir_cdf is not None:
          ncfile = dir_cdf+'/CDF_'+app+'.nc'
          if os.path.isfile(ncfile): compute_cdf = False
        if compute_cdf:
          kw_cdf = available_kwargs(fun_cdf,kwargs)
          cdf = fun_cdf(self.dat,data[var][app],period=period_ref,ncfile=ncfile,**kw_cdf)
        kw_spli = available_kwargs(fun_spli,kwargs)
        data[var_spli][app] = fun_spli(self.dat,data[var][app],cdf=cdf,ncfile=ncfile,**kw_spli)
      return


    #===============================================================================

    def compute_median(self,var,applis=None):
      '''
      Calcul de la médiane de la prévision d'ensemble
      '''
      if self.type_exp!='PSE':
        raise Exception('Computing ensemble median is only available for PSE experiments.')
      if applis is None: applis = list(self.data[var].keys())
      if isinstance(applis,str): applis = [applis]
      var_median = var+'_median'
      self.data[var_median] = {}
      for appli in applis:
        self.data[var_median][appli] = np.nanmedian(self.data[var][appli],axis=0)


    def compute_quantile(self,var,quantiles,applis=None):
      '''
      Calcul des quantiles de la prévision d'ensemble
      quantiles doit être compris entre 0 et 1, il peut être une valeur ou une liste de valeurs
      '''
      if self.type_exp!='PSE':
        raise Exception('Computing ensemble median is only available for PSE experiments.')
      if applis is None: applis = list(self.data[var].keys())
      if isinstance(applis,str): applis = [applis]
      try: quantiles[0]
      except: quantiles = [quantiles]
      for q in quantiles:
        var_quantile = var+'_Q{:.2f}'.format(q)
        self.data[var_quantile] = {}
        for appli in applis:
          #Inan = np.isfinite(self.data[var][appli])
          #self.data[var_quantile][appli] = np.nan*np.zeros(self.data[var][appli].shape[1:])
          #self.data[var_quantile][appli][Inan] = np.quantile(self.data[var][appli][Inan],q,axis=0)
          self.data[var_quantile][appli] = np.quantile(self.data[var][appli],q,axis=0)


    def compute_dr5(self,var,applis=None,frac=False):
      '''
      Calcul du DR5 (indice sécheresse) de la prévision d'ensemble
      '''
      if self.type_exp!='PSE':
        raise Exception('Computing DR5 threshold is only available for PSE experiments.')
      if applis is None: applis = list(self.data[var].keys())
      if isinstance(applis,str): applis = [applis]
      var_median,var_dr5 = var+'_median',var+'_dr5'
      if var_median not in self.data:
        self.compute_median(var,applis=applis)
      self.data[var_dr5] = {}
      for appli in applis:
        if frac:
          tmp = np.nan*np.zeros(self.data[var][appli].shape)
          tmp[self.data[var][appli]<=-0.84] = 1.
          tmp[self.data[var][appli]> -0.84] = 0.
          self.data[var_dr5][appli] = np.nanmean(tmp,axis=0)
        else:
          self.data[var_dr5][appli] = np.nan*np.zeros(self.data[var_median][appli].shape)
          self.data[var_dr5][appli][self.data[var_median][appli]<=-0.84] = 1.
          self.data[var_dr5][appli][self.data[var_median][appli]> -0.84] = 0.


    #===============================================================================

    def map_applis(self,var,ts=None,period=None,applis=None,title=None,**kwargs):
      '''
      Trace les cartes des sorties AquiFR
      '''
      if applis is None:
        applis = self.applis
      elif (isinstance(applis,str) and applis=='def'):
        applis = [app for app in default_map_applis if app in self.data[var].keys()]
      if isinstance(applis,str): applis = [applis]
      ts = self._get_timesteps(self.dat,ts,period)
      dat = self.dat[ts]
      data = {appli:self.data[var][appli][ts,:] for appli in applis}
      var = var.replace("_median", "").replace("_dr5","")
      ax = map_applis(data,var,title=self.exp,dat=dat,**kwargs)
      return ax if nargout()>0 else None


    #===============================================================================

    def time_series(self,var,period=None,applis=None,pzo_list=None,plot_obs=False,
                    bgcolor=True,logo=False,savedir=None,**kwargs):
      '''
      Trace les séries temporelles des sorties AquiFR
      '''
      if period is None: period = self.dat[[0,-1]]
      if applis is None: applis = self.applis
      if isinstance(applis,str):
        applis = default_map_applis.copy() if applis=='def' else [applis]
      if pzo_list is None: pzo_list = self.codes_piezos
      if any(~np.isin(applis,list(pzo_list.keys()))):
        raise Exception('Some applications are not in pzo_list.')
      #
      ts = self._get_timesteps(self.dat,period=period)
      for appli,codes_piezos in pzo_list.items():
        for pzo in codes_piezos:
          ipzo = np.where(self.codes_piezos[appli]==pzo)[0][0]
          filename = savedir+'/'+self.exp+'_'+pzo+'.png' if savedir else None
          if 'h_' in var:
            if bgcolor:
              if 'cdf_dir' not in dir(self):
                raise Exception('CDF directory not known (Experiment.cdf_dir).')
              with Dataset(self.cdf_dir+'CDF_'+appli+'.nc') as nc:
                cdf = [nc['min'][:,ipzo],nc['max'][:,ipzo],nc['cdf'][:,ipzo,:]]
            else:
              cdf = None
            ax = time_series_piezo(self.dat[ts],self.data[var][appli][:,ipzo],c='k',cdf=cdf,label='AquiFR',
                                   title=self.exp+' - '+pzo,filename=filename,**kwargs)
            if plot_obs:
              time_series_piezo(self.dat[ts],self.obs[var][appli][:,ipzo],c='g',ax=ax,label='observations',
                                title=self.exp+' - '+pzo,filename=filename,**kwargs)
          if 'spli_' in var:
            ax = time_series_spli(self.dat[ts],self.data[var][appli][:,ipzo],c='k',
                                  title=self.exp+' - '+pzo,filename=filename,**kwargs)


    def time_series_rea_ps(self,REA,var='spli_piezos',applis=None,pzo_list=None,#plot_obs=False,
                       bgcolor=True,savedir=None,**kwargs):
      '''
      Trace les séries temporelles combinant réanalyse et prévision saisonnière
      '''
      if self.type_exp!='PSE':
        raise Exception('Only PSE experiment can use time_series_ps.')
      if var!='spli_piezos':
        raise Exception('time_series_ps only works for spli_piezos.')
      if applis is None: applis = self.applis
      if isinstance(applis,str):
        applis = default_map_applis.copy() if applis=='def' else [applis]
      if pzo_list is None: pzo_list = self.codes_piezos
      if any(~np.isin(applis,list(pzo_list.keys()))):
        raise Exception('Some applications are not in pzo_list.')
      #
      dat = self.dat.union([REA.dat[-1]])
      for appli,codes_piezos in pzo_list.items():
        for pzo in codes_piezos:
          ipzo = np.where(self.codes_piezos[appli]==pzo)[0][0]
          data_last = np.repeat(REA.data[var][appli][-1].reshape((1,1,-1)),self.nb_ensemble,axis=0)
          data = np.concatenate((data_last,self.data[var][appli]),axis=1)
          #import pdb; pdb.set_trace()
          filename = savedir+'/'+self.exp+'_'+pzo+'.png' if savedir else None
          time_series_spli_rea_ps(REA.dat,REA.data[var][appli][:,ipzo],dat[:-1],data[:,:-1,ipzo].T,
                              title=self.exp+' - '+pzo,filename=filename,**kwargs)



    # -------------------------------
    # Plot spli
    #--------------------------------
    # SIM avec CDF sur 1990-2020        : "-" , color = "darkred" , linewidth = 2
    # SIM avec CDF sur période acceptée : "--", color = "darkred" , linewidth = 1
    # OBS avec CDF sur 1990-2020        : "-" , color = "darkblue", linewidth = 2
    # OBS avec CDF sur période acceptée : "--", color = "darkblue", linewidth = 1

    #piezo_code = str(piezos_codes[i]).replace('/','_')
    #nom_piezo = piezo_code
    #if piezo_code in dict_names.keys():
    #    nom_piezo = dict_names[piezo_code]
    #init = ""
    #if not nomSimul == "REA_TR":
    #    init = "  initialisation : " + anneemois
    #title = "application : "+appli.replace("_","-")+"    piézomètre : "+nom_piezo + \
    #            "\nsimulation " + titreSimul + init

    #if "Marthe" in label[k]: color = "gold"
    #elif "EauDyssée" in label[k]: color = "red"
    #elif "Eros" in label[k]: color = "green"
    #elif "Obs" in label[k]: color = "darkturquoise"

    #if stats is not None:
    #  for k in range(data.shape[1]):
    #    if sum(np.isnan(data[:,k]))!=len(dat):
    #      label[k] = label[k]+" | KGE : "+str(round(stats[k,0],2))+\
    #                          " | NSE : "+str(round(stats[k,1],2))+\
    #                          " | RMSE: "+str(round(stats[k,2],2))

