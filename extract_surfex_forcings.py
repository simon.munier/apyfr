#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 16 09:16:01 2022

@author: guillaumotl
"""

import numpy as np
import datetime as dt
from tqdm import trange
from .misc import days_in_month


def extract_surfex_forcings(date_start, n_years, n_months, date_cour, file_grid_surfex, surfex_dir, grid_surf,
                            extract_runoff=True, timestep='mon'):
    """
    Extract drainage and runoff from SURFEX for each cell of each application
    at a monthly time step.
    Arguments:
        date_start: first day of the simulation (ex: dt.date(2002, 8, 1))
        n_years: number of years included in the simulation
        n_months: number of months (or days) included in the simulation
        date_cour: array containing the date for each day of the simulation
        file_grid_surfex: name of the file containing SURFEX coordinates
        surfex_dir: name of the folder containing SURFEX forcings data
        grid_surf: dictionnary (keys = application names) containing X, Y, area
                   for the surface layer of the groundwater model
        extract_runoff : True if we want only recharge (default=True)
        timestep: 'mon' if we want monthly values, 'day' if we want daily
                  values (default='mon')
    Return:
        two dictionnaries (keys = application names) containing surfex recharge
        rates in mm/month in 2D array (rows=months, columns=cells):
        'recharge_from_surfex' and 'runoff_from_surfex'
    """

    Xc, Yc = np.genfromtxt(file_grid_surfex, dtype='float64', unpack=True)  # area of each SURFEX mesh = 8 km

    # Extracting drainage and runoff from SURFEX for each day of each year
    nb_records_days = 24  # SURFEX data at hourly scale

    drainj = np.empty((len(date_cour), 9892), dtype=np.float32)  # 9892 is the number of SURFEX meshes over FRANCE
    if extract_runoff:
        runoffj = np.empty((len(date_cour), 9892), dtype=np.float32)

    index = 0
    for yy in range(n_years):  # for each year (because one file per year)

        print('extracting recharge from SURFEX for ', date_start.year + yy)

        start_year = dt.datetime(date_start.year + yy, 8, 1, 0)
        end_year = dt.datetime(date_start.year + yy + 1, 7, 31, 0)
        nb_days_year =  (end_year - start_year).days + 1

        date_full = np.array([dt.date(date_start.year + yy, 8, 1) + dt.timedelta(days=i) for i in range(nb_days_year)])

        y = date_start.year + yy

        extension = '_{}_{}'.format(y, y + 1)

        nb_records_yy = nb_records_days * len(date_full)

        drainc_yy = np.empty((nb_records_yy, 9892), dtype=np.float32)
        drainj_yy = np.empty((len(date_full), 9892), dtype=np.float32)

        # Processing drainage
        i_record = 0
        j_record = 0
        DRAINC_FILE = '{}/DRAINC_ISBA.BIN{}'.format(surfex_dir,extension)
        with open(DRAINC_FILE, 'rb') as d_f:
            for t in date_full:
                for i in range(nb_records_days):
                    drainc_yy[i_record, :] = np.fromfile(d_f, dtype='>f', count=9892)  # SURFEX data are cumulative drainage at hourly scale for each year
                    drainc_yy[i_record, drainc_yy[i_record, :] > 1e10] = 0

                    if i == nb_records_days - 1:  # last point of the day
                        if j_record == 0:  # if we are in the first day
                            drainj_yy[j_record, :] = drainc_yy[i_record, :] # drainage of the first day = cumulative drainage recorded at the end of the first day
                        else:
                            drainj_yy[j_record, :] = drainc_yy[i_record, :] - drainc_yy[i_record - nb_records_days, :]
                        j_record += 1
                    i_record += 1
        del drainc_yy
        drainj[index:index+nb_days_year, :] = drainj_yy  # Saving daily drainage value for this year, in mm/day
        del drainj_yy

        # Processing runoff
        if extract_runoff:
            print('extracting runoff from SURFEX')
            runoffc_yy = np.empty((nb_records_yy, 9892), dtype=np.float32)
            runoffj_yy = np.empty((len(date_full), 9892), dtype=np.float32)
            i_record = 0
            j_record = 0
            RUNOFFC_FILE = '{}/RUNOFFC_ISBA.BIN{}'.format(surfex_dir,extension)
            with open(RUNOFFC_FILE, 'rb') as d_f:
                for t in date_full:
                    for i in range(nb_records_days):
                        runoffc_yy[i_record, :] = np.fromfile(d_f, dtype='>f', count=9892)  # SURFEX data are cumulative runoff at hourly scale for each year
                        runoffc_yy[i_record, runoffc_yy[i_record, :] > 1e10] = 0

                        if i == nb_records_days - 1:  # last point of the day
                            if j_record == 0:  # if we are in the first day
                                runoffj_yy[j_record, :] = runoffc_yy[i_record, :]
                            else:
                                runoffj_yy[j_record, :] = runoffc_yy[i_record, :] - runoffc_yy[i_record - nb_records_days, :]
                            j_record += 1
                        i_record += 1
            del runoffc_yy
            runoffj[index:index+nb_days_year, :] = runoffj_yy  # Saving daily runoff value for this year, in mm/day
            del runoffj_yy

        index += nb_days_year

    if timestep == 'mon':
        # Converting daily SURFEX drainage and runoff at monthly scale
        im = 0
        drainm = np.zeros((n_months, 9892), dtype=np.float32)  # It will contain monthly drainage value for each SURFEX mesh
        for mm in range(n_months):  # for each month
            nb_daysinmonth = days_in_month(date_cour[im])
            drainm[mm,:] = np.sum(drainj[im:im+nb_daysinmonth,:], 0)  # in mm/month
            im += nb_daysinmonth  # updating the number of day

        if extract_runoff:
            im = 0
            runoffm = np.zeros((n_months, 9892), dtype=np.float32)  # It will contain monthly runoff value for each SURFEX mesh
            for mm in range(n_months):  # for each month
                nb_daysinmonth = days_in_month(date_cour[im])
                runoffm[mm,:] = np.sum(runoffj[im:im+nb_daysinmonth,:], 0)  # in mm/month
                im += nb_daysinmonth  # updating the number of day


    # Computing monthly SURFEX drainage and runoff for each cell of each application
    # They are saved in two dictionaries (keys = application names)
    recharge_from_surfex = {} #np.zeros((n_months, nb_cells), dtype=np.float32)
    if extract_runoff:
        runoff_from_surfex = {} #np.zeros((n_months, nb_cells), dtype=np.float32)
    for applications in grid_surf:

        nb_cells_appli = len(grid_surf[applications])  # number of cells for this application

        # Drainage and runoff (ruissellement) will be saved in two 2D arrays
        Drainage = np.zeros((n_months, nb_cells_appli))
        if extract_runoff:
            Ruissellement = np.zeros((n_months, nb_cells_appli))

        for ii in range(nb_cells_appli):  # For each cell

            # Looking for the closer SURFEX mesh
            correspond_ind = np.argmin((grid_surf[applications][ii, 0] - Xc)**2 + (grid_surf[applications][ii, 1] - Yc)**2)

            if timestep == 'mon':
                # Adding SURFEX value of the closer mesh into Drainage and Ruissellement
                Drainage[:, ii] = drainm[:, correspond_ind] * (grid_surf[applications][ii, 2])
                if extract_runoff:
                    Ruissellement[:, ii] = runoffm[:, correspond_ind] * (grid_surf[applications][ii, 2])
            if timestep == 'day':
                # Adding SURFEX value of the closer mesh into Drainage and Ruissellement
                Drainage[:, ii] = drainj[:, correspond_ind] * (grid_surf[applications][ii, 2])
                if extract_runoff:
                    Ruissellement[:, ii] = runoffj[:, correspond_ind] * (grid_surf[applications][ii, 2])

        # Saving monthly SURFEX recharge (=drainage) and runoff for each X_sur,Y_sur coordinates
        recharge_from_surfex[applications] = Drainage / 1000  # in m3/day or m3/month
        if extract_runoff:
            runoff_from_surfex[applications] = Ruissellement / 1000  # in m3/day or m3/month

    if extract_runoff:
        return recharge_from_surfex, runoff_from_surfex
    else:
        return recharge_from_surfex

def extract_surfex_evap(date_start, n_years, n_months, date_cour, file_grid_surfex, surfex_dir, grid_surf, timestep='mon'):

    """
    Extract drainage and runoff from SURFEX for each cell of each application
    at a monthly time step.
    Arguments:
        date_start: first day of the simulation (ex: dt.date(2002, 8, 1))
        n_years: number of years included in the simulation
        n_months: number of months (or days) included in the simulation
        date_cour: array containing the date for each day of the simulation
        file_grid_surfex: name of the file containing SURFEX coordinates
        surfex_dir: name of the folder containing SURFEX forcings data
        grid_surf: dictionnary (keys = application names) containing X, Y, area
                   for the surface layer of the groundwater model
        extract_runoff : True if we want only recharge (default=True)
        timestep: 'mon' if we want monthly values, 'day' if we want daily
                  values (default='mon')
    Return:
        two dictionnaries (keys = application names) containing surfex recharge
        rates in mm/month in 2D array (rows=months, columns=cells):
        'recharge_from_surfex' and 'runoff_from_surfex'

    """

    Xc, Yc = np.genfromtxt(file_grid_surfex, dtype='float64', unpack=True)  # area of each SURFEX mesh = 8 km

    # Extracting drainage and runoff from SURFEX for each day of each year
    nb_records_days = 24  # SURFEX data at hourly scale

    drainj = np.empty((len(date_cour), 9892), dtype=np.float32)  # 9892 is the number of SURFEX meshes over FRANCE

    index = 0
    for yy in range(n_years):  # for each year (because one file per year)

        print('extracting recharge from SURFEX for ', date_start.year + yy)

        start_year = dt.datetime(date_start.year + yy, 8, 1, 0)
        end_year = dt.datetime(date_start.year + yy + 1, 7, 31, 0)
        nb_days_year =  (end_year - start_year).days + 1

        date_full = np.array([dt.date(date_start.year + yy, 8, 1) + dt.timedelta(days=i) for i in range(nb_days_year)])

        y = date_start.year + yy

        extension = '_{}_{}'.format(y, y + 1)

        nb_records_yy = nb_records_days * len(date_full)

        drainc_yy = np.empty((nb_records_yy, 9892), dtype=np.float32)
        drainj_yy = np.empty((len(date_full), 9892), dtype=np.float32)

        # Processing drainage
        i_record = 0
        j_record = 0
        DRAINC_FILE = '{}/EVAPC_ISBA.BIN{}'.format(surfex_dir,extension)
        with open(DRAINC_FILE, 'rb') as d_f:
            for t in date_full:
                for i in range(nb_records_days):
                    drainc_yy[i_record, :] = np.fromfile(d_f, dtype='>f', count=9892)  # SURFEX data are cumulative drainage at hourly scale for each year
                    drainc_yy[i_record, drainc_yy[i_record, :] > 1e10] = 0

                    if i == nb_records_days - 1:  # last point of the day
                        if j_record == 0:  # if we are in the first day
                            drainj_yy[j_record, :] = drainc_yy[i_record, :] # drainage of the first day = cumulative drainage recorded at the end of the first day
                        else:
                            drainj_yy[j_record, :] = drainc_yy[i_record, :] - drainc_yy[i_record - nb_records_days, :]
                        j_record += 1
                    i_record += 1
        del drainc_yy
        drainj[index:index+nb_days_year, :] = drainj_yy  # Saving daily drainage value for this year, in mm/day
        del drainj_yy

        index += nb_days_year

    if timestep == 'mon':
        # Converting daily SURFEX drainage and runoff at monthly scale
        im = 0
        drainm = np.zeros((n_months, 9892), dtype=np.float32)  # It will contain monthly drainage value for each SURFEX mesh
        for mm in range(n_months):  # for each month
            nb_daysinmonth = days_in_month(date_cour[im])
            drainm[mm,:] = np.sum(drainj[im:im+nb_daysinmonth,:], 0)  # in mm/month
            im += nb_daysinmonth  # updating the number of day


    # Computing monthly SURFEX drainage and runoff for each cell of each application
    # They are saved in two dictionaries (keys = application names)
    recharge_from_surfex = {} #np.zeros((n_months, nb_cells), dtype=np.float32)

    for applications in grid_surf:

        nb_cells_appli = len(grid_surf[applications])  # number of cells for this application

        # Drainage and runoff (ruissellement) will be saved in two 2D arrays
        Drainage = np.zeros((n_months, nb_cells_appli))

        for ii in trange(nb_cells_appli):  # For each cell

            # Looking for the closer SURFEX mesh
            correspond_ind = np.argmin((grid_surf[applications][ii, 0] - Xc)**2 + (grid_surf[applications][ii, 1] - Yc)**2)

            if timestep == 'mon':
                # Adding SURFEX value of the closer mesh into Drainage and Ruissellement
                Drainage[:, ii] = drainm[:, correspond_ind] * (grid_surf[applications][ii, 2])
            if timestep == 'day':
                # Adding SURFEX value of the closer mesh into Drainage and Ruissellement
                Drainage[:, ii] = drainj[:, correspond_ind] * (grid_surf[applications][ii, 2])

        # Saving monthly SURFEX recharge (=drainage) and runoff for each X_sur,Y_sur coordinates
        recharge_from_surfex[applications] = Drainage / 1000  # in m3/day or m3/month

    return recharge_from_surfex
