#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 22 16:56:48 2023

@author: jeanteta
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import matplotlib.dates as mdates
from PIL import Image
from scipy.stats import norm

from .misc import apyfr_path, nargout, to_date
from .spli import spli_norm

def set_date_ticks(ax,dat):
    '''
    Affichage des dates sur l'axe des abscisses
    '''
    tdiff = dat[-1]-dat[0]
    if   tdiff.days/365 > 40 : d = [d for d in dat if str(d)[3:7] == "0-01"]
    elif tdiff.days/365 > 10 : d = [d for d in dat if str(d)[3:7] in ["0-01","5-01"]]
    elif tdiff.days/365 > 5  : d = [d for d in dat if str(d)[5:7] == "01"]
    elif tdiff.days/365 > 1.5: d = [d for d in dat if str(d)[5:7] in ["01","07"]]
    else: d = dat
    plt.xticks(d,[str(di)[0:7] for di in d],fontsize=8,rotation=45,ha='right')
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m'))
    return


def add_logo(ax=None,loc=(0.15,0.9),zoom=0.5):
    '''
    Affichage du logo AquiFR
    '''
    if ax is None: ax = plt.gca()
    logo = np.array(Image.open(apyfr_path+'/data/aquifr_logo_entites.png'))[::-1]
    im = OffsetImage(logo, zoom=zoom, zorder=1000, alpha=1)
    ab = AnnotationBbox(im,loc,xycoords='figure fraction',frameon=True)
    ax.add_artist(ab)


def spli_background_color(ax=None,fill=True,piezo_level=False,dat=None,cdf=None,text=True):
    '''
    Affichage du fond de couleur des SPLI

    Si piezo_level :
      dat ne doit pas etre None
      cdf = (min1,max1,cdf)
    '''
    if ax is None: ax = plt.gca()
    if 'bgspli' in dir(ax) and ax.bgspli: return
    if dat is None: dat = ax.get_xlim()
    if piezo_level:
      min1,max1,cdf = cdf
      axfun = ax.fill_between
    else:
      axfun = ax.axhspan
    for i in range(len(spli_norm['colors'])):
      if piezo_level:
        val = np.zeros((len(dat),2))
        for m in range(12):
            hlin = np.linspace(min1[m],max1[m],cdf.shape[1])
            Idat = dat.month==m+1
            val[Idat,0] = np.interp(spli_norm['values'][i],norm.ppf(cdf[m]),hlin)
            val[Idat,1] = np.interp(spli_norm['values'][i+1],norm.ppf(cdf[m]),hlin)
        text_val = (val[-1,0]+val[-1,1])/2
        val = dat,val[:,0],val[:,1]
      else:
        val = spli_norm['values'][[i,i+1]]
        text_val = (spli_norm['values'][i]+spli_norm['values'][i+1])/2
      if fill:
        fc,ec = spli_norm['colors'][i],None
      elif i!=3:
        fc,ec = None,spli_norm['colors'][i]
      axfun(*val,fc=fc,ec=ec,lw=1,alpha=0.2)
      if text:
        plt.text(dat[-1]+0.01*(dat[-1]-dat[0]),text_val,spli_norm['period'][i],
                    ha='left',va='center',fontsize=7)
    ax.bgspli = True
    if not fill: ax.set_facecolor([.95,.95,.95])
    return


def time_series_piezo(dat, data, ax=None, cdf=None, fill=True, text=True, label=None,
                     title=None, logo=False, filename=None, **kwargs):
    """
    Comparaison graphique des hauteurs d'eau sur un piezometre donné

    Parameters
    ----------
    dat : liste, rempli de dates issues de la fonction "to_date" (cf. misc)
        Liste des dates associées à la comparaison.
    obs : array, nb de lignes = taille des dates, nb de colonnes = 1
        numpy array des observations. Si des données manquent dans la chronique, un NaN est attendu
    sim : array, nb de lignes = taille des dates, nb de colonnes = 3
        numpy array des simulations. Dans l'ordre en colonne : Marthe, EauDyssée et Eros
    stats : None ou array, nb de lignes = 3, nb de colonnes = 3. Optionnel
        tableau optionnel des valeurs de KGE, NSE et RMSE (dans l'ordre sur les colonnes)
        pour chacun des trois modèles Marthe, EauDyssée et Eros (dans l'ordre sur les lignes)
    title : string (optionnel) pour afficher un titre
    filename : string (optionnel) pour enregistrer la figure sans l'afficher
    logo : booléen (optionnel) pour afficher le logo AquiFR

    Returns
    -------
    None
    """

    if ax is None: fig,ax = plt.subplots(figsize=(8,5))
    plt.axes(ax)

    ax.plot(dat,data,label=label, **kwargs)
    plt.xlim(dat[[0,-1]])
    #set_date_ticks(ax,dat)
    ax.xaxis.set_tick_params(labelsize=8,labelrotation=45)
    plt.yticks(fontsize=8)
    plt.ylabel("Hauteur piézométrique (m)")

    if label: ax.legend(loc='upper right',shadow=False,prop={"size":7})
    if cdf is not None: spli_background_color(ax=ax,piezo_level=True,dat=dat,cdf=cdf,fill=fill,text=text)

    if title: plt.title(title)
    if logo: add_logo(ax)
    if filename:
        plt.savefig(filename)
        plt.close()
        return
    else:
        return ax if nargout()>0 else None


def time_series_spli(dat, spli, ax=None, bgcolor=True, fill=True, text=True, label=None,
                     title=None, logo=False, filename=None, **kwargs):
    """
    Tracé de la série temporelle de SPLI

    Parameters
    ----------
    dat : vecteur de dates issues de la fonction "to_date" (cf. misc)
    spli : vecteur ou matrice des valeurs de SPLI
    ax : instance axes (optionnel)
    bgcolor : bouléen (optionnel) pour tracer le fond en couleur des SPLI
    fill : booléen (optionnel) pour remplir le fond en couleur
    text : booléen (optionnel) pour afficher les classes de spli à droite de l'axe
    label : string (optionnel) ou liste de string pour affichage du nom de la série
    title : string (optionnel) pour afficher un titre
    filename : string (optionnel) pour enregistrer la figure sans l'afficher
    logo : booléen (optionnel) pour afficher le logo AquiFR

    Returns
    -------
    None
    """
    if ax is None: fig,ax = plt.subplots(figsize=(8,5))
    plt.axes(ax)

    if len(spli.shape)==1: spli = np.reshape(spli,(-1,1))
    spli[~np.isfinite(spli)] = np.nan
    ax.plot(dat,spli,label=label,**kwargs)
    plt.axis([dat[0],dat[-1],-3,3])
    plt.yticks(fontsize=8)
    plt.ylabel("SPLI")
    #set_date_ticks(ax,dat)
    ax.xaxis.set_tick_params(labelsize=8,labelrotation=45)
    if label: plt.legend(loc='upper left',shadow=False,prop={"size":7})
    if bgcolor: spli_background_color(ax=ax,piezo_level=False,fill=fill,text=text)

    if title: plt.title(title)
    if logo: add_logo(ax)
    if filename:
        plt.savefig(filename)
        plt.close()
        return
    else:
        return ax if nargout()>0 else None


def time_series_spli_rea_ps(dat_REA, spli_REA, dat_PS, spli_PS, ax=None, bgcolor=True, fill=True,
                        title=None, logo=False, filename=None):
    '''
    Tracé de la série temporelle de SPLI combinant réanalyse et prévision saisonnière

    Parameters
    ----------
    dat_REA
    spli_REA
    dat_PS
    spli_PS
    ax : instance axes (optionnel)
    bgcolor : bouléen (optionnel) pour tracer le fond en couleur des SPLI
    fill : booléen (optionnel) pour remplir le fond en couleur
    title : string (optionnel) pour afficher un titre
    filename : string (optionnel) pour enregistrer la figure sans l'afficher
    logo : booléen (optionnel) pour afficher le logo AquiFR

    Returns
    -------
    [ax1,ax2]
    '''

    dat_HIST_fin = dat_REA[-1]-pd.DateOffset(months=6)
    Idat_HIST = dat_REA<=dat_HIST_fin
    Idat_TR   = dat_REA>=dat_HIST_fin
    dat_HIST,spli_HIST = dat_REA[Idat_HIST],spli_REA[Idat_HIST]
    dat_TR  ,spli_TR   = dat_REA[Idat_TR]  ,spli_REA[Idat_TR]

    if ax is None:
        fig,(ax1, ax2) = plt.subplots(1,2,sharey=True,facecolor='w',figsize=(14,5))
        plt.subplots_adjust(left=0.04,right=0.93,top=0.92,bottom=0.1,wspace=0)
    else:
        ax1,ax2 = ax
    if title is not None: fig.suptitle(title, fontsize=16)

    time_series_spli(dat_HIST,spli_HIST,c='k',ax=ax1,bgcolor=bgcolor,fill=fill,text=False)
    ax1.grid(axis='x')

    dat2 = to_date(dat_TR.union(dat_PS))
    time_series_spli(dat_TR,spli_TR,c='k',ax=ax2,bgcolor=None)
    time_series_spli(dat_PS,spli_PS,c=(0.,0.5,1.),ax=ax2,bgcolor=bgcolor,fill=fill,text=True)
    ax2.grid(axis='x')
    #set_date_ticks(ax2,dat2[1:])
    ax2.xaxis.set_tick_params(labelsize=8,labelrotation=45)
    ax2.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m'))
    ax2.set_xlim(dat2[[0,-1]])
    ax2.set_ylabel(None)

    if logo: add_logo(ax1)
    if filename:
        plt.savefig(filename)
        plt.close()
        return
    else:
        return [ax1,ax2] if nargout()>0 else None


