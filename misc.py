#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  3 10:17:29 2023
Boîte à outils pour les fonctions courantes/transverses du package APYFR
@author: Alexis Jeantet, à partir des codes d'André Mounier et des apports de Luca Guillaumot'
"""
import os
import pandas as pd
import numpy as np
import datetime as dt
from inspect import getfullargspec
from traceback import extract_stack

apyfr_path = os.path.dirname(__file__)

applis_mart = ['mart_som','mart_poc','mart_bno','mart_als','mart_npc','mart_teg']
applis_odic = ['odic_somme','odic_loire','odic_basse-normandie','odic_seine','odic_seine-eure',
               'odic_seine-oise','odic_marne-oise','odic_marne-loing','odic_seine-tertiaire']
applis_eros = ['eros_karsts']
applis_hs1d = ['hs1d_hs1d01','hs1d_hs1d02']
applis_trip = ['trip_france']
applis_all = applis_mart+applis_odic

model_appli = {'som':'mart','poc':'mart','bno':'mart','als':'mart','npc':'mart','teg':'mart',
               'somme':'odic','loire':'odic','basse-normandie':'odic','seine':'odic',
               'seine-eure':'odic','seine-oise':'odic','marne-oise':'odic','marne-loing':'odic',
               'seine-tertiaire':'odic',
               'karsts':'eros',
               'france':'trip',
               }

default_map_applis = [#'trip_france',
                      'odic_loire','odic_seine','mart_bno','odic_basse-normandie','mart_npc','mart_som',
                      'odic_marne-loing','odic_marne-oise','odic_seine-oise','odic_seine-eure',
                      'odic_seine-tertiaire','mart_poc','mart_als','mart_teg']

var_dim = {'odic':{'h':'sou','h_surf':'sur','h_piezos':'pzo','q':'riv','q_nr':'sou',
                   'spli_surf':'sur','spli_piezos':'pzo'},
           'mart':{'h':'sou','h_surf':'sur','h_piezos':'pzo','q':'riv','q_nr':'riv','q_deb':'sou',
                   'spli_surf':'sur','spli_piezos':'pzo'},
           'eros':{'h':'sou','h_surf':'sur','spli_surf':'sur','q':'riv'},
           'hs1d':{'h':'sou','h_surf':'sur','q':'riv','q_deb':'sou'},
           'trip':{'h':'sou','h_surf':'sur','spli_surf':'sur','q':'riv'},
           }

annee_mois  = ['Janvier','Fevrier','Mars','Avril','Mai','Juin','Juillet',
               'Aout','Septembre','Octobre','Novembre','Decembre']


#%% Fonctions générales
def available_kwargs(f,kwargs):
  available_kwargs = getfullargspec(f).args
  return dict([(k,v) for k,v in kwargs.items() if k in available_kwargs])


def nargout():
    '''
    Return how many values the caller is expecting
    Important: function call should not be splitted into several lines
    '''
    callInfo = extract_stack()
    callLine = str(callInfo[-3].line)
    split_equal = callLine.split('(')[0].split('=')
    if len(split_equal)==1: return 0
    split_comma = split_equal[0].split(',')
    return len(split_comma)


def print_message(message):
    if type(message)!=str:
        print("'Message' n'est pas une chaine de caractere")
        return
    print('-'*(len(message)+10))
    print('---  '+message+'  ---')
    print('-'*(len(message)+10))


#%% Fonctions sur les dates
def to_date(dat,outfmt='datetime'):
    """
    Convertions dates
    Parameters
    ----------
    dat can be the following form:
      '2000-01-01'
      [2000,1,1]
      datetime.datetime(2000,1,1,0,0)
    outfmt: "pandas" (Timestamp), "str" (%Y-%m-%d), "datetime"
    """
    if isinstance(dat,str) and dat=="now":
        dat = pd.Timestamp.today().replace(hour=0,minute=0,second=0,microsecond=0)
    is_date_vector = ('__len__' in dir(dat) and not isinstance(dat[0],str) \
                      and len(dat)==3 and dat[0]>1800 and dat[0]<2100)
    if type(dat) in [tuple,list,np.ndarray] and not is_date_vector:
        return np.array([to_date(d,outfmt=outfmt) for d in dat])
    if not isinstance(dat,pd.Timestamp):
        if is_date_vector: dat = pd.Timestamp(year=dat[0],month=dat[1],day=dat[2])
        elif type(dat) in [str,np.str_,np.datetime64,dt.datetime]: dat = pd.to_datetime(dat)
    if outfmt=="datetime": dat = dat.to_pydatetime()
    if outfmt=='str': dat = dat.strftime('%Y-%m-%d')
    return dat

def days_in_month(t):
    """
    days_in_month(t)

    Retourne le nombre de jours dans le mois incluant une date donnée.

    Paramètre
    ---------
    t : datetime.datetime
        Objet datetime obtenu à partir du module datetime
    """
    next_month = dt.date(t.year, t.month, 28) + dt.timedelta(days=4)
    t_next_month = dt.date(next_month.year, next_month.month, 1)
    nb_days = (t_next_month - dt.timedelta(days=1)).day
    return nb_days

def days_in_year(t):
    """
    days_in_year(t)

    Retourne le nombre de jours de l'année en cours si t <= 31 juillet,
    de l'année suivante si t >= 1er aout

    Paramètre
    ---------
    t : datetime.datetime
        Objet datetime obtenu à partir du module datetime
    """
    if t.month <= 7:
        nb_days = (dt.date(t.year, 12, 31) - dt.date(t.year, 1, 1)).days + 1
    else:
        next_year = dt.date(t.year, t.month, 28) + dt.timedelta(days=360)
        nb_days = (dt.date(next_year.year, 12, 31) - dt.date(next_year.year, 1, 1)).days + 1
    return nb_days


#%% Fonctions de comparaison entre simulations et observations
#    Kling and Gupta Efficiency: KGE
def KGE(s,o):
    """
    Kling Gupta Efficiency (Kling et al., 2012, http://dx.doi.org/10.1016/j.jhydrol.2012.01.011)
    input:
        s: simulated
        o: observed
    output:
        KGE: Kling Gupta Efficiency
    """
    # s = sim_pzo_appli_i.values.tolist()
    # o = obs_pzo.values.tolist()[0]
    B   = np.nanmean(s) / np.nanmean(o)
    y   = (np.nanstd(s) / np.nanmean(s)) / (np.nanstd(o) / np.nanmean(o))
    #r = np.corrcoef(o, s)[0,1]
    # r = np.empty(s.shape)
    r   = np.empty(len(o))                  # Modifs Alexis
    # s1 = np.ma.masked_invalid(s[:])
    s1  = np.ma.masked_invalid(np.array(s)) # Modifs Alexis
    # o1 = np.ma.masked_invalid(o[:])
    o1  = np.ma.masked_invalid(np.array(o)) # Modifs Alexis
    msk = (~o1.mask & ~s1.mask)
    r   = np.ma.corrcoef(o1[msk], s1[msk]).data[0,1]
    KGE = 1 - np.sqrt((r - 1) ** 2 + (B - 1) ** 2 + (y - 1) ** 2)

    return KGE

#    Nash and Sutcliffe Efficiency: NSE
def NSE(s,o):
    """
    Nash-Sutcliffe efficiency coefficient
    input:
        s: simulated
        o: observed
    output:
        NS: Nash-Sutcliffe efficient coefficient
    """
    # s = sim_pzo[dates_eval].values
    # o = obs_pzo.values

    # NSE = 1 - np.nansum((s-o)**2)/np.nansum((o-np.nanmean(o))**2)
    NSE = 1 - np.nansum((np.array(s) - np.array(o))**2)/np.nansum((np.array(o)-np.nanmean(np.array(o)))**2)# Modifs Alexis
    return NSE

#    Root Mean Square Error: RMSE
def RMSE(s, o):
    """
    Root Mean Square Error
    input: (Modifs Alexis : les arguments d'entrée doivent être au format liste)
        s: simulated
        o: observed
    output:
        RMSE: Root Mean Square Error
    """
    # RMSE = np.sqrt(np.nanmean((s-o)**2))
    RMSE = np.sqrt(np.nanmean((np.array(s)-np.array(o))**2))
    return RMSE

#    Root Mean Square Error with Bias excluded: RMSE_BE
def RMSE_BE(s, o):
    """
    RMSE Bias Excluded (Vergnes et al., 2020, https://doi.org/10.5194/hess-24-633-2020)
    input:
        s: simulated
        o: observed
    output:
        RMSE_BE
    """
    RMSE_BE = np.sqrt(np.sum(np.square((np.array(s) - np.nanmean(np.array(s))) - (np.array(o) - np.nanmean(np.array(o)))))/len(s))
    return RMSE_BE

# Bias: BIAS
def BIAS(s, o):
    """
    Bias (Vergnes et al., 2020, https://doi.org/10.5194/hess-24-633-2020)
    input:
        s: simulated
        o: observed
    output:
        BIAS
    """
    BIAS = np.nansum(np.array(s)-np.array(o))/len(s)
    return BIAS


