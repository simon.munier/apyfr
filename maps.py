import numpy as np
from matplotlib import colors, colorbar, pyplot as plt
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
from PIL import Image
from cartopy import crs as ccrs, feature as cfeature
from cartopy.io.shapereader import Reader
from netCDF4 import Dataset
from .misc import available_kwargs, nargout, apyfr_path, var_dim, annee_mois

default_extent = {'france':(61000,1029000,1725000,2705000),
                  'seine':(400000,815000,2240000,2640000),
                  'somme':(515000,725000,2465000,2625000),
                  'loire':(335000,680000,2150000,2435000),
                  'basse-normandie':(330000,470000,2380000,2515000),
                  'marne-loing':(580000,815000,2250000,2480000),
                  'marne-oise':(645000,825000,2390000,2590000),
                  'seine-eure':(400000,585000,2330000,2540000),
                  'seine-oise':(415000,715000,2415000,2600000),
                  'seine-tertiaire':(473000,752000,2270000,2560000),
                  'som':(500000,700000,2480000,2625000),
                  'npc':(515000,750000,2515000,2700000),
                  'bno':(330000,490000,2375000,2520000),
                  'poc':(260000,525000,2025000,2265000),
                  'als':(940000,1015000,2285000,2375000),
                  'teg':(465000,560000,1850000,1920000),
                  'karsts':(310000,950000,1730000,2340000),
                  }

class EPSG(ccrs.Projection):
  def __init__(self,code):
    from pyproj import proj
    proj4_dict = {'proj':'lcc','lat_1':'46.8','lat_0':'46.8','lon_0':'0','k_0':'0.99987742',
                  'x_0':'600000','y_0':'2200000','pm':'paris','units':'m','no_defs':None}
    proj4_dict = proj.Proj(init='epsg:'+str(code))
    globe = ccrs.Globe(semimajor_axis='6378249.2',semiminor_axis='6356515',towgs84='-168,-60,320,0,0,0,0')
    super(L2E_Proj, self).__init__(proj4_dict,globe)
    x0, y0, x1, y1 = proj4_dict.crs.area_of_use.bounds
    geodetic = ccrs.Geodetic()
    lons = np.array([x0, x0, x1, x1])
    lats = np.array([y0, y1, y1, y0])
    points = self.transform_points(geodetic, lons, lats)
    x,y = points[:, 0],points[:, 1]
    self.bounds = (x.min(), x.max(), y.min(), y.max())
  @property
  def boundary(self):
    import shapely.geometry as sgeom
    x0, x1, y0, y1 = self.bounds
    return sgeom.LineString([(x0, y0), (x0, y1), (x1, y1), (x1, y0), (x0, y0)])
  @property
  def x_limits(self):
      x0, x1, y0, y1 = self.bounds
      return (x0, x1)
  @property
  def y_limits(self):
      x0, x1, y0, y1 = self.bounds
      return (y0, y1)
  @property
  def threshold(self):
      x0, x1, y0, y1 = self.bounds
      return min(x1 - x0, y1 - y0) / 100.

class L2E_Proj(ccrs.Projection):
  def __init__(self):
    proj4_dict = {'proj':'lcc','lat_1':'46.8','lat_0':'46.8','lon_0':'0','k_0':'0.99987742',
                  'x_0':'600000','y_0':'2200000','pm':'paris','units':'m','no_defs':None}
    globe = ccrs.Globe(semimajor_axis='6378249.2',semiminor_axis='6356515',towgs84='-168,-60,320,0,0,0,0')
    super(L2E_Proj, self).__init__(proj4_dict,globe)
    x0, x1, y0, y1 = [-4.87, 8.23, 42.33, 51.14]
    geodetic = ccrs.Geodetic()
    lons = np.array([x0, x0, x1, x1])
    lats = np.array([y0, y1, y1, y0])
    points = self.transform_points(geodetic, lons, lats)
    x,y = points[:, 0],points[:, 1]
    self.bounds = (x.min(), x.max(), y.min(), y.max())
  @property
  def boundary(self):
    import shapely.geometry as sgeom
    x0, x1, y0, y1 = self.bounds
    return sgeom.LineString([(x0, y0), (x0, y1), (x1, y1), (x1, y0), (x0, y0)])
  @property
  def x_limits(self):
      x0, x1, y0, y1 = self.bounds
      return (x0, x1)
  @property
  def y_limits(self):
      x0, x1, y0, y1 = self.bounds
      return (y0, y1)
  @property
  def threshold(self):
      x0, x1, y0, y1 = self.bounds
      return min(x1 - x0, y1 - y0) / 100.

#L2E = ccrs.epsg('27572')
L2E = L2E_Proj()
WGS = ccrs.PlateCarree()

# Default figure size and axes margins
figw_max,figh_max = 1000,1000
fig_margin = (20,20,20,30) # (left,right,bottom,top)
colorbar_width = 160
fig_dpi = 100


def get_coord_L2E(appli,lvl='sur',var=None):
  '''
  Get coordinates for given appli and level
  '''
  if '_' in appli: model,appli = appli.split('_')
  if var is not None: lvl = var_dim[model][var]
  with Dataset(apyfr_path+'/data/'+appli+'_L2E.nc') as nc:
    coord = np.stack((nc['X_'+lvl][:].filled(),nc['Y_'+lvl][:].filled(),nc['D_'+lvl][:].filled())).T
  return coord


def get_coord_values(applis_data,var):
  '''
  Create coord and values vectors
  '''
  indices = np.hstack((0,np.cumsum([len(v) for v in applis_data.values()])))
  coord,values = np.zeros((indices[-1],3)),np.zeros(indices[-1])
  for iapp,(mod_appli,v) in enumerate(applis_data.items()):
    model,appli = mod_appli.split('_')
    Iapp = slice(indices[iapp],indices[iapp+1])
    coord[Iapp,:] = get_coord_L2E(mod_appli,var=var)
    values[Iapp] = v
  return coord,values


def blank_map(applis=None,extent=None,zone=None,ax=None,draw_colorbar=True,syrah=False,
              draw_background=True,alpha_background=0.7,lw_depts=0.5,alpha_depts=0.7,**kwargs):
  '''
  '''
  if applis is None:
    applis = applis_all
  elif isinstance(applis,str):
    applis = [applis]
  if extent is None:
    if zone is not None:
      xmin,ymin = L2E.transform_point(zone[0][0],zone[1][1],WGS)
      xmax,ymax = L2E.transform_point(zone[1][0],zone[0][1],WGS)
    elif len(applis)==1:
      xmin,xmax,ymin,ymax = default_extent[applis[0].split('_')[1]]
    else:
      xmin,xmax,ymin,ymax = default_extent['france']
    extent = xmin,xmax,ymin,ymax
  # Prepare figure
  if ax is None:
    axratio = (extent[3]-extent[2])/(extent[1]-extent[0])
    margin = list(fig_margin)
    axw_max,axh_max = figw_max-margin[0]-margin[1],figh_max-margin[2]-margin[3]
    if axratio>axw_max/axh_max: axw,axh = axw_max/axratio,axw_max
    else                      : axw,axh = axh_max,axh_max*axratio
    if draw_colorbar: margin[1] = margin[1]+colorbar_width
    figw,figh = axw+margin[0]+margin[1],axh+margin[2]+margin[3]
    fig_kwargs = available_kwargs(plt.figure,kwargs)
    fig = plt.figure(figsize=(figw/fig_dpi,figh/fig_dpi),dpi=fig_dpi,facecolor='w',
                     constrained_layout=False,**fig_kwargs)
    ax = plt.axes([20/figw,20/figh,axw/figw,axh/figh],projection=L2E)
  ax.set_extent(extent,L2E)
  #ax.coastlines(resolution='10m')
  #ax.add_feature(cfeature.NaturalEarthFeature('cultural','admin_0_countries','10m',facecolor='none'),edgecolor='k')
  ax.add_geometries(Reader(apyfr_path+'/data/limites_france.shp').geometries(),
                           L2E,edgecolor='k',facecolor='none',linewidth=1)
  #ax.add_feature(cfeature.NaturalEarthFeature('physical','rivers_lake_centerlines','10m',edgecolor='blue',
  #                                            facecolor='none',linewidth=1.5,zorder=2,alpha=0.3))
  with Dataset(apyfr_path+'/data/departements_120D.nc') as nc: depx,depy = nc['lon'][:],nc['lat'][:]
  ax.plot(depx,depy,color=[.2,.2,.2],transform=WGS,lw=lw_depts,alpha=alpha_depts)
  if draw_background:
    ne = plt.imread(apyfr_path+'/data/SRTM_France_L2E.png')
    ax.imshow(ne,extent=(10000,1050000,1725000,2705000),origin='upper',transform=L2E,alpha=alpha_background)
  if syrah:
    ax.add_geometries(Reader(apyfr_path+'/data/SYRAH_Rang8765.shp').geometries(),
                      L2E,edgecolor='blue',facecolor='none',linewidth=1.0,alpha=.4)
  return ax


def map_applis(applis_data,var,ax=None,extent=None,zone=None,vlim=None,cmap=None,norm=None,
               nan_to_zero=False,nan_to_grey=True,syrah=False,dat=None,title=None,marker='s',marker_size=None,
               draw_colorbar=True,logo=False,title_colorbar=None,**kwargs):
  '''
  applis_data is a dict {appli:data} (appli='mart_npc', data being a 1D vector)
  extent = [xmin,xmax,ymin,ymax] (x,y are L2E coordinates)
  zone = [[lon_ul,lat_ul],[lon_lr,lat_lr]]
  '''

  # Time dimension
  if min([len(v.shape) for v in applis_data.values()])==1:
    applis_data = {mod_appli:v[np.newaxis] for mod_appli,v in applis_data.items()}
  ndat = min([v.shape[0] for v in applis_data.values()])

  # Get coordinates and values vectors
  coord = np.vstack([get_coord_L2E(appli,var=var) for appli in applis_data])
  values = np.hstack([applis_data[appli][0,:] for appli in applis_data])
  Inan = np.isfinite(values)
  if nan_to_zero: # NaN to 0
    values[~Inan] = 0
    Inan = np.isfinite(values)

  # Colormap (cmap, norm, vlim)
  if cmap=='spli' or ('spli' in var and cmap is None):
    colorlist = [(1.,0.,0.),(1.,0.5,0.),(1.,1.,0.),(0.,1.,0.),(0.,1.,1.),(0.0,0.5,1.),(0.,0.,1.)]
    cmap = colors.ListedColormap(colorlist)
    tick = np.array([-3.,-1.28,-0.84,-0.25,0.25,0.84,1.28,3.])
    norm = colors.BoundaryNorm(tick,len(tick)-1)
    tickz = np.arange(0.5,len(tick)-1)/(len(tick)-1)
    dureeRet = [">10 ans sec",">5 ans sec",">2.5 ans sec","autour de\nla normale",
                ">2.5 ans hum",">5 ans hum",">10 ans hum"]
    if vlim is None: vlim = np.nanmin(values),np.nanmax(values)
  else:
    if cmap is None: cmap = 'viridis'
    if isinstance(cmap,str): cmap = plt.get_cmap(cmap)
    if vlim is None:
      vlim = np.nanmin(values),np.nanmax(values)
    elif isinstance(vlim,str) and vlim=='percentile':
      vlim = np.nanpercentile(values[values!=0],2),np.nanpercentile(values[values!=0],98)
    if norm is None:
      norm = colors.Normalize(vmin=vlim[0],vmax=vlim[1])
    elif norm=='log':
      norm = colors.LogNorm(vmin=vlim[0],vmax=vlim[1])
    else:
      raise Exception('map_applis: norm argument is not recognized: '+norm)
    dureeRet = None
  cmap_nan = plt.cm.get_cmap('Greys')

  # Create map
  ax = blank_map(applis=list(applis_data),extent=extent,zone=zone,syrah=syrah,draw_colorbar=draw_colorbar)
  sc_kwargs = available_kwargs(ax.scatter,kwargs)
  hnan = None
  if nan_to_grey:
    hnan = ax.scatter(coord[~Inan,0],coord[~Inan,1],s=marker_size,c=[[.5,.5,.5]],marker=marker,lw=0,transform=L2E)
    hnan.coord,hnan.Inan = coord,~Inan
  h = ax.scatter(coord[Inan,0],coord[Inan,1],s=marker_size,c=values[Inan],marker=marker,lw=0,
                 transform=L2E,cmap=cmap,norm=norm,**sc_kwargs)
  h.coord,h.Inan = coord,Inan
  ax.h,ax.hnan = h,hnan

  # Colorbar
  if draw_colorbar:
    figw,figh = ax.figure.get_size_inches()*ax.figure.get_dpi()
    axw,axh = figw*ax.get_position().width,figh*ax.get_position().height
    cbar_ax = plt.axes([(fig_margin[0]+axw+20)/figw,fig_margin[2]/figh,30/figw,axh/figh])
    if dureeRet is not None:
      cb = colorbar.ColorbarBase(cbar_ax,cmap=cmap,spacing='uniform',extend='both')
      cb.set_ticks(tickz)
      cb.ax.set_yticklabels(dureeRet)
    else:
      cb = colorbar.ColorbarBase(cbar_ax,cmap=cmap,norm=norm,spacing='uniform',extend='both')
    cb.ax.tick_params(labelsize=10,size=0)
    if title_colorbar is not None:
      cb.set_label(title_colorbar, fontsize=14)

  # Adapt marker size when resizing/zooming
  def __get_msize(hh):
    ax_width,ax_height = ax.get_position().extents[2:]
    fx,fy = ax.figure.get_size_inches()*ax.figure.dpi
    msizex = coord[hh.Inan,2]/np.diff(ax.get_extent()[:2])*fx*ax_width
    msizey = coord[hh.Inan,2]/np.diff(ax.get_extent()[2:])*fy*ax_height
    msize = np.minimum(msizex,msizey)
    return msize
  def __update_msize(*args):
    if nan_to_grey: hnan.set_sizes(__get_msize(hnan)**2/2)
    h.set_sizes(__get_msize(h)**2/2)
  if marker_size is None:
    __update_msize()
    ax.callbacks.connect('xlim_changed',__update_msize)
    ax.callbacks.connect('ylim_changed',__update_msize)
    ax.figure.canvas.mpl_connect('draw_event',__update_msize)

  # Time dimension
  ax.ktime,ax.Ntime = 0,ndat
  def __redraw_h(h,Inan,values,isnan=False):
    h.Inan = Inan
    h.set_offsets(h.coord[Inan,:])
    if not isnan: h.set_array(values[Inan])
  def __redraw():
    __set_title()
    values = np.hstack([applis_data[appli][ax.ktime,:] for appli in applis_data])
    Inan = np.isfinite(values)
    if nan_to_grey: __redraw_h(hnan,~Inan,values,isnan=True)
    __redraw_h(h,Inan,values)
    __update_msize()
    ax.figure.canvas.draw_idle()
  def __change_date(event):
    if   event.key == 'home' : ax.ktime = 0
    elif event.key == 'up'   : ax.ktime = np.mod(ax.ktime+12,ax.Ntime)
    elif event.key == 'right': ax.ktime = np.mod(ax.ktime+1,ax.Ntime)
    elif event.key == 'left' : ax.ktime = np.mod(ax.ktime-1,ax.Ntime)
    elif event.key == 'down' : ax.ktime = np.mod(ax.ktime-12,ax.Ntime)
    elif event.key == 'end'  : ax.ktime = ax.Ntime-1
    __redraw()
  ax.figure.canvas.mpl_connect('key_press_event',__change_date)
  ax.__redraw = __redraw

  # Title and logo
  def __set_title():
    title_str = [title] if title is not None else []
    title_dat = []
    if dat is not None:
      title_dat = [dat[ax.ktime].strftime('%Y - ')+annee_mois[dat[ax.ktime].month-1]]
    ax.set_title(' - '.join(title_str+title_dat))
  __set_title()
  if logo:
    logo = Image.open(apyfr_path+'/data/aquifr_logo_entites.png')
    im = OffsetImage(logo,origin='upper',zoom=0.7,zorder=10000,alpha=1)
    ab = AnnotationBbox(im,(0,1),xycoords='axes fraction',box_alignment=(0,1),frameon=False)
    ax.add_artist(ab)

  return ax if nargout()>0 else None


