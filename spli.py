#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 14:50:44 2022

@author: guillaumotl
"""
import numpy as np
from scipy.stats import norm
from netCDF4 import Dataset
import pandas as pd
from sklearn.neighbors import KernelDensity
from tqdm import trange
from matplotlib.colors import ListedColormap, BoundaryNorm
from .misc import nargout, to_date

# For parallel computing
try:
  import multiprocessing
  import multiprocessing.managers
  from functools import partial
  class MyManager(multiprocessing.managers.BaseManager):
    pass
  MyManager.register('np_zeros', np.zeros, multiprocessing.managers.ArrayProxy)
except:
  pass

spli_norm = {'period':[">10 ans sec",">5 ans sec",">2.5 ans sec","autour de\nla normale",
                       ">2.5 ans hum",">5 ans hum",">10 ans hum"],
             'values':np.array([-3.,-1.28,-0.84,-0.25,0.25,0.84,1.28,3.]),
             'colors':[(1.,0.,0.),(1.,0.5,0.),(1.,1.,0.),(0.,1.,0.),(0.,1.,1.),(0.0,0.5,1.),(0.,0.,1.)]}


def compute_monthly_CDF(dat,data,months=None,period=['1991-08-01','2020-07-31'],Nbin=512,ncfile=None,progress=True):
  '''
  Computes the monthly CDF of data
  IN:
  * dat                 : dates, array((nb_dates))
  * data                : monthly data, array((nb_dates,nb_pts))
  * months   (optional) : list of months (starting from 1 for January)
  * period   (optional) : start and end years
  * Nbin     (optional) : number of bins for the cdf
  * ncfile   (optional) : name of the netCDF file to store the CDFs
  OUT:
  * cdf_opt_bw_lin: cdf, array((Nbin,nb_pts))
  * min1, max1: min and max of data, arrays((nb_pts))
  REMARKS:
  * if ncfile is given, all months are considered
  '''

  if not hasattr(dat,'__iter__'): dat = [dat]
  dat = pd.DatetimeIndex(dat)
  if months is None or ncfile is not None: months = np.arange(1,13)
  months = np.array(months).astype(int)
  nb_months = len(months)
  period = to_date(period)

  use_only_one_station = False
  if len(data.shape)==1:
    use_only_one_station = True
    data = data[:,np.newaxis]
  nb_pts = data.shape[1]
  cdf_opt_bw_lin = np.zeros((nb_months,nb_pts,Nbin))*np.nan
  min1, max1     = np.zeros((nb_months,nb_pts))*np.nan,np.zeros((nb_months,nb_pts))*np.nan

  for mo in months:
    id_dates_mo = (dat>=period[0])*(dat<=period[1])*(dat.month==mo)
    data_mo = data[id_dates_mo,:]
    imo = np.where(months==mo)[0][0]
    pts = trange(nb_pts) if progress else range(nb_pts)

    for pt in pts:
      # Optimized estimator of bandwidth [Silverman's rule of thumb]
      Inan = (~np.isnan(data_mo[:,pt]))
      if Inan.sum()>5: #0.5*len(data_mo[:,pt]):
        opt_bw = np.std(data_mo[Inan,pt])*(4./3./len(data_mo[Inan,pt]))**0.2
        if opt_bw==0: continue
        # Fit for Epanechnikov
        kde_opt_bw = KernelDensity(kernel='epanechnikov',bandwidth=opt_bw).fit(data_mo[Inan,pt][:,np.newaxis])
        # PDF of data
        minval1,maxval1 = min(data_mo[Inan,pt]),max(data_mo[Inan,pt])
        min1[imo,pt],max1[imo,pt] = minval1, maxval1
        #min1[imo,pt],max1[imo,pt] = minval1 - 0.2 * (maxval1 - minval1), maxval1 + 0.2 * (maxval1 - minval1)
        dens_opt_bw_lin = np.exp(kde_opt_bw.score_samples(np.linspace(min1[imo,pt],max1[imo,pt],Nbin)[:,np.newaxis]))
        # CDF of data
        if np.sum(dens_opt_bw_lin)>0:
          cdf_opt_bw_lin[imo,pt,:] = np.cumsum(dens_opt_bw_lin)/np.sum(dens_opt_bw_lin)
          cdf_opt_bw_lin[imo,pt,:] = (cdf_opt_bw_lin[imo,pt,:] - np.min(cdf_opt_bw_lin[imo,pt,:])) / \
                                     (np.max(cdf_opt_bw_lin[imo,pt,:]) - np.min(cdf_opt_bw_lin[imo,pt,:]))
  if ncfile is not None:
    write_monthly_CDF(cdf_opt_bw_lin,min1,max1,ncfile)
  if use_only_one_station:
    cdf_opt_bw_lin,min1,max1 = cdf_opt_bw_lin[:,0,:],min1[:,0],max1[:,0]
  return [cdf_opt_bw_lin,min1,max1] if nargout()>0 else None


def compute_monthly_CDF_parallel(dat,data,months=None,period=['1991-08-01','2020-07-31'],Nbin=512,ncfile=None):
  '''
  Computes the monthly CDF of data using parallelization
  IN:
  * dat                 : dates, array((nb_dates))
  * data                : monthly data, array((nb_dates,nb_pts))
  * months   (optional) : list of months (starting from 1 for January)
  * period   (optional) : start and end years
  * Nbin     (optional) : number of bins for the cdf
  * ncfile   (optional) : name of the netCDF file to store the CDFs
  OUT:
  * cdf_opt_bw_lin: cdf, array((Nbin,nb_pts))
  * min1, max1: min and max of data, arrays((nb_pts))
  REMARKS:
  * if ncfile is given, all months are considered
  '''
  if months is None or ncfile is not None: months = np.arange(1,13)
  months = np.array(months).astype(int)
  nb_months = len(months)
  period = to_date(period)
  use_only_one_station = False
  if len(data.shape)==1:
    use_only_one_station = True
    data = data[:,np.newaxis]
  nb_pts = data.shape[1]

  m = MyManager()
  m.start()
  cdf_opt_bw_lin = m.np_zeros((nb_months,nb_pts,Nbin))
  min1, max1     = m.np_zeros((nb_months,nb_pts)),m.np_zeros((nb_months,nb_pts))
  global compute_CDF_single_month
  def compute_CDF_single_month(cdf_opt_bw_lin,min1,max1,mo):
    imo = np.where(months==mo)[0][0]
    cdf_opt_bw_lin[imo],min1[imo],max1[imo] = compute_monthly_CDF(dat,data,months=[mo],period=period,Nbin=Nbin)
  pool = multiprocessing.Pool(processes=multiprocessing.cpu_count())
  func = partial(compute_CDF_single_month,cdf_opt_bw_lin,min1,max1)
  pool.map(func,(mo for mo in months))
  pool.close()
  cdf_opt_bw_lin,min1,max1 = np.array(cdf_opt_bw_lin),np.array(min1),np.array(max1)
  m.shutdown()

  if ncfile is not None:
    write_monthly_CDF(cdf_opt_bw_lin,min1,max1,ncfile)
  if use_only_one_station:
    cdf_opt_bw_lin,min1,max1 = cdf_opt_bw_lin[:,0,:],min1[:,0],max1[:,0]
  return [cdf_opt_bw_lin,min1,max1] if nargout()>0 else None


def write_monthly_CDF(cdf_opt_bw_lin,min1,max1,ncfile):
  '''
  Write CDFs into netCDF file
  '''
  nb_months,nb_pts,Nbin = cdf_opt_bw_lin.shape
  with Dataset(ncfile,'w') as nc:
    nc.createDimension('months',nb_months)
    nc.createDimension('points',nb_pts)
    nc.createDimension('bins',Nbin)
    nc.createVariable('months','f4','months')
    nc['months'][:] = np.arange(1,nb_months+1)
    nc.createVariable('cdf','f4',('months','points','bins'))
    nc['cdf'][:] = cdf_opt_bw_lin
    nc.createVariable('min','f4',('months','points'))
    nc['min'][:] = min1
    nc.createVariable('max','f4',('months','points'))
    nc['max'][:] = max1
  return


def compute_monthly_SPLI(dat,data,cdf=None,ncfile=None,progress=True,remove_nan=False):
  '''
  Computes the monthy SPLI
  IN:
  * dat                 : dates, array((nb_dates))
  * data                : monthly data, array((nb_dates,nb_pts))
  * cdf      (optional) : precomputed CDFs tuple (cdf,min,max)
  * ncfile   (optional) : name of the netCDF file where the CDFs are stored
  OUT:
  * spli_data: monthly SPLI, array((nb_dates,nb_pts))
  '''

  if cdf is None and ncfile is None:
    raise Exception("Les CDF sont requises pour le calcul du SPLI.")

  if not hasattr(dat,'__iter__'): dat = [dat]
  dat = pd.DatetimeIndex(dat)
  months = np.unique(dat.month).astype(int)

  data_shape = data.shape
  if len(data.shape)==1:
    if len(dat)==1: data = data[np.newaxis,:]
    else          : data = data[:,np.newaxis]

  nb_pts = data.shape[1]
  spli_data = np.nan*np.zeros(data.shape)
  if cdf is not None:
    cdf_opt_bw_lin,min1,max1 = cdf
  else:
    with Dataset(ncfile) as nc:
      cdf_opt_bw_lin,min1,max1 = nc['cdf'][:],nc['min'][:],nc['max'][:]
  Nbin = cdf_opt_bw_lin.shape[2]

  if remove_nan: del_pts = []
  for mo in months:
    Idat = dat.month==mo
    pts = trange(nb_pts) if progress else range(nb_pts)
    if remove_nan: nb_nan = 0
    for pt in pts:
      #if max1[pt]-min1[pt]<0.002: continue # to remove pixels with very small variations
      # Interpolation of data values on new CDF
      cdf_interp_data = np.interp(data[Idat,pt],np.linspace(min1[mo-1,pt],max1[mo-1,pt],Nbin),cdf_opt_bw_lin[mo-1,pt,:])
      cdf_interp_data = np.minimum(np.maximum(cdf_interp_data,0.0001),0.9999)
      # Correspondance on Gaussian distrib
      spli_data[Idat,pt] = norm.ppf(cdf_interp_data)

      # Remove cells where cdf is not well computed (three cells in odic_seine) where cdf is flat for several months
      if remove_nan and np.isnan(np.sum(cdf_opt_bw_lin[pt])):
        del_pts.append(pt)
        nb_nan += 1
  if remove_nan and nb_nan > 0:
    spli_data[:,np.unique(np.array(del_pts))] = np.nan
    #print('These points are excluded : ', np.unique(np.array(del_pts)))

  return spli_data.reshape(data_shape)


def compute_monthly_SPLI_parallel(dat,data,**kwargs):
  '''
  Computes the monthy SPLI using parallelization
  IN:
  * dat                 : dates, array((nb_dates))
  * data                : monthly data, array((nb_dates,nb_pts))
  * cdf      (optional) : precomputed CDFs tuple (cdf,min,max)
  * ncfile   (optional) : name of the netCDF file where the CDFs are stored
  OUT:
  * spli_data: monthly SPLI, array((nb_dates,nb_pts))
  '''
  if not hasattr(dat,'__iter__'): dat = [dat]
  dat = pd.DatetimeIndex(dat)
  months = np.unique(dat.month).astype(int)
  data_shape = data.shape
  if len(data.shape)==1: data = data[:,np.newaxis]

  m = MyManager()
  m.start()
  spli_data = m.np_zeros(data.shape)
  global compute_SPLI_single_month
  def compute_SPLI_single_month(spli_data,mo):
    Idat = dat.month==mo
    spli_data[Idat] = compute_monthly_SPLI(dat[Idat],data[Idat],**kwargs)
  pool = multiprocessing.Pool(processes=multiprocessing.cpu_count())
  func = partial(compute_SPLI_single_month,spli_data)
  pool.map(func,(mo for mo in months))
  pool.close()
  spli_data = spli_data._getvalue()
  m.shutdown()

  return spli_data.reshape(data_shape)

