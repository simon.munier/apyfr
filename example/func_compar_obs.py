#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 22 11:15:10 2022

@author: guillaumotl
"""

import numpy as np
import numpy.ma as ma


# Computing KGE, NSE, RMSE...

def KGE(s,o):
    """
    Kling Gupta Efficiency (Kling et al., 2012, http://dx.doi.org/10.1016/j.jhydrol.2012.01.011)
    input:
        s: simulated
        o: observed
    output:
        KGE: Kling Gupta Efficiency
    """

    B = np.nanmean(s) / np.nanmean(o)
    y = (np.nanstd(s) / np.nanmean(s)) / (np.nanstd(o) / np.nanmean(o))
    #r = np.corrcoef(o, s)[0,1]
    r = np.empty(s.shape)
    s1 = ma.masked_invalid(s[:])
    o1 = ma.masked_invalid(o[:])
    msk = (~o1.mask & ~s1.mask)
    r = ma.corrcoef(o1[msk], s1[msk]).data[0,1]
    KGE = 1 - np.sqrt((r - 1) ** 2 + (B - 1) ** 2 + (y - 1) ** 2)

    return KGE

def NS(s,o):
    """
    Nash-Sutcliffe efficiency coefficient
    input:
        s: simulated
        o: observed
    output:
        NS: Nash-Sutcliffe efficient coefficient
    """

    return 1 - np.nansum((s-o)**2)/np.nansum((o-np.nanmean(o))**2)

def RMSE(s, o):
    """
    Root Mean Square Error
    input:
        s: simulated
        o: observed
    output:
        RMSE: Root Mean Square Errorexit
    """
    RMSE = np.sqrt(np.nanmean((s-o)**2))
    return RMSE

def RMSE_global(s, o):
    """
    Root Mean Square Error for several points
    input:
        s: simulated
        o: observed
    output:
        RMSE: Root Mean Square Errorexit
    """
    RMSE = np.sqrt(np.nanmean((s-o)**2, axis=0))
    return RMSE

def QMNA5(s, o, dates_compar):
    """data are already at monthly scale"""

    return_period = 5  # years

    # need to gather each month and build an array for each month
    datmonth = np.zeros(len(dates_compar))
    for mm in range(len(dates_compar)):
        datmonth[mm] = dates_compar[mm].month
    months = np.unique(datmonth)

    QMNA5_sim = np.zeros(12)
    QMNA5_obs = np.zeros(12)
    for mo in months:
        Idat = datmonth==mo
        QMNA5_sim[int(mo-1)] = np.nanpercentile(s[Idat], 100.0/return_period)
        QMNA5_obs[int(mo-1)] = np.nanpercentile(o[Idat], 100.0/return_period)

    # comparing the two timeseries (12 months)
    criter = KGE(QMNA5_sim, QMNA5_obs)

    return criter

def moving_average(arr, window_size):

    if window_size == 1:
        return arr

    else:
        i = int(window_size/2)
        # Initialize an empty list to store moving averages
        moving_averages = np.zeros(len(arr))
        moving_averages [0:int(window_size/2)] = arr[0:int(window_size/2)]
        moving_averages [-int(window_size/2):] = arr[-int(window_size/2):]
        # Loop through the array to consider every window of size 3
        while i < len(arr) - int(window_size/2):
            # Calculate the average of current window
            moving_averages[i] = np.nansum(arr[i-int(window_size/2):i+int(window_size/2)+1]) / np.count_nonzero(~np.isnan(arr[i-int(window_size/2):i+int(window_size/2)+1]))
            i += 1
        return moving_averages


def compare_sim_obs_piezo(df_sim, df_obs):
    """
    From simulated and observed monthly water table data,
    it returns data for the same dates
    arg:
        df_sim = dataframe of simulated data
        df_obs = dataframe of observed data
    outputs:
        date_comparison
        wt_sim: simulated data for each date in date_comparison
        wt_obs: observed data for each date in date_comparison
    """

    # Searching the first date
    if df_obs.index[0] < df_sim.index[0]:  # the first date of the simulation will be the first date for the comparison
        id_start_sim = 0
        id_start_obs = np.argmin(np.abs(df_sim.index[0] - df_obs.index))
    else:
        id_start_sim = np.argmin(np.abs(df_sim.index - df_obs.index[0]))
        id_start_obs = 0
    # Searching the last date
    if df_obs.index[-1] < df_sim.index[-1]:
        id_end_sim = np.argmin(np.abs(df_sim.index - df_obs.index[-1])) + 1
        id_end_obs = len(df_obs.index)
    else:  # the last date of the simulation will be last date for the comparison
        id_end_sim = len(df_sim.index)
        id_end_obs = np.argmin(np.abs(df_sim.index[-1] - df_obs.index)) + 1

    # we keep the station if we have more than 10 years of data
    if id_end_obs - id_start_obs >= 10 * 12:

        # then, we keep the station if we have enough data
        temp_data = df_obs.values[id_start_obs:id_end_obs]
        if np.count_nonzero(~np.isnan(temp_data)) > 5 * 12:
            date_comparison = df_sim.index[id_start_sim:id_end_sim]
            wt_sim = df_sim.values[id_start_sim:id_end_sim]
            wt_obs = df_obs.values[id_start_obs:id_end_obs]
        else:
            date_comparison = np.nan
            wt_sim = np.nan
            wt_obs = np.nan
    else:
        date_comparison = np.nan
        wt_sim = np.nan
        wt_obs = np.nan

    return date_comparison, wt_sim, wt_obs