#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 11 11:51:22 2021
@author: andre.mounier@ens-paris-saclay.fr

updated by LG (CNRM) on 22 September 2022

    Script principal
"""

"""
This code compares simulated and observed groundwater levels.
It compares all applications inside the simulated folder
Requirements:
    - Simulated data should be stored in data_path inside Reanalyses.py
    - The simulated variable is 'h_piezos' (monthly or daily ? or whatever ?)
    - The folder 'Donnees_piezo' in the previous directory need to be deleted to
      run the code
    - Currently, we can not compare water table depth ! Only water table
    - The comparison include SPLI comparison
"""


# =============================================================================
# IMPORT LIBRARIES
# =============================================================================

from tqdm import tqdm

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import timedelta

from Observations_piezo     import (Update_all,
                              Display_chronicle,
                              Get_criteria_list,
                              to_date,
                              Get_monthly_chronicle_single_month,
                              Get_chronicle,
                              Get_monthly_chronicle,
                              Get_period_limits,
                              Display_active_pzo_evolution_by_appli,
                              Download_all)
from Piezometres      import (Get_pzo_list,
                              Get_infos,
                              Which_appli,
                              Get_filtered_pzo_list,
                              Get_appli_data)
from Reanalyses import Get_REA_monthly_chronicle

# to compare observed and simulated water tables
from func_compar_obs import RMSE, compare_sim_obs_piezo, moving_average

# to compute SPLI indices
from spli_functions import compute_monthly_CDF, compute_monthly_SPLI
import cftime

# Defining figures size
plt.rcParams["figure.figsize"] = (12, 9)

# Choose a name for the folder where results are printed
name_output_folder = 'watertablecomparison_REA_V13_1958_2022_v3'
try:
    os.makedirs(name_output_folder)
except ValueError:
    print("The output folder ", name_output_folder, " already exists.")


# =============================================================================
# INFORMATION ABOUT THE SIMULATION
# =============================================================================

# Directory containing the simulation:
#simulated_data_path = '/cnrm/surface/munier/AquiFR/Results/REA_V13_1958_2021/'
simulated_data_path = '/cnrm/surface/guillaumotl/REA_V13_1958_2022'
simulated_data_path = '/home/guillaumotl/AquiFR/Results_Luca/REA_1958_2022/'

# Applications where we want comparisons
applis_mart = ['mart_som','mart_poc','mart_bno','mart_npc','mart_teg'] #,'mart_als',]  # no stations for teg ???
applis_odic = ['odic_somme','odic_loire','odic_seine',
               'odic_seine-eure','odic_seine-oise','odic_marne-oise','odic_marne-loing']  # 'odic_basse-normandie'
applis = applis_mart + applis_odic
applis2 = ['som','poc','bno','npc', 'teg', 'somme','loire','seine','seine-eure','seine-oise','marne-oise','marne-loing']

# No need to modify this
models_names = {'teg':'mart', 'poc':'mart', 'som':'mart', 'bno':'mart', 'als':'mart', 'npc':'mart',
                'seine':'odic', 'seine-eure':'odic', 'seine-oise':'odic', 'marne-oise':'odic', 'marne-loing':'odic', 'somme':'odic', 'loire':'odic', 'basse-normandie':'odic'}

# Observed and simulated piezometric data will be saved inside a ".npy" file
name_saved_npy_data = 'Piezometric_data_V3' + '.npy'

# If True, the codes will upload 'name_saved_npy_data' 'npy' file
# without extracting new simulated or observed data
data_already_extracted = False

# =============================================================================
# UPLOADING WATER TABLES OBSERVED IN BOREHOLES
# =============================================================================


if data_already_extracted:

    Piezometric_data = np.load(name_saved_npy_data, allow_pickle=True)
    Piezometric_data = Piezometric_data[()]  # to convert into a dictionnary as the original 'Piezmetric_data' dictionary

else:
    liste_pzo = Get_pzo_list()  # list of piezometers in the AQUIFR domain

    # Dictionnary containing: keys = piezo_names_application, values = list of (dates, wt_simu, wt_obs)
    Piezometric_data = {}
    applications = []
    pz_names = []
    #'00245X0002_S1_somme'

    # Coordinates of the piezometers
    x_pz = []
    y_pz = []

    for pzo in tqdm(liste_pzo):  # for each piezometer

        # Keep piezometers only if they are inside the selected applications
        appli = models_names[Which_appli(pzo)[0]] + '_' + Which_appli(pzo)[0]
        if appli in applis:

            df_obs = Get_monthly_chronicle(pzo)  # gather all water tables data at monthly scale

            if df_obs is not None:  # because some stations are 'not available'

                # Second, we need to upload simulated water tables
                res = Get_REA_monthly_chronicle(simulated_data_path, pzo)

                if res[0][0] is not None:  # because some stations are 'not available'

                    corresponding_appli = np.shape(res)[0]  # We can have several application for the same piezometer
                    if corresponding_appli == 1:
                        df_sim = res[0][1]
                        key_name = pzo + '_' + str(res[0][0])
                        pzo_informations = Get_infos(pzo)
                        applications.append(str(res[0][0]))
                        pz_names.append(key_name)
#                        print(str(res[0][0]))
                        Piezometric_data[key_name] = compare_sim_obs_piezo(df_sim, df_obs), res[0][0], pzo_informations['x_L2E'].values[0], pzo_informations['y_L2E'].values[0]  # The simulated and observed dates need to correspond
#                        print(np.count_nonzero(~np.isnan(Piezometric_data[key_name][0][2])))

                        # Get coordinates:
                        pzo_informations = Get_infos(pzo)
                        x_pz.append(pzo_informations['x_L2E'].values[0])
                        y_pz.append(pzo_informations['y_L2E'].values[0])

                    if corresponding_appli > 1:
                        for i in range(corresponding_appli):
                            df_sim = res[i][1]
                            key_name = pzo + '_' + str(res[i][0])
                            pzo_informations = Get_infos(pzo)
                            applications.append(str(res[i][0]))
                            pz_names.append(key_name)
#                            print(str(res[i][0]))
                            Piezometric_data[key_name] = compare_sim_obs_piezo(df_sim, df_obs), res[i][0], pzo_informations['x_L2E'].values[i], pzo_informations['y_L2E'].values[i]  # The simulated and observed dates need to correspond
#                            print(np.count_nonzero(~np.isnan(Piezometric_data[key_name][0][2])))

                            # Get coordinates
                            x_pz.append(pzo_informations['x_L2E'].values[0])
                            y_pz.append(pzo_informations['y_L2E'].values[0])

    # Then, the piezometric data can be saved in order to avoid to repeat this step
    np.save(name_saved_npy_data, Piezometric_data)


# =============================================================================
# COMPUTING SPLI ON OBSERVED AND SIMULATED WATER TABLE
# =============================================================================

print('Computing SPLI')
# SPLI indices will be saved inside dictionaries (key=piezometer name)
simulated_spli = {}
observed_spli = {}

window_size = 1  # smoothing monthly streamflow with a moving average (no smoothing if window_size = 1)

for pz in Piezometric_data:  # for each piezometers

    # Initializing dictionaries
    observed_spli[pz] = {}
    simulated_spli[pz] = {}

    # SPLI is computed if we have enough observed data to compute CDF
    if np.count_nonzero(~np.isnan(Piezometric_data[str(pz)][0][2])) >= 12 * 10:  # if at least 10 years of data

        # Defining dates
        start_month = Piezometric_data[str(pz)][0][0][0] - timedelta(days=15)  # required because monthly water table data correspond to the 15th of each month
        end_month = Piezometric_data[str(pz)][0][0][-1]
        date_spli = pd.date_range(start=str(start_month)[0:10], end=str(end_month)[0:10], freq='MS') #+pd.DateOffset(days=14)
        date_spliV2 = []

        for mm in range(len(date_spli)):
            temp_date = str()
            date_spliV2.append(cftime.DatetimeGregorian(date_spli[mm].year, date_spli[mm].month, date_spli[mm].day))
        date_spliV2 = np.array(date_spliV2)
        period = [start_month.year, end_month.year]

        # Computing CDFs (cumulative density function first) for observed and simulated streamflow
        [cdf_obs, min1_obs, max1_obs] = compute_monthly_CDF(date_spliV2, moving_average(Piezometric_data[str(pz)][0][2], window_size),
                                                      period=period, ncfile=None, use_only_one_station=True)
        [cdf_sim, min1_sim, max1_sim] = compute_monthly_CDF(date_spliV2, moving_average(Piezometric_data[str(pz)][0][1], window_size),
                                                      period=period, ncfile=None, use_only_one_station=True)

        # Computing monthly SPLI indices for observed and simulated streamflow
        spli_obs = compute_monthly_SPLI(date_spliV2, moving_average(Piezometric_data[str(pz)][0][2], window_size),
                                        CDF_ncfile=[cdf_obs, min1_obs, max1_obs], use_netcdf=False, use_only_one_station=True)
        spli_sim = compute_monthly_SPLI(date_spliV2, moving_average(Piezometric_data[str(pz)][0][1], window_size),
                                        CDF_ncfile=[cdf_sim, min1_sim, max1_sim], use_netcdf=False, use_only_one_station=True)

        # Filling dictionary
        simulated_spli[str(pz)] = spli_sim
        observed_spli[str(pz)] = spli_obs

    else:
        simulated_spli[str(pz)] = np.nan
        observed_spli[str(pz)] = np.nan


# =============================================================================
# COMPARING OBSERVED AND SIMULATED WATER TABLES
# =============================================================================

# Computing the normalized RMSE after removing the bias
# Computing bias (error between mean observed and simulated water table at each borehole)
RMSE_pz = np.zeros(len(Piezometric_data.keys())) * np.nan
erreur = np.zeros(len(Piezometric_data.keys())) * np.nan
RMSEspli = np.zeros(len(Piezometric_data.keys()))  * np.nan  # this one is to compare SPLI timeseries (not water tables)
associated_index = {}  # the index need to be associated with piezometer name
cnt = 0
for pz in Piezometric_data:

    if np.count_nonzero(~np.isnan(Piezometric_data[pz][0][2])) >= 12 * 10:  # if at least 10 years of data

        RMSE_pz[cnt] = RMSE(Piezometric_data[str(pz)][0][2] - np.nanmean(Piezometric_data[str(pz)][0][2]), Piezometric_data[str(pz)][0][1] - np.nanmean(Piezometric_data[str(pz)][0][1]))
        RMSE_pz[cnt] = RMSE_pz[cnt] / np.nanstd(Piezometric_data[str(pz)][0][2])  # Normalizing by standard deviation of observed data

        erreur[cnt] = np.nanmean(Piezometric_data[str(pz)][0][1]) - np.nanmean(Piezometric_data[str(pz)][0][2])

        # Computing criteria for SPLI, # RMSE is normalized by standard deviation of observed SPLI
        RMSEspli[cnt] = RMSE(simulated_spli[pz], observed_spli[pz]) / np.nanstd(observed_spli[pz])

    associated_index[str(pz)] = cnt

    cnt += 1

RMSE_pz2 = RMSE_pz[~np.isnan(RMSE_pz)]
#RMSE_pz2[RMSE_pz2 > 3] = 3  # To focus the plots on the most interesting part (like in Vergnes et al., 2020)

erreur_abs = np.abs(erreur)
# Removing wells where the bias is too big
#erreur[erreur_abs > 50] = np.nan
#erreur_abs[erreur_abs > 50] = np.nan


for app in range(len(applis2)):
    sumpz = 0
    medianRMSE = []
    medianbias = []

    for ii in range(len(applications)):

        if applications[ii] == applis2[app]: # this is the right application

            if np.count_nonzero(~np.isnan(Piezometric_data[pz_names[ii]][0][2])) >= 12 * 10:  # if at least 10 years of data

                sumpz += 1
                medianRMSE.append(RMSE_pz[ii])
                medianbias.append(erreur[ii])

    print('Number of piezometers used for ', applis2[app], ' : ', sumpz)
    print('          median of RMSE values : ', np.nanmedian(np.array(medianRMSE)))
    print('          median of bias ', np.nanmedian(np.array(medianbias)))


# =============================================================================
# DISPLAY RESULTS
# =============================================================================

#  Plot water table and SPLI for two piezometers

plt.figure()
pz = '02982X0013_S1_marne-loing'
plt.subplot(2, 1, 1)
window_size = 1  # smoothing monthly streamflow with a moving average (no smoothing if window_size = 1)
plt.plot_date(Piezometric_data[str(pz)][0][0], moving_average(Piezometric_data[str(pz)][0][2], window_size),
              label='observed', marker=None, linewidth=3, color='k', linestyle='-')
plt.plot_date(Piezometric_data[str(pz)][0][0], moving_average(Piezometric_data[str(pz)][0][1], window_size),
              label='simulated', marker=None, linewidth=2, color='r', linestyle='-')
plt.grid()
plt.legend()
plt.ylabel('Water table [m]', fontsize=14)
txt_title = 'piezometer ' + str(pz) + ' : nRMSE = ' + str(round(RMSE_pz[associated_index[str(pz)]], 2))
plt.title(txt_title, fontsize=14)

plt.subplot(2, 1, 2)
plt.plot_date(Piezometric_data[str(pz)][0][0], observed_spli[str(pz)],
              label='observed', marker=None, linewidth=3, color='k', linestyle='-')
plt.plot_date(Piezometric_data[str(pz)][0][0], simulated_spli[str(pz)],
              label='simulated', marker=None, linewidth=2, color='r', linestyle='-')
plt.grid()
plt.legend()
plt.ylabel('SPLI', fontsize=14)
plt.ylim(-3, 3)
txt_title = 'piezometer ' + str(pz) + ' : nRMSE(SPLI) = ' + str(round(RMSEspli[associated_index[str(pz)]], 2))
plt.title(txt_title, fontsize=14)

# saving figure
plt.savefig('{0}/examplePLI.png'.format(name_output_folder))

plt.figure()
pz = '03287X0018_S1_seine'
plt.subplot(2, 1, 1)
window_size = 1  # smoothing monthly streamflow with a moving average (no smoothing if window_size = 1)
plt.plot_date(Piezometric_data[str(pz)][0][0], moving_average(Piezometric_data[str(pz)][0][2], window_size),
              label='observed', marker=None, linewidth=3, color='k', linestyle='-')
plt.plot_date(Piezometric_data[str(pz)][0][0], moving_average(Piezometric_data[str(pz)][0][1], window_size),
              label='simulated', marker=None, linewidth=2, color='r', linestyle='-')
plt.grid()
plt.legend()
plt.yscale('log')
plt.ylabel('Water table  [m]', fontsize=14)
txt_title = 'piezometer ' + str(pz) + ' : nRMSE = ' + str(round(RMSE_pz[associated_index[str(pz)]], 2))
plt.title(txt_title, fontsize=14)

plt.subplot(2, 1, 2)
plt.plot_date(Piezometric_data[str(pz)][0][0], observed_spli[str(pz)],
              label='observed', marker=None, linewidth=3, color='k', linestyle='-')
plt.plot_date(Piezometric_data[str(pz)][0][0], simulated_spli[str(pz)],
              label='simulated', marker=None, linewidth=2, color='r', linestyle='-')
plt.grid()
plt.legend()
plt.ylabel('SPLI', fontsize=14)
plt.ylim(-3, 3)
txt_title = 'piezometer ' + str(pz) + ' : nRMSE(SPLI) = ' + str(round(RMSEspli[associated_index[str(pz)]], 2))
plt.title(txt_title, fontsize=14)

# saving figure
plt.savefig('{0}/exampleSDI2.png'.format(name_output_folder))


# Plot time-averaged simulated VS observed absolute water table
plt.figure()
plt.plot([-100, 500], [-100, 500], 'k', linewidth=2)  # plot the 1/1 line in black
for i in Piezometric_data.keys():
    plt.scatter(np.nanmean(Piezometric_data[str(i)][0][2]), np.nanmean(Piezometric_data[str(i)][0][1]), 5, 'b')

title_txt = 'Median of the absolute error : ' + str(np.round(np.nanmedian(erreur_abs), 1)) + ' m'
plt.title(title_txt)
plt.xlabel('Mean observed water table [masl]', fontsize=16)
plt.ylabel('Mean simulated water table [masl]', fontsize=16)
plt.grid()
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.show()
plt.xlim(-100, 500)
plt.ylim(-100, 500)
plt.axis('equal')

# saving figure
plt.savefig('{0}/meanObsVSmeanSim.png'.format(name_output_folder))

# Plot the cumulative distribution of absolute biases
erreur_abs2 = erreur_abs[~np.isnan(erreur_abs)]
erreur_abs2[erreur_abs2 > 20] = 20   # (like in Vergnes et al., 2020)

plt.figure()
plt.plot(np.sort(erreur_abs2), 100.0 * np.arange(1, len(erreur_abs2) + 1) / len(erreur_abs2))
plt.xlabel('Values of the bias [m]', fontsize=16)
plt.ylabel('Number of piezometers [%]', fontsize=16)
plt.grid()
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.xlim(0, 18)

# saving figure
plt.savefig('{0}/cumulativeBIAS.png'.format(name_output_folder))

# Display the cumulative distribution of nRMSE for water table and SPLI:
plt.figure()
plt.plot(np.sort(RMSE_pz2), 100.0 * np.arange(1, len(RMSE_pz2) + 1) / len(RMSE_pz2), label='nRMSE on water table')
plt.plot(np.sort(RMSEspli[~np.isnan(RMSEspli)]), 100 * np.arange(1, len(RMSEspli[~np.isnan(RMSEspli)]) + 1) / len(RMSEspli[~np.isnan(RMSEspli)]), label='nRMSE on SPLI')
plt.xlabel('Values of the criteria', fontsize=16)
plt.ylabel('Number of piezometers [%]', fontsize=16)
plt.title('Cumulative distribution of several criteria (water table)', fontsize=16)
plt.legend(fontsize=16)
plt.grid()
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.xlim(0, 3)

# saving figure
plt.savefig('{0}/cumulativeRMSE.png'.format(name_output_folder))

# Display the map of the error
figw_max,figh_max = 1000,1000
fig_margin = (20,20,20,30) # (left,right,bottom,top)
colorbar_width = 160
fig_dpi = 100
from apyfr.maps import blank_map
from apyfr.maps import available_kwargs
from apyfr.maps import L2E_Proj
from matplotlib import colors
from matplotlib import colorbar

# Putting x and y for each piezometer in two 1D arrays
cnt = 0
x_pz = np.zeros(len(Piezometric_data.keys()))
y_pz = np.zeros(len(Piezometric_data.keys()))
for pz in Piezometric_data:
    x_pz[cnt] = Piezometric_data[str(pz)][2]
    y_pz[cnt] = Piezometric_data[str(pz)][3]
    cnt += 1

L2E = L2E_Proj()

extent = None
ax = blank_map(applis=applis,extent=extent,zone=None,syrah=None,
                       draw_colorbar=True)

values = erreur

Inan = np.isfinite(values)
#values[~Inan] = 0
values = np.where(Inan, values, 0)
#values[values == 0] = np.nan

vlim = np.nanmin(values[Inan]), np.nanmax(values[Inan])
vlim = -10, 10
#vlim = np.nanpercentile(values, 5), np.nanpercentile(values, 95)

cmap  = plt.cm.viridis
norm = colors.Normalize(vmin=vlim[0], vmax=vlim[1])
#norm = colors.LogNorm(vmin=vlim[0], vmax=vlim[1])

hnan = ax.scatter(x_pz[~Inan], y_pz[~Inan],5,[.5,.5,.5],
                  marker='s',lw=0,transform=L2E)
h = ax.scatter(x_pz[Inan], y_pz[Inan], 15, values[Inan], marker='s',
               lw=0,transform=L2E,cmap=cmap,norm=norm)
#h = ax.scatter(coord_q_deb[:,0],coord_q_deb[:,1],5, values,marker='s',lw=0,transform=L2E,cmap=cmap,norm=norm)

figw,figh = ax.figure.get_size_inches()*ax.figure.get_dpi()
axw,axh = figw*ax.get_position().width,figh*ax.get_position().height
cbar_ax = plt.axes([(fig_margin[0]+axw+20)/figw,fig_margin[2]/figh,30/figw,axh/figh])
cb = colorbar.ColorbarBase(cbar_ax,cmap=cmap,norm=norm,spacing='uniform',extend='both')
cb.ax.tick_params(labelsize=10,size=0)
title_colorbar = 'Water table bias [m] (>0 when overestimated)'
cb.set_label(title_colorbar, fontsize=14)

# saving figure
plt.savefig('{0}/map_piezometers.png'.format(name_output_folder))

plt.show()


"""import matplotlib as mpl

mpl.rcParams['figure.dpi']  = 300
mpl.rcParams['savefig.dpi'] = 300
mpl.rcParams['font.size']   = 12
"""

"""# Définition des piézomètres récurrents
pzo_1 = '00167X0001_P1'
pzo_2 = '00326X0018_P'
pzo_3 = '02558X0034_P'
pzo_4 = '01086X0011_LS4'

VIPs = [pzo_1,pzo_2,pzo_3,pzo_4]"""


"""
liste_pzo_crit_fort   = Get_criteria_list(infos = False)
liste_pzo_crit_faible = Get_criteria_list(years=[2020,2021], duration=3, tolerance = 3000, infos=False)
liste_pzo_crit_moyen  = Get_criteria_list(years=[1981,2010,2020,2021], duration=3, tolerance = 3000, infos=False)
liste_pzo_no_crit     = Get_pzo_list()

# =============================================================================
# Affichage des graphes du rapport
# =============================================================================

# Map_pzo(choice = 1,add_VIP=True, save=False)
# Map_forecast_refREA(liste_pzo_crit_faible,save=False)
# Map_forecast(liste_pzo_crit_faible,save=False)
# Map_observation(liste_pzo_crit_faible, save=False)
# Maps_comparison_OBS_REA(liste_pzo_no_crit, save=False)
# Map_correlation(liste_pzo_crit_faible, save=False)

for pzo in VIPs:
    # Display_REA_monthly_chronicle(pzo,save=False,add_OBS=False)
    # Display_forecast_H(pzo,save=False)
    # Display_chronicle(pzo,save=False)
    # Display_active_pzo_evolution_by_appli(save=False)
    # Display_CDF_refOBS(pzo,save=False)
    # Normal_distribution(save=False)
    # Display_chronicle_SPLI_refREA(pzo,add_OBS=True, save=False)
    # Display_SPLI_comparison(pzo,save=False)
    # Display_forecast_SPLI_refREA(pzo,save=False)
    # Display_milestones_forecast(pzo,save=False)
    # Display_REA_milestones_forecast(pzo,save=False)
    # Display_forecast_SPLI_AquiFR(pzo,save=False)
    continue

# Display_correlation_evolution(liste_pzo_crit_faible,save=False)
# Display_correlation_REA_evolution(liste_pzo_crit_faible,save=False)
# Display_milestones_correlation(liste_pzo_crit_faible,save=False)
# Display_REA_milestones_correlation(liste_pzo_crit_faible,save=False)
# Display_REA_initial_gap_evolution_by_model(save=False)

# Display_drought_chronicle_by_appli(save=False)
# Display_watershed_evolution(save=False)

# MK_REA = Compute_REA_Mann_Kendall_by_appli(period = [to_date('1958-01-01'), to_date('2016-12-31')])

# =============================================================================
# Autres
# =============================================================================
"""






