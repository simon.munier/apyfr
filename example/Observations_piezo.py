#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 12 11:20:28 2021
@author: andre.mounier@ens-paris-saclay.fr

    Récupération des observations d'Adès sur la plateforme hub'eau.
"""

import requests
import pandas as pd
import json
import os
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

from Piezometres import Get_pzo_list, Which_appli, Get_appli_data,Get_filtered_pzo_list

def to_date(ch):
    """
    Passage d'une chaîne de caractère à une date.

    Parameters
    ----------
    ch : Chaîne de caractères (au format YYYY-MM-DD)
        Date en chaîne de caractères.

    Returns
    -------
    date : Date
        Même date, sous forme d'objet Date.

    """
    date = pd.to_datetime(ch, format='%Y-%m-%d')
    return date


def is_available(pzo):
    """
    Gestion des quelques piézomètres non disponibles sur hub'eau.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").

    Returns
    -------
    Booléen
        Vrai si les données du piézomètre sont téléchargeables, faux sinon.

    """
    not_in_hubeau = ['000000_TOURY','00663X0124_F_2008','00633X0132_FRPAC','00331X0051_FSEP','00197X0279_PZ2','0000000_TOURY','00624X0085_FR296','00205X0042_F1','07092X077_F']
    return not pzo in not_in_hubeau


def Get_URL(pzo, start_date, stop_date):
    """
    Obtention de l'URL de téléchargement via l'API de hub'eau.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    start_date : Date
        Date minimale des données demandées.
    stop_date : Date
        Date maximale des données demandées.

    Returns
    -------
    URL_data : Chaîne de caractères
        URL de téléchargement.

    """
    pzo = pzo.replace('_','%2F')

    URL_data_uncompleted = 'http://hubeau.eaufrance.fr/api/v1/niveaux_nappes/chroniques?code_bss={}&date_debut_mesure={}&date_fin_mesure={}&size={}'
    URL_data = URL_data_uncompleted.format(pzo, start_date.strftime('%Y-%m-%d'), stop_date.strftime('%Y-%m-%d'),1)

    analyzes_number = json.loads(requests.get(URL_data).text)['count']

    if analyzes_number > 0:
        URL_data  = URL_data_uncompleted.format(pzo, start_date.strftime('%Y-%m-%d'), stop_date.strftime('%Y-%m-%d'), analyzes_number+1)
        return URL_data
    else:
        print('\nStation {} : Pas de données'.format(pzo.replace('%2F','_')))
        return


def Download_chronicle(pzo, start_date=to_date('1899-01-01'), stop_date= pd.Timestamp.today(), save=True):
    """
    Création du fichier csv relatif à un piézomètre.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    start_date : Date, optionnel
        Date minimale des données demandées. Par défaut : to_date('1899-01-01').
    stop_date : Date, optionnel
        Date maximale des données demandées. Par défaut : pd.Timestamp.today().
    save : Booléen, optionnel
        Sauvegarde du fichier csv créé. Par défaut : True.

    Returns
    -------
    df : Dataframe
        Dataframe des dates de mesures et des hauteurs d'eau correspondantes.

    """
    if not is_available(pzo):
        print("{} : Station non disponible".format(pzo))
        return

    previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    data_path = previous_folder+'/Donnees_piezo/Observations/Hauteur/'
    if not os.path.exists(data_path): os.makedirs(data_path)
    URL = Get_URL(pzo, start_date, stop_date)
    data = json.loads(requests.get(URL).text)['data']
    df = pd.DataFrame(columns = ['Mesure','Hauteur'])
    for mesure in data:
        date = mesure['date_mesure']
        niveau = mesure['niveau_nappe_eau']
        newline = pd.DataFrame([[date,niveau]],columns = ['Mesure','Hauteur'])
        df = df.append(newline, ignore_index=True)

    df['Mesure'] = pd.to_datetime(df['Mesure'])
    df = df.set_index('Mesure')
    if save : df.to_csv(data_path + pzo + '.csv')
    return df


def Update_chronicle(pzo, infos=True):
    """
    Mise à jour des données de la chronique du piézomètre demandé.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    infos : Booléen, optionnel
        Affiche les nouvelles mesures trouvées si Vrai. Par défaut : True.

    Returns
    -------
    None.

    """
    if not is_available(pzo):
        print("{} : Station non disponible".format(pzo))
        return

    previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    data_path = previous_folder+'/Donnees_piezo/Observations/Hauteur/'
    if not os.path.exists(data_path): os.makedirs(data_path)
    if pzo + '.csv' not in os.listdir(data_path):
        Download_chronicle(pzo)

    df_hauteur = pd.read_csv(data_path+pzo+'.csv')
    df_hauteur['Mesure'] = pd.to_datetime(df_hauteur['Mesure'])
    df_hauteur = df_hauteur.set_index('Mesure')
    last_measure = pd.to_datetime(df_hauteur.index.to_list()[-1])
    today = pd.Timestamp.today()
    new_measures = Download_chronicle(pzo, start_date = last_measure, stop_date = today, save = False)
    if new_measures.index.to_list()[-1] != last_measure:
        df_hauteur = df_hauteur.append(new_measures)
        df_hauteur = df_hauteur[~df_hauteur.index.duplicated(keep='first')]
        df_hauteur.to_csv(data_path + pzo + '.csv')

        # téléchargement et enregistrement des données pour le pzo
        _ = Get_monthly_chronicle(pzo, force=True)
        if infos: print('Nouvelles mesures :\n', new_measures[1:])
        return
    if infos: print('Pas de nouvelles données pour le piézomètre {}.'.format(pzo))
    return


def Download_all():
    """
    Appel de la fonction 'Download_chronicle' pour tous les piézomètres d'AquiFR. C'est long...

    Returns
    -------
    None.

    """
    list_pzo = Get_pzo_list()
    for pzo in tqdm(list_pzo):
        Download_chronicle(pzo)
    return


def Update_all(infos=False):
    """
    Appel de la fonction 'Update_chronicle' pour tous les piézomètres d'AquiFR.

    Returns
    -------
    None.

    """
    list_pzo = Get_pzo_list()
    for pzo in tqdm(list_pzo):
        Update_chronicle(pzo, infos=infos)
        if infos: print('')
    return


def Get_chronicle(pzo, date_range=[to_date('1899-01-01'), pd.Timestamp.today()]):
    """
    Accès à la chronique d'un piézomètre entre deux années.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    date_range : Liste, optionnel
        Liste des dates minimale et maximale de la chronique. Par défaut : [to_date('1899-01-01'), pd.Timestamp.today()].

    Returns
    -------
    df_hauteur : Dataframe
        Dataframe des dates de mesures et des hauteurs d'eau correspondantes.

    """
    if not is_available(pzo):
        print("{} : Station non disponible".format(pzo))
        return

    previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    data_path = previous_folder+'/Donnees_piezo/Observations/Hauteur/'
    if not os.path.exists(data_path): os.makedirs(data_path)
    if pzo + '.csv' not in os.listdir(data_path):
        Download_chronicle(pzo)
    df_hauteur = pd.read_csv(data_path + pzo + '.csv')
    df_hauteur['Mesure'] = pd.to_datetime(df_hauteur['Mesure'])
    df_hauteur = df_hauteur.set_index('Mesure')
    df_hauteur = df_hauteur[(df_hauteur.index >= date_range[0]) & (df_hauteur.index <= date_range[1])]
    return df_hauteur


def Get_monthly_chronicle(pzo, date_range=[to_date('1899-01-01'), pd.Timestamp.today()], force=False, save_monthly_data=False):
    """
    Accès à la chronique mensualisée d'un piézomètre entre deux années.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    date_range : Liste, optionnel
        Liste des dates minimale et maximale de la chronique. Par défaut : [to_date('1899-01-01'), pd.Timestamp.today()].
    force : Booléen, optionnel
        Force le recalcul de la moyenne si Vrai, sinon, utilise le fichier enregistré (accélération du temps de calcul). Par défaut : False.

    Returns
    -------
    df_hauteur : Dataframe
        Dataframe mensuel des dates de mesures et des hauteurs d'eau correspondantes.

    """
    if not is_available(pzo):
        print("{} : Station non disponible".format(pzo))
        return

    previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    if save_monthly_data:  # modif luca
        folder_save = previous_folder + '/Donnees_piezo/Observations/Hauteur_mensuelle/'
        if not os.path.exists(folder_save): os.makedirs(folder_save)
        file_name = '{}.csv'.format(pzo)
        if file_name not in os.listdir(folder_save) or force:
            df_hauteur = Get_chronicle(pzo, date_range = [to_date('1899-01-01'), pd.Timestamp.today()])
            df_hauteur = df_hauteur.resample('1M').mean()
            df_hauteur.index = [d.replace(day = 15) for d in df_hauteur.index]
            df_hauteur.to_csv(folder_save + file_name)
        df_hauteur = pd.read_csv(folder_save + file_name, index_col = 0)
        df_hauteur.index = pd.to_datetime(df_hauteur.index)
        df_hauteur = df_hauteur[(df_hauteur.index >= date_range[0]) & (df_hauteur.index <= date_range[1])]
    else:
        df_hauteur = Get_chronicle(pzo, date_range = [to_date('1899-01-01'), pd.Timestamp.today()])
        df_hauteur = df_hauteur.resample('1M').mean()
        df_hauteur.index = [d.replace(day = 15) for d in df_hauteur.index]
        df_hauteur.index = pd.to_datetime(df_hauteur.index)
        df_hauteur = df_hauteur[(df_hauteur.index >= date_range[0]) & (df_hauteur.index <= date_range[1])]

    return df_hauteur


def Get_monthly_chronicle_single_month(pzo, month, date_range = [to_date('1899-01-01'), pd.Timestamp.today()]):
    """
    Accès à la chronique mensualisée d'un piézomètre entre deux années, pour un seul mois donné.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    month : Entier
        Mois de l'année, de 1 pour janvier à 12 pour décembre.
    date_range : Liste, optionnel
        Liste des dates minimale et maximale de la chronique. Par défaut : [to_date('1899-01-01'), pd.Timestamp.today()].

    Returns
    -------
    df_hauteur : Dataframe
        Dataframe mensuel des dates de mesures et des hauteurs d'eau correspondantes, pour un seul mois donné.

    """
    df_hauteur = Get_monthly_chronicle(pzo, date_range = date_range)
    if df_hauteur.empty : return df_hauteur
    df_hauteur = df_hauteur[df_hauteur.index.month == month]
    return df_hauteur


def Display_chronicle(pzo, date_range=[to_date('1899-01-01'), pd.Timestamp.today()], save=False):
    """
    Graphe de la série temporelle des mesures d'un piézomètre.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    date_range : Liste, optionnel
        Liste des dates minimale et maximale de la chronique. Par défaut : [to_date('1899-01-01'), pd.Timestamp.today()].
    save : Booléen, optionnel
        Sauvegarde de la chronique dans le dossier Images. The default is False.

    Returns
    -------
    None.

    """
    if not is_available(pzo):
        print("{} : Station non disponible".format(pzo))
        return

    df_hauteur = Get_chronicle(pzo, date_range)
    list_appli = Which_appli(pzo)
    title = pzo + " ({})".format(', '.join(list_appli))
    previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    image_path = previous_folder+'/Images/chronique_H/'
    if not os.path.exists(image_path): os.makedirs(image_path)

    cmap_OBS = plt.cm.get_cmap('magma')
    fig, ax = plt.subplots()
    ax.plot(df_hauteur,'.', ms = 2, color = cmap_OBS(0.6))
    ax.grid()
    ax.set_title(title)
    ax.set_ylabel("Piezometric level (m)")
    ax.yaxis.get_major_locator().set_params(integer=True)
    if save:
        plt.savefig(image_path + "chronique_H_{}.png".format(pzo),bbox_inches='tight')
        plt.close()
    else : plt.show()
    return


def Get_period_limits(pzo):
    """
    Récupération des limites disponibles pour la chronique d'un piézomètre (années complètes uniquement).

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").

    Returns
    -------
    limits : Liste
        Liste de deux éléments : date de début et date de fin.

    """
    if not is_available(pzo):
        return [np.nan]*2

    df_hauteur = Get_monthly_chronicle(pzo)
    first_complete_year = df_hauteur[df_hauteur.index.month == 1].index[0]
    last_complete_year = df_hauteur[df_hauteur.index.month == 12].index[-1]
    limits = [first_complete_year, last_complete_year]
    return limits


def Check_years(pzo, years=[2020,2021]):
    """
    Vérification de présence des données pour les années spécifiées.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    years : Liste ou entier, optionnel
        Liste des années pour lesquelles on veut des données. Par défaut : [2020,2021].

    Returns
    -------
    Booléen
        Vrai si il existe des données pour les années spécifiées, faux sinon.

    """
    if not is_available(pzo):
        print("{} : Station non disponible".format(pzo))
        return

    if type(years) == int : years = [years]
    df_hauteur = Get_chronicle(pzo)
    presence = [year in df_hauteur.index.year for year in years]
    return False not in presence


def Check_duration(pzo, duration=15):
    """
    Vérification de la longueur de la chronique (en années).

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    duration : Entier, optionnel
        Durée minimale voulue pour la chronique (en années). Par défaut : 15.

    Returns
    -------
    Booléen
        Vrai si la chronique dure plus longtemps que le nombre d'années spécifié, faux sinon.

    """
    if not is_available(pzo):
        print("{} : Station non disponible".format(pzo))
        return

    df_hauteur = Get_chronicle(pzo)
    return len(set(df_hauteur.index.year)) > duration


def Check_monthly(pzo, tolerance=15):
    """
    Vérification de la fréquence mensuelle des mesures.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    tolerance : Entier, optionnel
        Nombre de mois pouvant être manqués. Par défaut : 15.

    Returns
    -------
    Booléen
        Vrai si le nombre de mois manqués est inférieur à la tolérance, faux sinon.

    """
    if not is_available(pzo):
        print("{} : Station non disponible".format(pzo))
        return

    df_hauteur = Get_chronicle(pzo)
    measures = df_hauteur.index
    list_skipped_measures = (measures[1:]-measures[:-1]) / np.timedelta64(1, 'M')
    missed_months = sum(d for d in list_skipped_measures if d > 1)
    return missed_months < tolerance


def Get_criteria_list(years=[1981,2010,2020,2021], duration=30, tolerance=30, infos=False, save=True):
    """
    Récupération des stations dont la chronique présente certaines qualités.

    Parameters
    ----------
    years : Liste ou entier, optionnel
        Liste des années pour lesquelles on veut des données. Par défaut : [1980,2010,2020,2021].
    duration : Entier, optionnel
        Durée minimale voulue pour la chronique (en années). Par défaut : 30.
    tolerance : Entier, optionnel
        Nombre de mois pouvant être manqués. Par défaut : 30.
    infos : Booléen, optionnel
        Si Vrai, affichage du nombre de piézomètres pour chacun des critères. Par défaut : False.
    save : Booléen, optionnel
        Si Vrai, sauvegarde de la liste des piézomètres correspondants au critère. Par défaut : True.

    Returns
    -------
    List_criteria : Liste
        Liste des piézomètres répondant aux trois critères à la fois.

    """
    List_pzo = Get_pzo_list()
    if type(years) == int : years = [years]
    nb_available_station = 0
    set_years_station = set()
    set_duration_station = set()
    set_monthly_station = set()

    previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    folder_save = previous_folder + '/Donnees_piezo/Liste_critere/'
    if not os.path.exists(folder_save): os.makedirs(folder_save)
    file_name = 'pzo_years-{}_duration-{}_tolerance-{}.txt'.format('-'.join([str(i) for i in years]), duration, tolerance)
    if file_name not in os.listdir(folder_save):
        for pzo in tqdm(List_pzo):
            if is_available(pzo):
                nb_available_station +=1
                if Check_years(pzo, years): set_years_station.add(pzo)
                if Check_duration(pzo, duration): set_duration_station.add(pzo)
                if Check_monthly(pzo, tolerance): set_monthly_station.add(pzo)
        List_criteria = list(set_years_station.intersection(set_duration_station,set_monthly_station))
        if save:
            with open(folder_save + file_name, 'w') as f:
                f.write('Critères de sélection des piézomètres et fréquences de ces critères :\n')
                f.write("    - {:>3} stations sur {:>3} présentent des données en {}.\n".format(len(set_years_station),nb_available_station,', '.join([str(i) for i in years])))
                f.write("    - {:>3} stations sur {:>3} présentent plus de {} ans de données.\n".format(len(set_duration_station),nb_available_station,duration))
                f.write("    - {:>3} stations sur {:>3} présentent des données mensuelles ({} mois de tolérance).\n".format(len(set_monthly_station),nb_available_station, tolerance))
                f.write("    - {:>3} stations sur {:>3} présentent toutes ces qualités.\n\n".format(len(List_criteria),nb_available_station))
                for pzo in List_criteria:
                    f.write(pzo+'\n')
    else: List_criteria = list(np.genfromtxt(folder_save + file_name, str, skip_header = 6))

    if save and infos:
        with open(folder_save + file_name, 'r') as f:
            header = f.read().split('\n')[:5]
            for elem in header:
                print(elem)
    return List_criteria


def Get_active_pzo_dates(list_pzo):
    """
    Établit le nombre de piézomètres actifs (parmis ceux de la liste) pour chaque année.

    Parameters
    ----------
    list_pzo : Liste
        Liste des piézomètres que l'on considère.

    Returns
    -------
    years : Python range
        Intervalle de dates, entre la date minimale d'activité et aujourd'hui.
    nb_pzo : Liste
        Nombre de piézomètres actifs à chaque date.

    """
    df_period = pd.DataFrame(index=['start_year','end_year'])
    for pzo in list_pzo:
        period = Get_period_limits(pzo)
        try: df_period[str(pzo)] = [period[0].year, period[1].year]
        except: continue

    first_year = df_period.min().min()
    last_year = pd.Timestamp.today().year-1
    years = range(first_year,last_year)
    nb_pzo = [0]*len(years)
    df_period=df_period.T
    start_years = df_period['start_year'].to_list()
    end_years = df_period['end_year'].to_list()
    for i,y in enumerate(years):
        nb_pzo[i] = nb_pzo[i-1] + start_years.count(y) - end_years.count(y)
    return years, nb_pzo


def Display_active_pzo_evolution(save=False):
    """
    Graphe de l'évolution du nombre de piézomètres actifs parmis les piézomètres Aqui-FR.

    Parameters
    ----------
    save : Booléen, optionnel
        Si Vrai, enregistrement du graphe. Par défaut : False.

    Returns
    -------
    None.

    """
    list_pzo = Get_pzo_list()
    years, nb_pzo = Get_active_pzo_dates(list_pzo)
    previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    image_path = previous_folder+'/Images/nombre_piezometres_actifs/'
    if not os.path.exists(image_path): os.makedirs(image_path)
    cmap_OBS = plt.cm.get_cmap('magma')

    fig, ax = plt.subplots(figsize=(7,4))
    ax.fill_between(years, nb_pzo, edgecolor=cmap_OBS(0.6), facecolor=cmap_OBS(0.6)[:-1]+(0.3,),step='post',zorder=5)
    ax.set_title("Number of active piezometers (Aqui-FR domain)")
    ax.grid()
    ax.set_xlim(1950, years[-1])
    ax.set_ylim(0,ax.get_ylim()[-1])
    if save:
        plt.savefig(image_path + "nombre_pzo_actifs_allpzo.png",bbox_inches='tight')
        plt.close()
    plt.show()
    return


def Display_active_pzo_evolution_by_appli(save=False):
    """
    Graphe de l'évolution du nombre de piézomètres actifs pour chacune des applications.

    Parameters
    ----------
    save : Booléen, optionnel
        Si Vrai, enregistrement du graphe. Par défaut : False.

    Returns
    -------
    None.

    """
    list_appli = Get_appli_data().index.to_list()
    models = ['odic','mart']
    nb_appli = {'odic':8, 'mart':6}
    previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    image_path = previous_folder+'/Images/nombre_piezometres_actifs/'
    if not os.path.exists(image_path): os.makedirs(image_path)

    viridis_cmap = plt.cm.get_cmap('viridis')
    styles = ['solid', 'dashed', 'dashdot', 'dotted']
    for m in models:
        fig, ax = plt.subplots()
        i=0
        for appli in list_appli:
            model = Get_appli_data().loc[appli]['Code_modèle']
            if model == m:
                title_opt = Get_appli_data().loc[appli]['Modèle']
                list_pzo = Get_filtered_pzo_list(appli)
                years, nb_pzo = Get_active_pzo_dates(list_pzo)
                ax.plot(years, nb_pzo, linestyle=styles[i%4], color=viridis_cmap(i/nb_appli[m]), label=appli)
                i+=1
        ax.set_ylabel("Number of active piezometers")
        ax.set_title("Over the {} applications".format(title_opt))
        ax.grid()
        ax.set_xlim(1950, years[-1])
        ax.set_ylim(0,ax.get_ylim()[-1])
        ax.legend()
        if save:
            plt.savefig(image_path + "nombre_pzo_actifs_{}.png".format(m),bbox_inches='tight')
            plt.close()
        plt.show()
    return


















