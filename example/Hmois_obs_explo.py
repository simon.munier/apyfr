#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 31 16:34:36 2023

@author: jeanteta
"""

#%% Extraction des packages d'intérêt
import os
import re
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter
from apyfr.maps import *
from matplotlib.backends.backend_pdf import PdfPages


#%% Extraction du répertoire où AQUI-FR est stocké --> "AQUIFRDIR" doit être exporté en variable d'env.
#     !!!!!! IMPORTANT !!!!!!
#     1. "AQUIFRDIR" doit être défini dans le .bash_profile/.bashrc/... Au choix
#     2. Il faut exécuter l'IDE (ex. Spyder) depuis le terminal sinon les variables d'env ne seront pas lu
DIR_AQUI      =  os.environ["AQUIFRDIR"]+"/"
folder_piezos = DIR_AQUI+"LIB/python_modules/apyfr/data_for_apyfr/Donnees_piezo/"

#%% Extraction des fonctions annexes développées dans APYFR
os.chdir(DIR_AQUI+"LIB/python_modules/apyfr/example")
from TOOLS_BOX import *

#%% Extraction de la liste des pzos dans hauteurs mensuelles
LISTE_PZOS = os.listdir(folder_piezos+"Observations/Hauteur_mensuelle")
LISTE_PZOS = np.array([str(pzo_i).replace(".csv","") for pzo_i in LISTE_PZOS])

#%% Boucle sur les pzos
# TAB_SYN     = np.empty(shape = (len(LISTE_PZOS),9))
# TAB_SYN     = TAB_SYN*[np.nan]
# TAB_SYN     = TAB_SYN.astype(str)
# RANGE_DATES = [to_date("1990-08-01"),to_date("2020-07-31")]
# for i_pzo_i, pzo_i in enumerate(LISTE_PZOS):
#     # i_pzo_i = 7
#     # pzo_i   = LISTE_PZOS[i_pzo_i]
#     print("i = "+str(i_pzo_i)+"  |  "+pzo_i)
#     TAB_PZO_i       = pd.read_csv(folder_piezos+'Observations/Hauteur_mensuelle/'+pzo_i+'.csv',index_col = 0)
#     DATES_PZO_i     = np.array(TAB_PZO_i.index)
#     IND_DATE_RANGE  = [i_date_i for i_date_i, date_i in enumerate(DATES_PZO_i) if (to_date(date_i) >= RANGE_DATES[0]) & (to_date(date_i) <= RANGE_DATES[1])]
#     TAB_PZO_i.index = range(0,TAB_PZO_i.shape[0])

#     TAB_SYN[i_pzo_i,0] = pzo_i
#     TAB_SYN[i_pzo_i,1] = str(DATES_PZO_i[0])[0:7]
#     TAB_SYN[i_pzo_i,2] = str(DATES_PZO_i[len(DATES_PZO_i)-1])[0:7]
#     TAB_SYN[i_pzo_i,3] = len(DATES_PZO_i)
#     TAB_SYN[i_pzo_i,4] = sum(np.array(np.isfinite(TAB_PZO_i[:])))[0]
#     if IND_DATE_RANGE != []:
#         TAB_SYN[i_pzo_i,5] = len(IND_DATE_RANGE)
#         TAB_SYN[i_pzo_i,6] = sum(np.array(np.isfinite(np.array(TAB_PZO_i)[np.array(IND_DATE_RANGE),:])))[0]
#         TAB_SYN[i_pzo_i,7] = DATES_PZO_i[IND_DATE_RANGE[0]][0:7]
#         TAB_SYN[i_pzo_i,8] = DATES_PZO_i[IND_DATE_RANGE[len(IND_DATE_RANGE)-1]][0:7]

# TAB_SYN = pd.DataFrame(TAB_SYN, columns = ["PZO_CODE","START","END","NB_M_TOT","NB_M_FINITE","NB_REF","NB_REF_FINITE","REF_START","REF_END"])
# TAB_SYN["NB_REF_FINITE"].values
# [int(date_i[0:4]) for date_i in TAB_SYN["START"].values]


# #%% Histogrammes
# #    Nombre d'années total
# n, bins, patches = plt.hist(x = TAB_SYN["NB_M_TOT"].values.astype(float)/12, bins='auto', color='darkcyan',
#                             alpha=0.7, rwidth=0.85)
# plt.grid(axis='y', alpha=0.75)
# plt.xlabel('Value')
# plt.ylabel('Frequency')
# plt.title("Nombre d'années total")


# #    Années au démarrage
# n, bins, patches = plt.hist(x = [int(date_i[0:4]) for date_i in TAB_SYN["START"].values], bins='auto', color='darkcyan',
#                             alpha=0.7, rwidth=0.85, density = True)
# plt.grid(axis='y', alpha=0.75)
# plt.xlabel('Value')
# plt.ylabel('Frequency')
# plt.title("Années démarrage")


# #    Nombre d'années avec des valeurs dans la période de référence 1990-2020
# n, bins, patches = plt.hist(x = TAB_SYN["NB_REF_FINITE"].values.astype(float)/12, bins=range(0,32,1), color='darkcyan',
#                             alpha=0.7, rwidth=0.85, density = True, cumulative = False)
# plt.gca().yaxis.set_major_formatter(PercentFormatter(1))
# plt.grid(axis='y', alpha=0.75)
# plt.xlabel('Value')
# plt.ylabel('Frequency')
# plt.title("Nombre d'années avec valeurs entre 1990 et 2020")

# #%% Cartographies
# TAB_L2E = pd.read_csv(folder_piezos+'piezometres.csv',index_col = 0)

# #    Filtre 20 ans de données
# NB_FILT  = 20
# TAB_FILT = TAB_SYN[TAB_SYN["NB_REF_FINITE"].values.astype(float)/12 >= NB_FILT]
# L2E_FILT = np.empty(shape = (TAB_FILT.shape[0],2))
# L2E_FILT = L2E_FILT*[np.nan]

# for i_pzo_i in range(0,L2E_FILT.shape[0]):
#     # i_pzo_i = 0
#     IND_PZO_L2E = [i for i, bool_i in enumerate(TAB_L2E["Code"].values == TAB_FILT["PZO_CODE"].values[i_pzo_i]) if bool_i][0]
#     L2E_FILT[i_pzo_i,0] = TAB_L2E.values[IND_PZO_L2E,3]
#     L2E_FILT[i_pzo_i,1] = TAB_L2E.values[IND_PZO_L2E,4]


# plot_obj = blank_map()
# plot_obj.scatter(L2E_FILT[:,0], L2E_FILT[:,1], 100, "#0504aa","o",lw=0,transform=L2E)
# plt.title("FILTRE : "+str(NB_FILT)+" ans  -  NBRE PZOs : "+str(TAB_FILT.shape[0]), fontsize = 20)


#%% Extraction pzos avec chros continues avant 2020
DATE_DEB  = "1990-08-15"
DATE_FIN  = "2020-07-15"
FILTRE    = 20
DATES_ACC = pd.date_range(start = to_date(str(int(DATE_FIN[0:4]) - FILTRE - 1)+"-08-15"),
                          end   = to_date(DATE_FIN))
DATES_ACC = [str(date_i)[0:10] for date_i in DATES_ACC if str(date_i)[8:10] == "15"]
DATES_REF = [str(date_i)[0:10] for date_i in pd.date_range(start = to_date(DATE_DEB),end = to_date(DATE_FIN)) if str(date_i)[8:10] == "15"]
PZOS_ACC  = []
PZOS_REF  = []

for i_pzo_i, pzo_i in enumerate(LISTE_PZOS):
    # pzo_i = "00155C0017_F1"
    print("i = "+str(i_pzo_i)+"  |  "+pzo_i)
    TAB_PZO_i = pd.read_csv(folder_piezos+'Observations/Hauteur_mensuelle/'+pzo_i+'.csv',index_col = 0)

    # Test sur la chronique de dates acceptées
    TEST_ACC  = True if sum([date_i not in TAB_PZO_i.index for date_i in DATES_ACC]) == 0 else False
    if TEST_ACC:
        IND_FIN = [i_date_i for i_date_i, date_i in enumerate(TAB_PZO_i.index) if date_i == DATE_FIN][0]
        if sum(np.array(~np.isfinite(TAB_PZO_i.iloc[(IND_FIN - 12*FILTRE + 2):IND_FIN])))[0] == 0: PZOS_ACC.append(pzo_i)

    # Test sur la chronique de dates sur la période de référence complète
    TEST_REF  = True if sum([date_i not in TAB_PZO_i.index for date_i in DATES_REF]) == 0 else False
    if TEST_REF:
        IND_DEB = [i_date_i for i_date_i, date_i in enumerate(TAB_PZO_i.index) if date_i == DATE_DEB][0]
        IND_FIN = [i_date_i for i_date_i, date_i in enumerate(TAB_PZO_i.index) if date_i == DATE_FIN][0]
        if sum(np.array(~np.isfinite(TAB_PZO_i.iloc[IND_DEB:IND_FIN])))[0] == 0: PZOS_REF.append(pzo_i)

#%% Cartographies
TAB_L2E = pd.read_csv(folder_piezos+'piezometres.csv',index_col = 0)
L2E_REF = np.empty(shape = (len(PZOS_REF),2))
L2E_REF = L2E_REF*[np.nan]
for i_pzo_i in range(0,(L2E_REF.shape[0])):
    # i_pzo_i = 0
    IND_PZO_L2E = [i for i, bool_i in enumerate(TAB_L2E["Code"].values == PZOS_REF[i_pzo_i]) if bool_i][0]
    L2E_REF[i_pzo_i,0] = TAB_L2E.values[IND_PZO_L2E,3]
    L2E_REF[i_pzo_i,1] = TAB_L2E.values[IND_PZO_L2E,4]


L2E_ACC = np.empty(shape = (len(PZOS_ACC),2))
L2E_ACC = L2E_ACC*[np.nan]
for i_pzo_i in range(0,(L2E_ACC.shape[0])):
    # i_pzo_i = 0
    print("i = "+str(i_pzo_i))
    IND_PZO_L2E = [i for i, bool_i in enumerate(TAB_L2E["Code"].values == PZOS_ACC[i_pzo_i]) if bool_i][0]
    L2E_ACC[i_pzo_i,0] = TAB_L2E.values[IND_PZO_L2E,3]
    L2E_ACC[i_pzo_i,1] = TAB_L2E.values[IND_PZO_L2E,4]

# fig, (M_REF, M_ACC) = plt.subplots(1, 2)
# M_REF = blank_map()
plotfig = PdfPages(folder_piezos+"CARTE_PZOs_DISPO_DATA.pdf")
plot_obj = blank_map()
plot_obj.scatter(L2E_ACC[:,0], L2E_ACC[:,1], 100, "#0504aa","o",lw=4,transform=L2E, alpha = 0.5, label = ">2000-2020 (au moins "+str(FILTRE)+" ans) - NB pzos : "+(str(len(PZOS_ACC))))
plot_obj.scatter(L2E_REF[:,0], L2E_REF[:,1], 100, "red","^",lw=5,transform=L2E, alpha = 0.50, label = "1990-2020 (30 ans fixes) - NB pzos : "+(str(len(PZOS_REF))))
plot_obj.legend(fontsize = "x-large", loc = "upper right")
plt.title("FILTRE : "+str(FILTRE)+" ans", fontsize = 20)
plotfig.savefig()
plt.close()
plotfig.close()



