from matplotlib import pyplot as plt
from apyfr.experiment import Experiment
from apyfr.maps import map_applis

#===============================================================================
# AquiFR output for this example can be found on the AquiFR ftp server:
#   aquifr_dev@ftp.umr-cnrm.fr:/apyfr_example/PS7_202104.tgz
# The file should be untarred within the example directory

exp = 'PS7_202104'
monday = 'mon'

var = 'spli_surf'

#time_step = 0
time_step = '2021-04-15'

#applis = None # list of applications of experiment
applis = 'def' # default list of applications
#applis = 'odic_seine'

extent = None # default map extent depending on application list
#extent = (260000,680000,2025000,2435000)
#extent = (320000,640000,2330000,2580000)
#extent = (514000,740000,2490000,2700000)

vlim = None

#===============================================================================

EXP = Experiment(exp)
EXP.read_results(var,monday,ts=time_step,applis=applis,file_suffix='_2021')
#EXP.compute_median(var,monday)
#EXP.map_applis(var+'_median',extent=extent,vlim=vlim,syrah=True,logo=False)
#EXP.compute_dr5(var,monday,frac=False)
#EXP.map_applis(var+'_dr5',extent=extent,vlim=[0,1],cmap='Reds',draw_colorbar=False,syrah=True,logo=False)

EXP.compute_dr5(var,monday,frac=True)
EXP.map_applis(var+'_dr5',extent=extent,vlim=[0,1],cmap='Reds',draw_colorbar=False,syrah=True,logo=False)


plt.show()
