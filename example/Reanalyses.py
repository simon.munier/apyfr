#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  4 11:36:01 2021
@author: andre.mounier@ens-paris-saclay.fr

    Lecture des fichiers netCDF de réanalyses.
"""

import os
import netCDF4 as nc
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import locale

from Piezometres import Which_appli, Get_appli_data, Get_filtered_pzo_list, Get_pzo_list
from Observations_piezo import Get_monthly_chronicle, to_date

locale.setlocale(locale.LC_ALL, 'fr_FR.utf8')


def is_present(pzo):
    """
    Exclusion des piézomètres ne faisant pas parti des fichiers de réanalyses.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").

    Returns
    -------
    Booléen
        Vrai si les données du piézomètre sont existantes, faux sinon.

    """
    not_in_rea = ['07092X0077_F']
    return not pzo in not_in_rea


def Add_real_time(pzo, date_range=[to_date('1899-01-01'),pd.Timestamp.today()]):
    """
    Ajout des réanalyses temps réel.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    date_range : Liste, optionnel
        Liste des dates de début et de fin. Par défaut : [to_date('1899-01-01'),pd.Timestamp.today()].

    Returns
    -------
    Liste
        Liste de tuples. Chaque tuple est composée du code de l'application et d'un dataframe contenant les niveaux piézomètriques pour chaque date.

    """
    if not is_present(pzo):
        print("{} : Station non disponible".format(pzo))
        return [(None,pd.DataFrame())]

    previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    data_path = previous_folder+'/Donnees/REA_TR/'
    dataframe_appli = Get_appli_data()
    list_appli = Which_appli(pzo)
    result = [0]*len(list_appli)
    for idx_appli, appli in enumerate(list_appli):
        model_code = dataframe_appli['Code_modèle'].loc[[appli]][0]
        REA_nc_file = "{}_{}_mon.nc".format(model_code, appli)
        if REA_nc_file not in os.listdir(data_path):
            print('{} : Fichier inexistant'.format(pzo))
            return pd.DataFrame()

        REA_nc = nc.Dataset(data_path+REA_nc_file)
        REA_start_date = pd.to_datetime(str(REA_nc.variables['time'].units.split(' ')[-1]), format='%Y-%m-%d')
        days_after = REA_nc.variables['time'][:]
        REA_time = [np.nan]*len(days_after)
        for i,d in enumerate(days_after):
            REA_time[i] = REA_start_date + pd.Timedelta(days=int(d))

        list_pzo_filtered = Get_filtered_pzo_list(appli)
        pzo_number = list_pzo_filtered.index(pzo)
        data_hauteur = REA_nc.variables['h_piezos'][:,pzo_number]
        REA_nc.close()
        prov = pd.DataFrame({'Hauteur':data_hauteur}, index = REA_time)
        prov = prov[(prov.index >= date_range[0]) & (prov.index <= date_range[1])]
        result[idx_appli] = (appli, prov)
    return result


def Get_REA_monthly_chronicle(simulated_data_path, pzo, date_range=[to_date('1899-01-01'),pd.Timestamp.today()]):
    """
    Lecture des réanalyses au format netCDF.

    Parameters
    ----------
    simulated_data_path: pathname of the folder containing simulated data
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    date_range : Liste, optionnel
        Liste des dates minimale et maximale de la chronique. Par défaut : [to_date('1899-01-01'), pd.Timestamp.today()].

    Returns
    -------
    result : Liste
        Liste des ensembles : application et dataframe des hauteurs piézométriques de la réanalyse.

    """
    if not is_present(pzo):
        print("{} : Station non disponible".format(pzo))
        return [(None,pd.DataFrame())]

    # modif luca
    #previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    #data_path = previous_folder+'/Donnees/Reanalyses/'
#    simulated_data_path = '/cnrm/surface/munier/AquiFR/Results/REA_V13_1958_2021/'
    #dataframe_appli = Get_appli_data()  # modif luca
    list_appli = Which_appli(pzo)
    result = [0]*len(list_appli)

    #modif luca (pour remplacer le fichier applications.csv)
    models_names = {'teg':'mart', 'poc':'mart', 'som':'mart', 'bno':'mart', 'als':'mart', 'npc':'mart',
                    'seine':'odic', 'seine-eure':'odic', 'seine-oise':'odic', 'marne-oise':'odic', 'marne-loing':'odic', 'somme':'odic', 'loire':'odic', 'basse-normandie':'odic'}
    for idx_appli, appli in enumerate(list_appli):
        #model_code = dataframe_appli['Code_modèle'].loc[[appli]][0]   # modif luca
        model_code = models_names[appli]
        REA_nc_file = "{}_{}_mon.nc".format(model_code, appli)

        if REA_nc_file not in os.listdir(simulated_data_path):
            print('{} : Fichier inexistant'.format(pzo))
            return pd.DataFrame()

        REA_nc = nc.Dataset(simulated_data_path+REA_nc_file)
        REA_start_date = pd.to_datetime(str(REA_nc.variables['time'].units.split(' ')[-1]), format='%Y-%m-%d')
        days_after = REA_nc.variables['time'][:]
        REA_time = [np.nan]*len(days_after)
        for i,d in enumerate(days_after):
            REA_time[i] = REA_start_date + pd.Timedelta(days=int(d))
        list_pzo_filtered = Get_filtered_pzo_list(appli)
        pzo_number = list_pzo_filtered.index(pzo)
        data_hauteur = REA_nc.variables['h_piezos'][:,pzo_number]
        REA_nc.close()
        prov = pd.DataFrame({'Hauteur':data_hauteur}, index = REA_time)
        prov = prov[(prov.index >= date_range[0]) & (prov.index <= date_range[1])]
        result[idx_appli] = (appli, prov)

# modif luca
#    if result[0][1].empty:
#        return Add_real_time(pzo, date_range=date_range)
#    elif result[0][1].index[-1] < date_range[-1]:
#        complete_result = []
#        REA_TR = Add_real_time(pzo, date_range=[result[0][1].index[-1],date_range[-1]])
#        for idx_appli, appli in enumerate(list_appli):
#            REA_Archive = result[idx_appli][1]
#            REA_complete = REA_Archive.append(REA_TR[idx_appli][1])
#            complete_result.append((appli,REA_complete))
#        return complete_result
    return result


def Get_REA_monthly_chronicle_single_month(pzo, month, date_range=[to_date('1899-01-01'), pd.Timestamp.today()]):
    """
    Lecture des réanalyses au format netCDF pour un seul mois donné.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    month : Entier
        Mois de l'année, de 1 pour janvier à 12 pour décembre.
    date_range : Liste, optionnel
        Liste des dates minimale et maximale de la chronique. Par défaut : [to_date('1899-01-01'), pd.Timestamp.today()].

    Returns
    -------
    result : Liste
        Liste des ensembles : application et dataframe des hauteurs piézométriques de la réanalyse.

    """
    data_hauteur = Get_REA_monthly_chronicle(pzo, date_range = date_range)
    result = [0]*max(1,len(data_hauteur))
    for i, (appli, df_hauteur) in enumerate(data_hauteur):
        if df_hauteur.empty : return None
        df_hauteur = df_hauteur[df_hauteur.index.month == month]
        result[i] = (appli, df_hauteur)
    return result


def Display_REA_monthly_chronicle(pzo, date_range=[to_date('1899-01-01'), pd.Timestamp.today()], add_OBS=False, save=False):
    """
    Affichage des chroniques des réanalyses.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    date_range : Liste, optionnel
        Liste des dates minimale et maximale de la chronique. Par défaut : [to_date('1899-01-01'), pd.Timestamp.today()].
    add_OBS : Booléen, optionnel
        Si Vrai, ajout de la chronique observée sur le graphique. Par défaut : False.
    save : Booléen, optionnel
        Enregistrement de l'image si Vrai. Par défaut : False.

    Returns
    -------
    None.

    """
    REA_hauteur = Get_REA_monthly_chronicle(pzo, date_range = date_range)
    if add_OBS: OBS_hauteur = Get_monthly_chronicle(pzo, date_range = date_range)
    if REA_hauteur == None: return

    title = pzo
    cmap_REA = plt.cm.get_cmap('viridis')
    cmap_OBS = plt.cm.get_cmap('magma')
    index_cmap = np.linspace(0.35,0.65,len(REA_hauteur))
    previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    image_path = previous_folder+'/Images/chronique_REA/'
    if not os.path.exists(image_path): os.makedirs(image_path)
    fig, ax = plt.subplots()
    ax.grid()
    ax.set_title(title)
    ax.set_ylabel("Piezometric level (m)")

    for i, (appli, df_hauteur) in enumerate(REA_hauteur):
        label = "Reanalysis ({})".format(appli)
        ax.plot(df_hauteur, label = label, color = cmap_REA(index_cmap[i]))
    if add_OBS: ax.plot(OBS_hauteur, label = "Observations", color = cmap_OBS(0.6))
    ax.legend()
    ax.yaxis.get_major_locator().set_params(integer=True)
    if save:
        plt.savefig(image_path + "reanalyse_H_{}.png".format(pzo),bbox_inches='tight')
        plt.close()
    plt.show()
    return


def Compute_bias_OBS_REA(pzo, best_appli=None, date_range=[to_date('1899-01-01'), pd.Timestamp.today()]):
    """
    Calcul de l'écart moyen entre les niveaux piézométriques des observations et des réanalyses, sur une période donnée.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    best_appli : None ou Chaîne de caractères
        Si spécifié, permet de choisir l'application souhaitée lorsqu'il y en a plusieurs. Par défaut: None
    date_range : Liste, optionnel
        Liste des dates minimale et maximale de la chronique. Par défaut : [to_date('1899-01-01'), pd.Timestamp.today()].

    Returns
    -------
    minimal_bias : Flottant
        Biais moyen correspondant à l'application.
    best_appli : Chaîne de caractères
        Application choisie en entrée ou présentant l'écart moyen minimal.

    """
    REA_hauteur = Get_REA_monthly_chronicle(pzo, date_range = date_range)
    OBS_hauteur = Get_monthly_chronicle(pzo, date_range = date_range)
    if REA_hauteur[0][0] is None or OBS_hauteur is None: return np.nan, np.nan
    if best_appli is None:
        minimal_bias = np.nan
        for data_REA_appli in REA_hauteur:
            appli = data_REA_appli[0]
            REA_appli = data_REA_appli[1]
            df_height = pd.merge(REA_appli, OBS_hauteur, left_index=True, right_index=True, suffixes=('_REA', '_OBS'))
            height_biais_evolution = df_height['Hauteur_REA']-df_height['Hauteur_OBS']
            height_bias = height_biais_evolution.mean()
            if np.isnan(minimal_bias) or height_bias<minimal_bias:
                minimal_bias, best_appli = height_bias, appli
    else:
        idx_appli = Which_appli(pzo).index(best_appli)
        REA_appli = REA_hauteur[idx_appli][1]
        df_height = pd.merge(REA_appli, OBS_hauteur, left_index=True, right_index=True, suffixes=('_REA', '_OBS'))
        height_biais_evolution = df_height['Hauteur_REA']-df_height['Hauteur_OBS']
        minimal_bias = height_biais_evolution.mean()
    return minimal_bias, best_appli


def Compute_relative_difference_OBS_REA(pzo, best_appli=None, date_range = [to_date('1899-01-01'), pd.Timestamp.today()]):
    """
    Calcul de l'écart relatif entre les niveaux piézométriques des observations et des réanalyses, sur une période donnée.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    best_appli : None ou Chaîne de caractères
        Si spécifié, permet de choisir l'application souhaitée lorsqu'il y en a plusieurs. Par défaut: None
    date_range : Liste, optionnel
        Liste des dates minimale et maximale de la chronique. Par défaut : [to_date('1899-01-01'), pd.Timestamp.today()].

    Returns
    -------
    rd : Flottant
        Écart relatif correspondant à l'application.
    best_appli : Chaîne de caractères
        Application choisie en entrée ou présentant l'écart moyen minimal.

    """
    REA_hauteur = Get_REA_monthly_chronicle(pzo, date_range = date_range)
    OBS_hauteur = Get_monthly_chronicle(pzo, date_range = date_range)
    if REA_hauteur[0][0] is None or OBS_hauteur is None: return np.nan, np.nan
    if best_appli is None:
        rd = np.nan
        for data_REA_appli in REA_hauteur:
            appli = data_REA_appli[0]
            REA_appli = data_REA_appli[1]
            df_height = pd.merge(REA_appli, OBS_hauteur, left_index=True, right_index=True, suffixes=('_REA', '_OBS'))
            relative_difference_evolution = (df_height['Hauteur_REA']-df_height['Hauteur_OBS'])/df_height['Hauteur_OBS'].abs()
            relative_difference = relative_difference_evolution.mean()
            if np.isnan(rd) or abs(relative_difference)<abs(rd):
                rd, best_appli = relative_difference, appli
    else:
        idx_appli = Which_appli(pzo).index(best_appli)
        REA_appli = REA_hauteur[idx_appli][1]
        df_height = pd.merge(REA_appli, OBS_hauteur, left_index=True, right_index=True, suffixes=('_REA', '_OBS'))
        relative_difference_evolution = (df_height['Hauteur_REA']-df_height['Hauteur_OBS'])/(df_height['Hauteur_OBS'].abs())
        rd = relative_difference_evolution.mean()
    return rd, best_appli


def Compute_correlation_OBS_REA(pzo, best_appli=None, date_range = [to_date('1899-01-01'), pd.Timestamp.today()]):
    """
    Calcul de la corrélation entre les niveaux piézométriques des observations et des réanalyses, sur une période donnée.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    best_appli : None ou Chaîne de caractères
        Si spécifié, permet de choisir l'application souhaitée lorsqu'il y en a plusieurs. Par défaut: None
    date_range : Liste, optionnel
        Liste des dates minimale et maximale de la chronique. Par défaut : [to_date('1899-01-01'), pd.Timestamp.today()].

    Returns
    -------
    best_correlation : Flottant
        Coefficient de corrélation correspondant à l'application.
    best_appli : Chaîne de caractères
        Application choisie en entrée ou présentant l'écart moyen minimal.

    """
    REA_hauteur = Get_REA_monthly_chronicle(pzo, date_range = date_range)
    OBS_hauteur = Get_monthly_chronicle(pzo, date_range = date_range)
    if REA_hauteur[0][0] is None or OBS_hauteur is None: return np.nan, np.nan
    if best_appli is None:
        best_correlation = np.nan
        for data_REA_appli in REA_hauteur:
            appli = data_REA_appli[0]
            REA_appli = data_REA_appli[1]
            df_height = pd.merge(REA_appli, OBS_hauteur, left_index=True, right_index=True, suffixes=('_REA', '_OBS'))
            correlation = df_height.corr().values[0,1]
            if np.isnan(best_correlation) or abs(correlation)>abs(best_correlation):
                best_correlation, best_appli = correlation, appli
    else:
        idx_appli = Which_appli(pzo).index(best_appli)
        REA_appli = REA_hauteur[idx_appli][1]
        df_height = pd.merge(REA_appli, OBS_hauteur, left_index=True, right_index=True, suffixes=('_REA', '_OBS'))
        best_correlation = df_height.corr().values[0,1]
    return best_correlation, best_appli


def excluded(pzo):
    """
    Exclusion de piézomètres dont les données sont de trop mauvaise qualité.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").

    Returns
    -------
    Booléen
        Vrai si le piézomètre est exclu, faux sinon.

    """
    excluded = ['07094X0038_F','00146B0007_F1','07106X0517_PZ1','00096D0003_F1','04562X0071_F','00198X0087_P','00205X0397_PZ2']
    return pzo in excluded


def Formatting_correlation_relative_difference_OBS_REA(list_pzo, date_range=[to_date('1899-01-01'), pd.Timestamp.today()]):
    df_characteristics = pd.DataFrame(index=['Corrélation','Écart relatif','Écart absolu'])
    for pzo in tqdm(list_pzo):
        if excluded(pzo): continue
        corr, best_appli = Compute_correlation_OBS_REA(pzo, date_range=date_range)
        rel_diff, best_appli = Compute_relative_difference_OBS_REA(pzo, best_appli, date_range)
        abs_diff, best_appli = Compute_bias_OBS_REA(pzo, best_appli, date_range)
        df_characteristics[pzo] = [corr,rel_diff,abs_diff]
    return df_characteristics


def Display_correlation_histogram_OBS_REA(save=False):
    """
    Affichage de l'histogramme des coefficients de corrélations entre les niveaux piézométriques d'observation et de réanalyse pour tous les piézomètres.

    Parameters
    ----------
    save : Booléen, optionnel
        Enregistrement de l'image si Vrai. Par défaut : False.

    Returns
    -------
    None.

    """
    list_pzo = Get_pzo_list()
    df_c_rd_ad = Formatting_correlation_relative_difference_OBS_REA(list_pzo)
    df_corr = df_c_rd_ad.T['Corrélation']

    previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    image_path = previous_folder+'/Images/corr_OBS_REA/'
    if not os.path.exists(image_path): os.makedirs(image_path)

    color = plt.cm.get_cmap('viridis')(0.5)
    fig, ax = plt.subplots()
    ax.grid()
    ax.set_ylabel("Piezometers ratio")
    ax.set_xlabel("Correlation coefficient")
    ax.hist(df_corr, cumulative=-1, density=True, histtype='stepfilled', range=(0.,1.), align='mid', rwidth=0.9, zorder=2, color=(0.127568, 0.566949, 0.550556, 0.35),edgecolor=color)
    if save:
        plt.savefig(image_path + "hist_corr_all_pzo.png", bbox_inches='tight')
        plt.close()
    plt.show()
    return


def Get_date_limits_OBS_REA(pzo):
    """
    Renvoie la période d'intersection entre observations et réanalyses, pour un piézomètre donné.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").

    Returns
    -------
    time_limits : Liste
        Liste de deux dates, celle de début et celle de fin.

    """
    OBS = Get_monthly_chronicle(pzo)
    if OBS is None: return None
    OBS_time_limits = [OBS.index[0], OBS.index[-1]]
    appli, REA = Get_REA_monthly_chronicle(pzo)[0]
    if appli is None: return None
    REA_time_limits = [REA.index[0], REA.index[-1]]
    time_limits = [max(REA_time_limits[0],OBS_time_limits[0]), min(REA_time_limits[1],OBS_time_limits[1])]
    return time_limits


def Get_drought_dates_REA(pzo):
    """
    Renvoie la liste des mois pour lesquels les niveaux piézométriques des réanalyses sont inférieurs au premier décile du mois.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").

    Returns
    -------
    drought_dates : Array
        Numpy array contenant les différentes dates.

    """
    date_range = Get_date_limits_OBS_REA(pzo)
    if date_range is None: return None

    _, best_appli = Compute_correlation_OBS_REA(pzo, date_range=date_range)
    idx_appli = Which_appli(pzo).index(best_appli)
    REA = Get_REA_monthly_chronicle(pzo, date_range=date_range)[idx_appli][1]
    drought_dates = None
    for i in range(1,13):
        REA_single_month = REA[REA.index.month == i]
        first_decile_singe_month = REA_single_month.quantile(0.1)
        drought_dates_single_month = REA_single_month[REA_single_month<first_decile_singe_month].dropna().index.to_numpy()
        if drought_dates is None: drought_dates = drought_dates_single_month
        else: drought_dates = np.concatenate([drought_dates,drought_dates_single_month])
    drought_dates = np.sort(drought_dates)
    return drought_dates


def Get_drought_dates_OBS(pzo):
    """
    Renvoie la liste des mois pour lesquels les niveaux piézométriques des observations sont inférieurs au premier décile du mois.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").

    Returns
    -------
    drought_dates : Array
        Numpy array contenant les différentes dates.

    """
    date_range = Get_date_limits_OBS_REA(pzo)
    if date_range is None: return None

    OBS = Get_monthly_chronicle(pzo, date_range=date_range)
    drought_dates = None
    for i in range(1,13):
        OBS_single_month = OBS[OBS.index.month == i]
        first_decile_singe_month = OBS_single_month.quantile(0.1)
        drought_dates_single_month = OBS_single_month[OBS_single_month<first_decile_singe_month].dropna().index.to_numpy()
        if drought_dates is None: drought_dates = drought_dates_single_month
        else: drought_dates = np.concatenate([drought_dates,drought_dates_single_month])
    drought_dates = np.sort(drought_dates)
    return drought_dates


def Compile_drought_dates(pzo):
    """
    Compilation de la période d'interscetion, des dates de sécheresse de l'observation et des dates de sécheresse de la réanalyse, pour un piézomètre donné.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").

    Returns
    -------
    date_limits : Liste
        Liste de deux dates, celle de début et celle de fin.
    OBS_drought_dates : Array
        Numpy array contenant les différentes dates.
    REA_drought_dates : Array
        Numpy array contenant les différentes dates.

    """
    REA_drought_dates = Get_drought_dates_REA(pzo)
    OBS_drought_dates = Get_drought_dates_OBS(pzo)

    date_limits = Get_date_limits_OBS_REA(pzo)
    if date_limits is None: return None, None, None

    REA_drought_dates = REA_drought_dates[REA_drought_dates>date_limits[0]]
    REA_drought_dates = REA_drought_dates[REA_drought_dates<date_limits[1]]
    OBS_drought_dates = OBS_drought_dates[OBS_drought_dates>date_limits[0]]
    OBS_drought_dates = OBS_drought_dates[OBS_drought_dates<date_limits[1]]
    return date_limits, OBS_drought_dates, REA_drought_dates


def Compute_drought_dates(list_pzo, pzo_ratio=0.5):
    """
    Calcul les sécheresses et leur pourcentage à chaque date disponible.

    Parameters
    ----------
    list_pzo : Liste
        Liste des piézomètres que l'on considère.
    pzo_ratio : Flottant, optionnel
        Calcul du pourcentage si la part de piézomètres non disponibles est inférieure au ratio. Par défaut : 0.5.

    Returns
    -------
    df_drought_count : DataFrame
        Tableau de caractérisation des sécheresses. Les colonnes sont les différentes dates et les lignes sont :
            - Nombre de piézomètres en sécheresses (observations) à telle date
            - Nombre de piézomètres en sécheresses (réanalyses) à telle date
            - Nombre de piézomètres ne présentant pas de donnée à telle date
            - Nombre de piézomètres dans la liste
            - Pourcentage du nombre de piézomètres en sécheresse (observations)
            - Pourcentage du nombre de piézomètres en sécheresse (réanalyses)

    """
    index = [d.replace(day=15) for d in pd.date_range(to_date('1958-08-15'),pd.Timestamp.today(),freq='M')]
    df_OBS_drought = pd.DataFrame(index=index)
    df_REA_drought = pd.DataFrame(index=index)

    for pzo in tqdm(list_pzo):
        date_limits, OBS_drought_dates, REA_drought_dates = Compile_drought_dates(pzo)
        if date_limits is None:
            df_OBS_drought[str(pzo)] = [-1]*len(index)
            df_REA_drought[str(pzo)] = [-1]*len(index)
            continue

        drought_OBS = [0]*len(index)
        drought_REA = [0]*len(index)
        for i,date in enumerate(index):
            if date < date_limits[0] or date > date_limits[1]:
                drought_OBS[i] = -1.
                drought_REA[i] = -1.
            else:
                drought_OBS[i] = float(date in OBS_drought_dates)
                drought_REA[i] = float(date in REA_drought_dates)
        df_OBS_drought[str(pzo)] = drought_OBS
        df_REA_drought[str(pzo)] = drought_REA
    df_OBS_drought = df_OBS_drought.T
    df_REA_drought = df_REA_drought.T
    df_drought_count = pd.DataFrame(index=['Sécheresses_OBS', 'Sécheresses_REA', 'NaN', 'Piézomètres', 'Pourcentage_OBS', 'Pourcentage_REA'])

    for date in index:
        OBS_list = df_OBS_drought[date].to_list()
        REA_list = df_REA_drought[date].to_list()
        nb_OBS_drought = OBS_list.count(1.)
        nb_REA_drought = REA_list.count(1.)
        nb_NaN = OBS_list.count(-1.)
        nb_pzo, OBS_ratio, REA_ratio = len(list_pzo), np.nan, np.nan
        if nb_NaN < pzo_ratio*nb_pzo:
            OBS_ratio = nb_OBS_drought/(nb_pzo-nb_NaN)*100
            REA_ratio = nb_REA_drought/(nb_pzo-nb_NaN)*100
        df_drought_count[date] = [nb_OBS_drought, nb_REA_drought, nb_NaN, nb_pzo, OBS_ratio, REA_ratio]
    return df_drought_count


def Display_drought_chronicle(list_pzo, pzo_ratio=0.75, save=False):
    """
    Affichage du pourcentage de piézomètres en sécheresse en focntion du temps (observations et réanalyses)

    Parameters
    ----------
    list_pzo : Liste
        Liste des piézomètres que l'on considère.
    pzo_ratio : Flottant, optionnel
        Calcul du pourcentage si la part de piézomètres non disponibles est inférieure au ratio. Par défaut : 0.75.
    save : Booléen, optionnel
        Si Vrai, enregistrement du graphe. Par défaut : False.

    Returns
    -------
    None.

    """
    df_drought_count = Compute_drought_dates(list_pzo, pzo_ratio=pzo_ratio)
    df_drought_count = df_drought_count.T

    jet_cmap = plt.cm.get_cmap('jet')
    previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    image_path = previous_folder+'/Images/sécheresses/'
    if not os.path.exists(image_path): os.makedirs(image_path)

    fig, ax = plt.subplots(figsize=(10,4))
    ax.grid()
    ax.set_ylabel("Pourcentage des piézomètres \nen sécheresse")
    ax.plot(df_drought_count['Pourcentage_OBS'], color='tab:red', label='Observations')
    ax.plot(df_drought_count['Pourcentage_REA'], color=jet_cmap(0.1), label='Réanalyse')
    ax.legend()
    x_limits = ax.get_xlim()
    ax2 = ax.twinx()
    ax2.fill_between(df_drought_count.index, df_drought_count['Piézomètres']-df_drought_count['NaN'], step='mid', facecolor=(0.8, 0.8, 0.8, 0.7))
    ax2.set_ylabel("Nombre de piézomètres \n(sur {})".format(len(list_pzo)), color='grey')
    ax.set_zorder(1)
    ax.set_frame_on(False)
    ax.set_xlim(x_limits)
    if save:
        plt.savefig(image_path + "secheresse_{}pzo.png".format(len(list_pzo)), bbox_inches='tight')
        plt.close()
    else: plt.show()
    return


def Display_drought_chronicle_all_pzo(pzo_ratio=0.75, save=False):
    """
    Affichage du graphe d'évolution des sécheresses pour tous les piézomètres Aqui-FR.

    Parameters
    ----------
    pzo_ratio : Flottant, optionnel
        Calcul du pourcentage si la part de piézomètres non disponibles est inférieure au ratio. Par défaut : 0.75.
    save : Booléen, optionnel
        Si Vrai, enregistrement du graphe. Par défaut : False.

    Returns
    -------
    None.

    """
    list_pzo = Get_pzo_list()
    df_drought_count = Compute_drought_dates(list_pzo, pzo_ratio=pzo_ratio)
    df_drought_count = df_drought_count.T

    jet_cmap = plt.cm.get_cmap('jet')
    previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    image_path = previous_folder+'/Images/sécheresses/'
    if not os.path.exists(image_path): os.makedirs(image_path)

    fig, ax = plt.subplots(figsize=(10,4))
    ax.grid()
    ax.set_title("Évolution sur l'ensemble du domaine Aqui-FR")
    ax.set_ylabel("Pourcentage des piézomètres \nen sécheresse")
    ax.plot(df_drought_count['Pourcentage_OBS'], color='tab:red', label='Observations')
    ax.plot(df_drought_count['Pourcentage_REA'], color=jet_cmap(0.1), label='Réanalyse')
    ax.legend()
    x_limits = ax.get_xlim()
    ax2 = ax.twinx()
    ax2.fill_between(df_drought_count.index, df_drought_count['Piézomètres']-df_drought_count['NaN'], step='mid', facecolor=(0.8, 0.8, 0.8, 0.7))
    ax2.set_ylabel("Nombre de piézomètres \n(sur {})".format(len(list_pzo)), color='grey')
    ax2.tick_params(axis='y', colors='grey')
    ax.set_zorder(1)
    ax.set_frame_on(False)
    ax.set_xlim([x_limits[0], pd.Timestamp.today()])
    if save:
        plt.savefig(image_path + "secheresse_Aqui-FR.png", bbox_inches='tight')
        plt.close()
    else: plt.show()
    return


def Display_drought_chronicle_by_appli(pzo_ratio=0.75, save=False):
    """
    Affichage des graphes d'évolution des sécheresses pour chacune des applications d'Aqui-FR.

    Parameters
    ----------
    pzo_ratio : Flottant, optionnel
        Calcul du pourcentage si la part de piézomètres non disponibles est inférieure au ratio. Par défaut : 0.75.
    save : Booléen, optionnel
        Si Vrai, enregistrement du graphe. Par défaut : False.

    Returns
    -------
    None.

    """
    list_appli = Get_appli_data().index.to_list()
    for appli in list_appli:
        list_pzo = Get_filtered_pzo_list(appli)
        df_drought_count = Compute_drought_dates(list_pzo, pzo_ratio=pzo_ratio).T
        cmap_REA = plt.cm.get_cmap('viridis')
        cmap_OBS = plt.cm.get_cmap('magma')

        previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
        image_path = previous_folder+'/Images/sécheresses_par_appli/'
        if not os.path.exists(image_path): os.makedirs(image_path)

        fig, ax = plt.subplots(figsize=(10,4))
        ax.grid()
        ax.set_title('Evolution for the "{}" application'.format(appli))
        ax.set_ylabel("Percentage of drought piezometers")
        ax.plot(df_drought_count['Pourcentage_OBS'], color=cmap_OBS(0.6), label='Observations')
        ax.plot(df_drought_count['Pourcentage_REA'], color=cmap_REA(0.4), label='Reanalyses',zorder=-1)
        ax.legend()
        ax2 = ax.twinx()
        ax2.fill_between(df_drought_count.index, df_drought_count['Piézomètres']-df_drought_count['NaN'], step='mid', facecolor=(0.8, 0.8, 0.8, 0.7))
        ax2.set_ylabel("# of piezometers (out of {})".format(len(list_pzo)), color='grey')
        ax2.tick_params(axis='y', colors='grey')
        ax.set_zorder(1)
        ax.set_frame_on(False)
        ax.set_xlim(to_date('1970-01-01'), pd.Timestamp.today())
        ax.set_ylim(0,100)
        ax2.set_ylim(0,len(list_pzo))
        if save:
            plt.savefig(image_path + "secheresse_{}.png".format(appli), bbox_inches='tight')
            plt.close()
        else: plt.show()
    return

