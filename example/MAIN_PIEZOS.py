#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  4 17:21:34 2023

@author: jeanteta
"""

#%% Extraction des packages d'intérêt
import os
from apyfr.experiment import Experiment
from apyfr.maps import *
from apyfr.extract_obs_piezos import *
from apyfr.piezometre import *

#%% Extraction du répertoire où AQUI-FR est stocké --> "AQUIFRDIR" doit être exporté en variable d'env.
#     !!!!!! IMPORTANT !!!!!!
#     1. "AQUIFRDIR" doit être défini dans le .bash_profile/.bashrc/... Au choix
#     2. Il faut exécuter l'IDE (ex. Spyder) depuis le terminal sinon les variables d'env ne seront pas lu
DIR_AQUI =  os.environ["AQUIFRDIR"]+"/"

#%% Téléchargement des données de tous les piezometres disponibles --> Ne pas exécuter à chaque fois
# download_all(update = True, infos = False)
# piezos_issues = pd.read_csv(folder_piezos+'Observations/Piezos_problemes_telechargement.csv')
# download_all(List_pzo = piezos_issues["Code"].tolist(), update = True, infos = False)

#%% Téléchargement des données de tous les piezometres disponibles --> Ne pas exécuter à chaque fois
#     Changement de répertoire vers le répertoire des résultats de simulations + Setting de l'expérience évaluée
os.chdir(DIR_AQUI+"Results/")
exp           = 'REA_V13_1958_2021'
monday        = 'mon'

#     Construction d'un objet de classe "Experiment"
EXP           = Experiment(DIR_AQUI+"Results/"+exp)

#     Construction d'un objet de classe "Piezometre"
PZO_EXP       = Piezometre(EXP, monday = monday)

#    Hauteurs piézométriques
STATS_PZO_EXP = PZO_EXP.Compare_H_PIEZOS(list_pzo   = None,
                                         date_range = None,
                                         save_stats = True)

#    SPLI
SPLI_SIM = PZO_EXP.Extract_SPLI_SIM(date_range = [to_date("2019-08-15"),to_date("2021-07-15")],
                                    plot_tempo = True, plot_spat= True,
                                    save_CDFs_ACC = True, USE_CDFs_ACC = True)
Compare_SLIM = PZO_EXP.Compare_SPLI_PIEZOS(date_range = [to_date("2000-08-15"),to_date("2021-07-15")],show = [True,True,True,True],
                                           plot_tempo = False)



