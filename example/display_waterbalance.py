# Author: LG (CNRM)
# Date: 15 September 2022
# post-processing for AQUI-FR project


"""
This code displays the water balance of AQUIFR simulation.
Requirements:
    1) 'q_deb' (for MARTHE only), 'q_nr' and 'q' at monthly time step.
    2) Simulations should contain full years, starting on 1st August.
"""


# =============================================================================
# IMPORT LIBRARIES
# =============================================================================

from matplotlib import pyplot as plt
from apyfr.experiment import Experiment
from apyfr.maps import map_applis
import numpy as np
from tqdm import trange

from netCDF4 import Dataset
import os
from os import environ
import datetime as dt
import aquifr_modules.dirs as aquidirs
#import collections.abc

from apyfr.maps import blank_map
from apyfr.maps import available_kwargs
from apyfr.maps import L2E_Proj 
from matplotlib import colors
from matplotlib import colorbar

#%% Extraction fonctions propres à "Example" dans APYFR (Modifs Alexis)
DIR_AQUI =  os.environ["AQUIFRDIR"]+"/"
os.chdir(DIR_AQUI+"LIB/python_modules/apyfr/example/")
# function to extract surfex forcings (drainage and runoff)
from extract_surfex_forcings import extract_surfex_forcings
from other_functions import days_in_month

#%% 
# Defining figures size
plt.rcParams["figure.figsize"] = (12, 9)

# Choose a name for the folder where results are printed
name_waterbalance_folder = 'water_balance_REA_1958-2022'
try:
    os.makedirs(name_waterbalance_folder )
except ValueError:
    print("The output folder ", name_waterbalance_folder, " already exists.")


# =============================================================================
# INFORMATION ABOUT THE SIMULATION
# =============================================================================

# Folder containing the simulation

# AquiFR output for this example can be found on the AquiFR ftp server:
#   ftp.umr-cnrm.fr:/apyfr_example/REA_TR_202103.tgz
# The file should be untarred within the example directory

#exp = '/home/guillaumotl/AquiFR/aqui-fr/Results/test_Bilans7_odic_test2'
#exp = '/cnrm/surface/guillaumotl/REA_V13_1958_2022'
#exp = '/home/guillaumotl/AquiFR/aqui-fr/Results/test_Bilans7'

#simu_dir   = '/cnrm/surface/munier/AquiFR/Results/'
#dirs_simus = {'REA'  : simu_dir+'REA_V13_1958_2021/'}
#exp = dirs_simus['REA']
exp = '/home/jeanteta/AquiFR/aqui-fr/Results/REA_V13_1958_2022/'

# Which applications ?
#applis = 'def' # default list of applications
applis_mart = ['mart_som', 'mart_poc', 'mart_bno', 'mart_npc', 'mart_teg']  # 'mart_als'
applis_odic = ['odic_somme', 'odic_loire', 'odic_seine',
               'odic_seine-eure', 'odic_seine-oise', 'odic_marne-oise', 'odic_marne-loing']  # 'odic_basse-normandie'
applis = applis_mart + applis_odic

# Time step of the outputs (monthly 'mon' or daily 'day'), only monthly can work here
monday = 'mon'

apyfr_path = DIR_AQUI+"LIB/python_modules/apyfr/"
HOME = environ['HOME']

# To extract drainage and runoff from SURFEX
#surfex_dir = '/home/guillaumotl/AquiFR/forcages_V8F/SURFEX_SAFRAN'
surfex_dir = HOME+'/PostDoc_AquiFR/01_DATA/01_RAW/forcages_V8F/SURFEX_SAFRAN'


# Time information about the simulation
date_start = dt.date(2010, 8, 1)#dt.date(2002, 8, 1)
date_end   = dt.date(2022, 7, 31) #dt.date(2004, 7, 31)
# Computing lag because the REA simulation started in August 1958
lag = int(date_start.year - 1958) * 12   # in month  # THIS OPTION IS NOT COMPLETED !!!
# If date_start = first day of the simulation, lag = 0 
#lag = 0

nb_days = (date_end  - date_start).days + 1
n_years = date_end.year - date_start.year
n_months = int(n_years * 12)
date_cour = np.array([date_start + dt.timedelta(days=i) for i in range(int(nb_days))])

# =============================================================================
# NEED TO ENTRY MANUALLY BASIN INLETS AND OUTLETS: (X,Y) COORDINATES
# =============================================================================
# !!!! A ne surtout pas toucher !!!!
# Outlets
exutoires = {'mart_som': np.array([[531180, 550241, 547318, 549166], [2563137, 2576819, 2584163, 2596231]]),
             'mart_poc': np.array([[441548, 400347, 396234, 389467, 355365, 341565, 301403, 294636, 330286, 407380, 415563, 463597, 487703],
                                   [2050452, 2078694, 2081303, 2086301, 2107355, 2115449, 2163484, 2167111, 2134292, 2221470, 2230803, 2207405, 2191703]]),
             'odic_somme': np.array([[546806, 531445, 598934, 607078, 553322, 641712], [2581184, 2564282, 2574171, 2570050, 2591796, 2498798]]),
             'odic_loire': np.array([[393555, 392494, 387385, 377599, 425600, 430477, 431605, 448457, 478810, 530361, 562539, 577600, 590670, 589326, 622549, 635702, 634408, 633463, 636498, 632385, 631506, 603508, 542701, 478711, 488712, 491399, 442535, 440678, 430494, 429432, 429531, 429465, 417556, 418518, 383487, ], [2273480, 2272452, 2267343, 2259465, 2219425, 2206388, 2205326, 2201379, 2183399, 2216539, 2236443, 2243475, 2246328, 2244520, 2271457, 2285506, 2286385, 2287529, 2298443, 2295507, 2294462, 2409440, 2402407, 2385390, 2380430, 2382421, 2355285, 2351271, 2355219, 2354456, 2326358, 2323406, 2318430, 2316439, 2285489]]),
             'odic_seine': np.array([[550335, 531461, 525521, 509664, 497783, 474582, 455264, 456433, 573455, 445501, 527497, 774211, 776649, 784523, 769475, 757306], [2580420, 2567604, 2561383, 2550531, 2548520, 2543281, 2532055, 2496997, 2322594, 2473500, 2346456, 2422565, 2424471, 2426488, 2409492, 2378478]]),
             'odic_basse-normandie': np.array([[412887, 356119, 387634, 397171, 403690, 411900, 422880, 435353, 369132, 418341],[2477586, 2493932, 2486866, 2485871, 2484129, 2479634, 2480895, 2487894,2485108, 2416365]]),
             'odic_seine-eure': np.array([[435765, 450284, 451788, 454773, 458798, 465544, 479001, 528749, 493298, 478539, 475023], [2484873, 2493857, 2493868, 2494355, 22494631, 2499618, 2500791, 2356587, 2383115, 2385879, 2392348]]),
             'odic_seine-oise': np.array([[458099, 475327, 494833, 499156, 506355, 510125, 526613, 531611, 544902, 546892, 678910, 478832, 468925, 465375, 444852, 546118],
                                          [2531343, 2540875, 2544833, 2546348, 2547354, 2547631, 2559883, 2563642, 2580096, 2579599, 2518616, 2500326, 2499851, 2500094, 2502316, 2518903]]),
             'odic_marne-oise': np.array([[701860, 682399, 692351, 673348, 718115, 727863, 767897, 768903, 772187, 773868, 777351, 778402, 782449, 786186, 787889, 787392, 784119, 779894],
                                          [2456151, 2489103, 2505866, 2517626, 2559866, 2541378, 2492597, 2487366, 2483916, 2478366, 2473390, 2467839, 2463847, 2456427, 2450157, 2449118, 2430629, 2427876]]),
             'odic_marne-loing': np.array([[629643, 634619, 704359, 703375, 721927, 702399],
                                          [2293348, 2376624, 2426317, 2455565, 2347849, 2338328]]),
             'mart_bno': np.array([[348696, 388685, 393943, 396806, 412073, 423739, 435762],
                                   [2488464, 2486850, 2486887, 2485864, 2479657, 2480174, 2488225]]),
             'mart_als': np.array([[971202, 976209, 982187, 986578, 990105],
                                   [2357274, 2357680, 2358191, 2358611, 2358899]]),
             'mart_npc': np.array([[584144, 618751, 619669, 651759, 678212, 687703, 611273, 547663, 550193],
                                   [2667350, 2656785, 2649890, 2642811, 2612844, 2545833, 2545354, 2597383, 2613310]]),
             'mart_teg': np.array([[476248], [1905646]])}

# Inlets
inflow = {'odic_seine': np.array([[646456, 641692, 691462, 691350, 696450, 733329, 742520, 758494, 761408, 770600, 773542, 761492, 720382, 720396, 555661, 445473, 787466, 780600, 775528, 768550, 756521], [2267486, 2259332, 2319386, 2326532, 2333453, 2361505, 2365316, 2377254, 2386305, 2408388, 2419513, 2501425, 2544399, 2548435, 2491491, 2466438, 2426264, 2425423, 2422425, 2411454, 2380383]]),
          'odic_loire': np.array([[378478, 383554, 395430, 401500, 403524, 418452, 448341, 452421, 453283, 465557, 470500, 487286, 494551, 498598, 515549, 526563, 531539, 530577, 530544, 542320, 550547, 556319, 576356, 577550, 583421, 585279, 589426, 622400, 623312, 636498, 635619, 634408, 630594, 632534, 634508, 637394, 487468, 471495, 442502, 441474, 439417, 429465, 428370, 429366, 417490, 393572],
                                  [2250209, 2247390, 2239494, 2230372, 2226491, 2219392, 2200218, 2192389, 2191360, 2186451, 2184394, 2190199, 2191294, 2190431, 2193483, 2197298, 2200450, 2209406, 2215444, 2222311, 2225396, 2244570, 2243409, 2242281, 2243243, 2244537, 2246328, 2270495, 2273215, 2285356, 2286434, 2287347, 2292422, 2294413, 2296486, 2298377, 2380331, 2351437, 2356513, 2355351, 2350342, 2355418, 2353295, 2325363, 2319392, 2310452]]),
          'odic_basse-normandie': np.array([[434375, 430875, 420135, 422142, 416660, 416146, 399895, 396831, 383661, 378122, 376090], [2404871, 2405335, 2416332, 2431301, 2434619, 2437612, 2462339, 2465362, 2469583, 2476367, 2480331]]),
          'odic_seine-eure': np.array([[], []]),
          'odic_seine-oise': np.array([[532097, 534364, 562893, 567637, 578628, 581625, 584632, 555368, 538638], [2527854, 2527124, 2505872, 2502621, 2491629, 2488610, 2484342, 2493393, 2507337]]),
          'odic_marne-oise': np.array([[], []]),
          'odic_marne-loing': np.array([[], []]),
          'odic_somme': np.array([[], []]),
          'mart_poc': np.array([[472442, 487317], [2149321, 2154340]]),
          'mart_als': np.array([[990766, 986876, 974669, 974236, 957569, 958601, 966951, 969370], [2316263, 2315909, 2316381, 2316368, 2316879, 2322545, 2334389, 2355160]]),
          'mart_npc': np.array([[726706], [2591335]]),
          'mart_teg': np.array([[486305, 501567, 507580, 537056, 538542, 541916, 538163, 534728, 531961, 529421, 527700, 526898, 525692, 524737, 529794, 525564, 520355, 516761, 514600, 509922, 509103, 505358, 502098, 501294, 501552, 505210, 499247, 493925, 492894, 487162, 486385, 481631],
                                [1903944, 1902583, 1904372, 1910114, 1908688, 1897437, 1893365, 1892581, 1891584, 1891045, 1890780, 1890513, 1890028, 1887245, 1877722, 1875986, 1872938, 1872923, 1867411, 1864098, 1866350, 1868586, 1868495, 1871528, 1874743, 1882832, 1881547, 1889724, 1891506, 1891915, 1895680, 1896673]])}

# Pour 'mart_poc': on enleve le point [443030, 2219392] (id_maille = 28 sur QGIS) qui renvoie un débit de sortie énorme et ne semble pas correct
# On considere aussi que les rivières se jetant dans la baie au dessus de l'Ile de Ré ne quittent pas le modèle: [[333515, 332321, 330330, 328605, 320467],[2146455, 2150259, 2152249, 2151188, 2151144]]


    
# =============================================================================
# IMPORTING GRID CELLS COORDINATES AND AREA OF THE SURFACE LAYER FOR EACH APPLI
# =============================================================================
    
grid_surf = {}  # for each application, it will contain X_sur, Y_sur, and associated cell area
nb_cells = 0  # total number of cells in 'h_surf'
for app in range(len(applis)):  # For each application
    
    if '_' in applis[app]:
        appli = applis[app].split('_')[1]  # = 'bno', 'som', 'seine',...
    
    # Importing cell coordinates of 'h_surf'
    with Dataset(apyfr_path+'/data/' + appli + '_L2E.nc') as nc:
        X_surf = nc['X_sur'][:]
        Y_surf = nc['Y_sur'][:]
        dx_surf = nc['D_sur'][:]
    
    # Initializing
    grid_surf[applis[app]] = np.zeros(((len(X_surf), 3)), dtype='float')  # area of each cell for all applications
    
    # Filling the dictionary
    grid_surf[applis[app]][:, 0] = X_surf  # X coordinate, in m
    grid_surf[applis[app]][:, 1] = Y_surf  # Y coordinate, in m
    grid_surf[applis[app]][:, 2] = dx_surf**2  # area of each cell, in m2
    
    appli_area = np.nansum(grid_surf[applis[app]][:, 2])
    print(applis[app], ' area = ', appli_area/1e6, ' km2') 
    
    nb_cells += len(X_surf)
    
    
# =============================================================================
# EXTRACTING SIMULATED DATA
# =============================================================================

print("\nExtracting simulated data\n")

# Simulated data and associated coordinates are saved in dictionnaries (key = application name)
GW_rivers_exchanges = {}
coord_rivers_exchanges = {}
GW_deb = {}
coord_deb = {}
q_riv = {}
coord_riv = {}
coord_surf = {}
topo = {}

im = 0
for mm in trange(n_months):  # For each month

    nb_daysinmonth = days_in_month(date_cour[im])
    
    for app in range(len(applis)):  # For each application
        
        if '_' in applis[app]:
            appli = applis[app].split('_')[1]  # = 'bno', 'som', 'seine',...
            model = applis[app].split('_')[0]  # = 'odic' or 'mart'

        EXP = Experiment(exp)
        
        # Extracting groundwater-river exchanges ('q_nr')
        EXP.read_results('q_nr', monday, ts=mm+lag, applis=applis[app])
        coord_q_nr, values_q_nr = EXP.map_applis('q_nr', unit='m3/d', display=False)
        
        # Extracting groundwater overflow ('q_deb'), only for MARTHE models as there is no overflow on ODIC
        if model == 'mart':
            EXP.read_results('q_deb', monday,ts=mm+lag, applis=applis[app])
            coord_q_deb, values_q_deb = EXP.map_applis('q_deb', unit='m3/d', display=False)
            
        # Extracting river discharge ('q')    
        EXP.read_results('q', monday, ts=mm+lag, applis=applis[app])
        coord_q_riv, values_q_riv = EXP.map_applis('q', display=False)
        
        if mm == 0:  # Only for the fist month, initializing dictionaries
            
            # For MARTHE: size q_nr = size rivers, FOR ODIC: size q_nr = size surface
            GW_rivers_exchanges[applis[app]] = np.zeros((n_months, len(coord_q_nr)))
            coord_rivers_exchanges[applis[app]] = coord_q_nr
            
            if model == 'mart':
                GW_deb[applis[app]] = np.zeros((n_months, len(grid_surf[applis[app]])))  # coordinates of 'h_surf', not coord_q_deb !
                coord_deb[applis[app]] = coord_q_deb[:len(grid_surf[applis[app]])]
            
            q_riv[applis[app]] = np.zeros((n_months, len(coord_q_riv)))
            coord_riv[applis[app]] = coord_q_riv
            
        # Saving the data of the month in dictionaries 
        GW_rivers_exchanges[applis[app]][mm] = values_q_nr * nb_daysinmonth  # m3/month
        if model == 'mart':
            GW_deb[applis[app]][mm] = values_q_deb[:len(grid_surf[applis[app]])] * nb_daysinmonth  # m3/month
        q_riv[applis[app]][mm] = values_q_riv * 24 * 3600 * nb_daysinmonth  # m3/month
        
    im += nb_daysinmonth  # updating the number of day

# test luca
#for app in range(len(applis)):
#    plt.figure()
#    plt.hist(np.reshape(GW_rivers_exchanges[applis[app]],-1), bins=200)
#    plt.title(applis[app])
#    plt.grid()

# Extracting recharge, runoff, storage changes and pumping volumes saved during the simulation
# They are saved in dictionnaries (key = application name)
pump = {}
storage_change = {}
rechactual = {}
runoffactual = {}
for app in range(len(applis)):

    if '_' in applis[app]:
        appli = applis[app].split('_')[1]  # = 'bno', 'som', 'seine',...
        model = applis[app].split('_')[0]  # = 'odic' or 'mart'
        
     # mart and odic models need to be treated separately beause data are saved
     # at the end of each day in 'mart' and at the end of each year in 'odic'
    if model == 'mart':
        pumping_filename = "{0}/{1}_pumping.txt".format(exp, applis[app])
        pump_temp = np.loadtxt(pumping_filename, delimiter='\t', skiprows=1, usecols=0, unpack=False, max_rows=nb_days+1)
        
        recharge_filename = "{0}/{1}_recharge.txt".format(exp, applis[app])
        rech_temp = np.loadtxt(recharge_filename, delimiter='\t', skiprows=1, usecols=0, unpack=False, max_rows=nb_days+1)
        
        runoff_filename = "{0}/{1}_runoff.txt".format(exp, applis[app])
        runoff_temp = np.loadtxt(runoff_filename, delimiter='\t', skiprows=1, usecols=0, unpack=False, max_rows=nb_days+1)
        
        storage_filename = "{0}/{1}_storage.txt".format(exp, applis[app])
        storage_temp = np.loadtxt(storage_filename, delimiter='\t', skiprows=1, usecols=0, unpack=False, max_rows=nb_days+1)
        
        # Unité de débit: bno, npc, som, als => m3/s, poc => 3.803-7 m3/s, teg => 'm3/h'
        # We want m3/day
        if appli == 'poc': 
            pump[applis[app]] = pump_temp * 3.803e-7 * 24 * 3600
            rechactual[applis[app]] = rech_temp * 3.803e-7 * 24 * 3600
            runoffactual[applis[app]] = runoff_temp * 3.803e-7 * 24 * 3600
            storage_change[applis[app]] = storage_temp * 3.803e-7 * 24 * 3600
        elif appli == 'teg': 
            pump[applis[app]] = pump_temp * 24
            rechactual[applis[app]] = rech_temp * 24
            runoffactual[applis[app]] = runoff_temp * 24
            storage_change[applis[app]] = storage_temp * 24
        else: 
            pump[applis[app]] = pump_temp * 24 * 3600
            rechactual[applis[app]] = rech_temp * 24 * 3600
            runoffactual[applis[app]] = runoff_temp * 24 * 3600
            storage_change[applis[app]] = storage_temp * 24 * 3600

    if model == 'odic':
        
        # for odic applications, we only have the annual pumping volume
        pumping_filename = "{0}/{1}_pumping.txt".format(exp, applis[app])
        pump_temp = np.loadtxt(pumping_filename, delimiter='\t', skiprows=1, usecols=0, unpack=False, max_rows=n_years+1)
        pump[applis[app]] = pump_temp  # in m3/yr

        # annual storage volume change
        storage_change_filename = "{0}/{1}_storage.txt".format(exp, applis[app])
        storage_change_temp = np.loadtxt(storage_change_filename, delimiter='\t', skiprows=1, usecols=0, unpack=False, max_rows=n_years+1)
        storage_change[applis[app]] = storage_change_temp  # in m3/yr
        
        # annual recharge volume
        recharge_filename = "{0}/{1}_recharge.txt".format(exp, applis[app])
        rech_temp = np.loadtxt(recharge_filename, delimiter='\t', skiprows=1, usecols=0, unpack=False, max_rows=n_years+1)
        rechactual[applis[app]] = rech_temp  # in m3/yr
        

# =============================================================================
# EXTRACTING DRAINAGE AND RUNOFF FROM SURFEX
# =============================================================================

# Drainage from SURFEX (also called recharge from SURFEX) can differ from
# recharge applied to the groudnwater model because of the NONSAT module in
# odic and because recharge and runoff are modified within mart for several
# applications

    
# File containing the coordinates of the SURFEX meshes
file_grid_surfex = apyfr_path + '/data/' + 'france_surfex_L2E'

# Computing monthly SURFEX drainage and runoff for each cell of each application
# They are saved in two dictionaries (keys = application names)
recharge_from_surfex, runoff_from_surfex = extract_surfex_forcings(date_start, n_years, n_months, date_cour,
                                                                   file_grid_surfex, surfex_dir, grid_surf)


# =============================================================================
# COMPUTING TOTAL VOLUMES FOR EACH APPLICATION
# =============================================================================

# Timeseries
recharge_time_serie = np.zeros((len(applis), n_months))  # mm/month
runoff_time_serie = np.zeros((len(applis), n_months))  # mm/month
q_nr_time_serie = np.zeros((len(applis), n_months))  # mm/month
q_deb_time_serie = np.zeros((len(applis), n_months))  # mm/month
q_riv_time_serie = np.zeros((len(applis), n_months))  # mm/month

# These variables are not timeseries
pump_total = np.zeros(len(applis))  # mm
rechactual_total = np.zeros(len(applis))  # mm
runoffactual_total = np.zeros(len(applis))  # mm
storagechange_total = np.zeros(len(applis))  # mm

for app in range(len(applis)):
    
    if '_' in applis[app]:
        appli = applis[app].split('_')[1]
        model = applis[app].split('_')[0]
    
    appli_area = np.nansum(grid_surf[applis[app]][:, 2])
    
    recharge_time_serie[app, :] = np.nansum(recharge_from_surfex[applis[app]], 1) / appli_area * 1000   # sum across the surface 
    runoff_time_serie[app, :] = np.nansum(runoff_from_surfex[applis[app]], 1) / appli_area * 1000 
    q_nr_time_serie[app, :] = np.nansum(GW_rivers_exchanges[applis[app]], 1) / appli_area * 1000  # *1000 to convert into mm/month
    if model == 'mart':
        q_deb_time_serie[app, :] = np.nansum(GW_deb[applis[app]], 1) / appli_area * 1000  # *1000 to convert into mm/month
    
    # Importing river coordinates
    with Dataset(apyfr_path+'/data/' + appli + '_L2E.nc') as nc:
        X_riv = nc['X_riv'][:]
        Y_riv = nc['Y_riv'][:]
    
    # for q_river, we keep only streamflow at the outlets minus streamflow at the inlets
    q_app = np.zeros((np.shape(exutoires[applis[app]])[1], n_months))
    for xx in range(len(q_app)):
        q_app[xx, :] = q_riv[applis[app]][:, np.argmin((exutoires[applis[app]][0, xx] - X_riv)**2 + (exutoires[applis[app]][1, xx] - Y_riv)**2)]
    q_riv_time_serie[app, :] = np.nansum(q_app, 0) / appli_area * 1000  # *1000 to convert into mm/month
    
    # We have inlets for several applications
    if 'odic' in applis[app] or applis[app] == 'mart_poc' or applis[app] == 'mart_als' or applis[app] == 'mart_npc' or applis[app] == 'mart_teg':
        q_app = np.zeros((np.shape(inflow[applis[app]])[1], n_months))
        for xx in range(len(q_app)):
            q_app[xx, :] = q_riv[applis[app]][:, np.argmin((inflow[applis[app]][0, xx] - X_riv)**2 + (inflow[applis[app]][1, xx] - Y_riv)**2)]
        q_riv_time_serie[app, :] = q_riv_time_serie[app, :] - np.nansum(q_app, 0) / appli_area * 1000  # *1000 to convert into mm/month
    
    # for these variables, we only get a total volume (not function of time)
    pump_total[app] = np.nansum(pump[applis[app]]) / appli_area * 1000  # in mm
    storagechange_total[app] = np.nansum(storage_change[applis[app]]) / appli_area * 1000  # in mm  
    rechactual_total[app] = np.nansum(rechactual[applis[app]]) / appli_area * 1000  # in mm   
    if model == 'mart':  # runoff is not modified in odic
        runoffactual_total[app] = np.nansum(runoffactual[applis[app]]) / appli_area * 1000  # in mm
        


# Displaying timeseries (to update)
display_timeseries = True
if display_timeseries:
    
    for app in range(len(applis)):
    
        if '_' in applis[app]:
            appli = applis[app].split('_')[1]
            model = applis[app].split('_')[0]
    
        fig0, (ax1, ax2) = plt.subplots(1, 2)
        ax1.plot(recharge_time_serie[app, :],'k', label='Recharge SURFEX')
        ax1.plot(runoff_time_serie[app, :],'grey', label='Ruissellement SURFEX')
        ax1.plot(q_riv_time_serie[app, :],'g', label='Débit de rivière')
        ax1.plot(q_nr_time_serie[app, :],'b', label='Echanges nappe-rivière')
        if model == 'mart':
            ax1.plot(q_deb_time_serie[app, :],'r', label='Débordement de nappe')
    
        ax1.legend(fontsize=14)
        ax1.grid()
        ax1.set_xlabel('Mois', fontsize=14)
        ax1.set_ylabel('Flux [mm/mois]', fontsize=14)
        
        ax2.plot(np.cumsum(recharge_time_serie[app, :]),'k', label='Recharge SURFEX')
        ax2.plot(np.cumsum(runoff_time_serie[app, :]),'grey', label='Ruissellement SURFEX')
        ax2.plot(np.cumsum(q_riv_time_serie[app, :]),'g', label='Débit de rivière (max)')
        ax2.plot(np.cumsum(q_nr_time_serie[app, :]),'b', label='Echanges nappe-rivière')
        if model == 'mart':
            ax2.plot(np.cumsum(q_deb_time_serie[app, :]),'r', label='Débordement de nappe')
    
        ax2.legend(fontsize=14)
        ax2.grid()
        ax2.set_xlabel('Mois', fontsize=14)
        ax2.set_ylabel('Flux cumulé [mm]', fontsize=14)
        
        title_text = applis[app] + ', period: ' + str(date_start.year) + '-' + str(date_end.year)
        plt.suptitle(title_text, fontsize=15, fontweight="bold")
        
        plt.savefig('{0}/timeseriesflow_{1}.png'.format(name_waterbalance_folder, applis[app]))
        plt.close()

# test for mart_teg
#test_teg = True
#if test_teg:
#    recharge_rate_yr = np.reshape(recharge_time_serie[app, :], (int(768/12),12))
#    runoff_rate_yr = np.reshape(runoff_time_serie[app, :], (int(768/12),12))
#    streamflow_rate_yr = np.reshape(q_riv_time_serie[app, :], (int(768/12),12))
#    gwriverexchange_rate_yr = np.reshape(q_nr_time_serie[app, :], (int(768/12),12))
#    pumping_rate_yr = -np.reshape(pump[applis[app]][:int(len(pump[applis[app]])/365)*365], (64, 365)) / appli_area * 1000
#    plt.figure()
#    plt.plot(np.sum(recharge_rate_yr,1),label='recharge')
#    plt.plot(np.sum(runoff_rate_yr,1),label='runoff')
#    plt.plot(np.sum(streamflow_rate_yr,1),label='debit de rivière')
#    plt.plot(np.sum(gwriverexchange_rate_yr,1),label='echanges nappe-riviere')
#    plt.plot(np.sum(pumping_rate_yr,1),label='pompages')
#    plt.ylabel('Flow [mm/yr]')
#    plt.xlabel('Months')
#    plt.legend()
#    plt.grid()
#    np.save('recharge_1958_2022_mart_teg.npy', recharge_rate_yr) 
#    np.save('runoff_1958_2022_mart_teg.npy', runoff_rate_yr) 
    
# =============================================================================
# DISPLAY WATER BALANCE
# =============================================================================

# Converting total volumes in mm/year:
recharge_rate_yr = np.sum(recharge_time_serie, 1) / n_years  # mm/yr
runoff_rate_yr = np.sum(runoff_time_serie, 1) / n_years
gwriverexchange_rate_yr = np.sum(q_nr_time_serie, 1) / n_years
overflow_rate_yr = np.sum(q_deb_time_serie, 1) / n_years
streamflow_rate_yr = np.sum(q_riv_time_serie, 1) / n_years
q_river = np.sum(q_riv_time_serie, 1) / n_years

pumping_rate_yr = pump_total / n_years
storagechange_rate_yr = storagechange_total / n_years
rechargeactual_rate_yr = rechactual_total / n_years
runoffactual_rate_yr = runoffactual_total / n_years 


# Plot circle diagram 
for app in range(len(applis)):
    
    if '_' in applis[app]:
        appli = applis[app].split('_')[1]
        model = applis[app].split('_')[0]
    
    # 1st diagram    
    # Pie chart: the slices will be ordered and plotted counter-clockwise:
    labels = ('DRAINAGE\n' + str(int(np.round(recharge_rate_yr[app]))) + ' mm/yr',
              '\nRUISSELLEMENT\n' + str(int(np.round(runoff_rate_yr[app]))) + ' mm/yr')
    sizes = [recharge_rate_yr[app], runoff_rate_yr[app]]
    colors_diagram=['royalblue','magenta']
    explode = (0.03, 0.03)
    fig1, (ax1, ax2, ax3) = plt.subplots(1, 3)
    wedges, texts = ax1.pie(sizes, explode=explode, labels=labels, shadow=False, startangle=180,
                            labeldistance=0.3, counterclock=False, colors=colors_diagram, radius=1.2)  #autopct='%1.1f%%'
    #ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    plt.setp(texts, size=10, weight="bold")
    ax1.set_title('Flux appliqués\npar SURFEX\n\n', fontsize=14, fontweight='bold')    
    
    # 2nd diagram 
    if model == 'mart':      
        labels = ('RECHARGE\n' + str(int(np.round(rechargeactual_rate_yr[app]))) + ' mm/yr',
                  '\nRUISSELLEMENT\n' + str(int(np.round(runoffactual_rate_yr[app]))) + ' mm/yr')
        sizes = [rechargeactual_rate_yr[app], runoffactual_rate_yr[app]]
        ax2.set_title("Flux appliqués\ndans MARTHE\n\n", fontsize=14, fontweight='bold')
    if model == 'odic':
        labels = ('RECHARGE\n' + str(int(np.round(rechargeactual_rate_yr[app]))) + ' mm/yr',
                  '\nRUISSELLEMENT\n' + str(int(np.round(runoff_rate_yr[app]))) + ' mm/yr')
        sizes = [rechargeactual_rate_yr[app], runoff_rate_yr[app]]  # because runoff is nnever modified in odic
        ax2.set_title("Flux appliqués\ndans EauDyssée\n\n", fontsize=14, fontweight='bold')        
    colors_diagram=['royalblue','magenta']
    explode = (0.03, 0.03)
    wedges, texts = ax2.pie(sizes, explode=explode, labels=labels, shadow=False, startangle=180,
                            labeldistance=0.3, counterclock=False, colors=colors_diagram, radius=1.2)  #autopct='%1.1f%%'
    #ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    plt.setp(texts, size=10, weight="bold")

    # 3rd diagram  
    labels = ('ECHANGES NAPPE-RIV\n' + str(int(np.round(gwriverexchange_rate_yr[app]+overflow_rate_yr[app]))) + ' mm/yr',
              'POMPAGE FIXE\n' + str(int(np.round(abs(pumping_rate_yr[app])))) + ' mm/yr',
              'STOCKAGE\n' + str(int(np.round(storagechange_rate_yr[app]))) + ' mm/yr\n\n\n\n')
    sizes = [gwriverexchange_rate_yr[app]+overflow_rate_yr[app], abs(pumping_rate_yr[app]), abs(storagechange_rate_yr[app])]
    colors_diagram=['orange','chocolate', 'orangered']
    explode = (0.03, 0.03, 0.03)   
    wedges3, texts3 = ax3.pie(sizes, explode=explode, labels=labels, shadow=False, startangle=90,
                              labeldistance=0.3, counterclock=False, colors=colors_diagram, radius=1.2)
    #ax2.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    plt.setp(texts3, size=10, weight="bold")
    if model == 'mart': 
        ax3.set_title("Bilan de masse\nde MARTHE\n\n", fontsize=14, fontweight='bold')
    if model == 'odic':
        ax3.set_title("Bilan de masse\nd'EauDyssée\n\n", fontsize=14, fontweight='bold')
        
    title_text = '\n\n\n\n\nBilans de masse pour ' + applis[app] +  '\nperiod: ' + str(date_start.year) + '-' + str(date_end.year)    
    plt.suptitle(title_text, fontsize=16,fontweight="bold")

    plt.savefig('{0}/waterbalance_{1}.png'.format(name_waterbalance_folder, applis[app]))    
    plt.close()


# =============================================================================
# RATIO RIVER INFILTRATION (Q_NR < 0) / GW DISCHARGE (Q_NR > 0 + Q_DEB)
# =============================================================================


# =============================================================================
# DISPLAY MEAN FLOW ON MAPS
# =============================================================================

default_extent = {'france':(61000,1029000,1725000,2705000),
                  'seine':(400000,815000,2240000,2640000),
                  'somme':(515000,725000,2465000,2625000),
                  'loire':(335000,680000,2150000,2435000),
                  'basse-normandie':(330000,470000,2380000,2515000),
                  'marne-loing':(580000,815000,2250000,2480000),
                  'marne-oise':(645000,825000,2390000,2590000),
                  'seine-eure':(400000,585000,2330000,2540000),
                  'seine-oise':(415000,715000,2415000,2600000),
                  'som':(500000,700000,2480000,2625000),
                  'npc':(515000,750000,2515000,2700000),
                  'bno':(330000,490000,2375000,2520000),
                  'poc':(260000,525000,2025000,2265000),
                  'als':(940000,1015000,2285000,2375000),
                  'teg':(465000,560000,1850000,1920000)
                  }

associated_grid_size = {}
for app in range(len(applis)): # for each application
    
    if '_' in applis[app]:
        appli = applis[app].split('_')[1]
        model = applis[app].split('_')[0]
        
    extent = default_extent[appli]

    figw_max,figh_max = 1000,1000
    fig_margin = (20,20,20,30) # (left,right,bottom,top)
    colorbar_width = 160
    fig_dpi = 100
    
    L2E = L2E_Proj()
    
    # Graph for SURFEX drainage (recharge)
    ax = blank_map(applis=applis,extent=extent,zone=None,syrah=None, draw_colorbar=True)
    
    # Averaging SURFEX recharge over time    
    values = np.nansum(recharge_from_surfex[applis[app]], 0) / n_years / grid_surf[applis[app]][:, 2] * 1000  # converting in mm/yr
    
    #values[values <= 0] = np.nan
    Inan = np.isfinite(values)
    #values[~Inan] = 0
    values = np.where(Inan, values, 0)
    values[values == 0] = np.nan
       
    vlim = np.nanmin(values[Inan]), np.nanmax(values[Inan])
    #vlim = np.nanpercentile(values, 5), np.nanpercentile(values, 95)
    
    cmap  = plt.cm.viridis
    # getting the original colormap using cm.get_cmap() function
    orig_map=plt.cm.get_cmap('viridis')
  
    # reversing the original colormap using reversed() function
    reversed_map = orig_map.reversed()
    
    norm = colors.Normalize(vmin=vlim[0], vmax=vlim[1])
    #norm = colors.LogNorm(vmin=vlim[0], vmax=vlim[1]) 
    
    coord2 = grid_surf[applis[app]]
        
    hnan = ax.scatter(coord2[~Inan,0], coord2[~Inan,1], 5,[.5,.5,.5],
                      marker='s',lw=0,transform=L2E)
    h = ax.scatter(coord2[Inan,0], coord2[Inan,1], 5, values[Inan], marker='s',
                   lw=0,transform=L2E,cmap=cmap,norm=norm)
    
    # adding rivers in black
    h_riv = ax.scatter(coord_riv[applis[app]][:,0], coord_riv[applis[app]][:,1], 1, 'r', marker='s',
                   alpha=0.5, lw=0,transform=L2E,cmap=cmap,norm=norm)
    
    figw,figh = ax.figure.get_size_inches()*ax.figure.get_dpi()
    axw,axh = figw*ax.get_position().width,figh*ax.get_position().height
    cbar_ax = plt.axes([(fig_margin[0]+axw+20)/figw,fig_margin[2]/figh,30/figw,axh/figh])
    cb = colorbar.ColorbarBase(cbar_ax,cmap=cmap,norm=norm,spacing='uniform',extend='both')
    cb.ax.tick_params(labelsize=10,size=0)
    
    # Adapt marker size when resizing/zooming
    def get_msize(hh,II):
        ax_width,ax_height = ax.get_position().extents[2:]
        fx,fy = ax.figure.get_size_inches()*ax.figure.dpi
        msizex = np.sqrt(coord2[II,2])/np.diff(ax.get_extent()[:2])*fx*ax_width
        msizey = np.sqrt(coord2[II,2])/np.diff(ax.get_extent()[2:])*fy*ax_height
        msize = np.minimum(msizex,msizey)
        return msize
    def update_msize(*args):
        hnan.set_sizes(get_msize(hnan,~Inan)**2/2)
        h.set_sizes(get_msize(h,Inan)**2/2)
    
    update_msize()
    ax.callbacks.connect('xlim_changed',update_msize)
    ax.callbacks.connect('ylim_changed',update_msize)
    ax.figure.canvas.mpl_connect('draw_event',update_msize)
    
    title_colorbar = 'Recharge SURFEX [mm/yr]'
    cb.set_label(title_colorbar, fontsize=14)
    
    title_text = applis[app]
    ax.set_title(title_text, fontsize=16)
    plt.savefig('{0}/recharge_{1}.png'.format(name_waterbalance_folder, applis[app]))
    plt.close()
    
    # Same graph but for gw-river exchanges: THIS PART IS NOT FINISHED !!!!
    
    ax = blank_map(applis=applis,extent=extent,zone=None,syrah=None, draw_colorbar=True)

#    # Importing river coordinates
#    with Dataset(apyfr_path+'/data/' + appli + '_L2E.nc') as nc:
#        X_riv = nc['X_riv'][:]
#        Y_riv = nc['Y_riv'][:]
            
    if model == 'mart': # for mart models, we need to project gw-river exchanges on X_sur, Y_sur grid
        values = np.nansum(GW_deb[applis[app]], 0)[:len(grid_surf[applis[app]])]
        values[np.isnan(values)] = 0.0  # new line to avoid to sum gw-rive exchange with gw overflow = nan in some cells
        for ii in range(len(coord_riv[applis[app]][:, 0])):  # For each river cell
            distance = np.min(np.sqrt((grid_surf[applis[app]][:, 0] - coord_riv[applis[app]][ii, 0])**2 + (grid_surf[applis[app]][:, 1] - coord_riv[applis[app]][ii, 1])**2))
            if distance <= 10:  # check that distance is very small to be sure that we are not adding a river not included within the hsurf grid
                # Looking for the closer X_sur,Y_sur cell
                correspond_ind = np.argmin((grid_surf[applis[app]][:, 0] - coord_riv[applis[app]][ii, 0])**2 + (grid_surf[applis[app]][:, 1] - coord_riv[applis[app]][ii, 1])**2)
                values[correspond_ind] = np.nansum(GW_rivers_exchanges[applis[app]][:, ii]) + values[correspond_ind]

        
        values = values / n_years / grid_surf[applis[app]][:, 2] * 1000  # converting in mm/yr
    
    if model == 'odic':
        values = np.nansum(GW_rivers_exchanges[applis[app]], 0)
        if appli == 'loire' or appli == 'basse-normandie' or appli == 'seine' or appli == 'seine-oise' or appli == 'marne-oise' or appli == 'marne-loing':
            associated_grid_size[applis[app]] = np.zeros(len(coord_rivers_exchanges[applis[app]][:, 0]))
            for ii in range(len(coord_rivers_exchanges[applis[app]][:, 0])):  # For each gw-river exchange cell
                distance = np.min(np.sqrt((grid_surf[applis[app]][:, 0] - coord_rivers_exchanges[applis[app]][ii, 0])**2 + (grid_surf[applis[app]][:, 1] - coord_rivers_exchanges[applis[app]][ii, 1])**2))
                if distance <= 10:  # check that distance is very small to be sure that we are not adding a river not included within the hsurf grid
                    # Looking for the closer X_sur,Y_sur cell
                    correspond_ind = np.argmin((grid_surf[applis[app]][:, 0] - coord_rivers_exchanges[applis[app]][ii, 0])**2 + (grid_surf[applis[app]][:, 1] - coord_rivers_exchanges[applis[app]][ii, 1])**2)
                    associated_grid_size[applis[app]][ii] = grid_surf[applis[app]][correspond_ind, 2]
                    values[ii] = values[ii] / n_years / grid_surf[applis[app]][correspond_ind, 2] * 1000  # converting in mm/yr
                else:
                    values[ii] = 0
        else:
            values = values / n_years / grid_surf[applis[app]][:, 2] * 1000  # converting in mm/yr
    
    Inan = np.isfinite(values)
    #values[~Inan] = 0
    values = np.where(Inan, values, 0)
    values[values == 0] = np.nan
       
    vlim = np.nanmin(values[Inan]), np.nanmax(values[Inan])
    vlim = np.nanpercentile(values[Inan], 15), np.nanpercentile(values[Inan], 80)
    
    cmap  = plt.cm.viridis
    # getting the original colormap using cm.get_cmap() function
    orig_map=plt.cm.get_cmap('viridis')
  
    # reversing the original colormap using reversed() function
    reversed_map = orig_map.reversed()
    
    norm = colors.Normalize(vmin=vlim[0], vmax=vlim[1])
    #norm = colors.LogNorm(vmin=vlim[0], vmax=vlim[1]) 
    
    coord2 = grid_surf[applis[app]]
    # specific for several odic applications:
    if appli == 'loire' or appli == 'basse-normandie' or appli == 'seine' or appli == 'seine-oise' or appli == 'marne-oise' or appli == 'marne-loing':
        coord2 = coord_rivers_exchanges[applis[app]]
        coord2[:, 2] = associated_grid_size[applis[app]]
        
    hnan = ax.scatter(coord2[~Inan,0], coord2[~Inan,1], 5,[.5,.5,.5],
                      marker='s',lw=0,transform=L2E)
    h = ax.scatter(coord2[Inan,0], coord2[Inan,1], 7, values[Inan], marker='s',
                   lw=0,transform=L2E,cmap=cmap,norm=norm)
    
    # adding rivers in black
    h_riv = ax.scatter(coord_riv[applis[app]][:,0], coord_riv[applis[app]][:,1], 1, 'r', marker='s',
                   alpha=0.5, lw=0,transform=L2E,cmap=cmap,norm=norm)
    
    figw,figh = ax.figure.get_size_inches()*ax.figure.get_dpi()
    axw,axh = figw*ax.get_position().width,figh*ax.get_position().height
    
    # Adapt marker size when resizing/zooming
    def get_msize(hh,II):
        ax_width,ax_height = ax.get_position().extents[2:]
        fx,fy = ax.figure.get_size_inches()*ax.figure.dpi
        msizex = np.sqrt(coord2[II,2])/np.diff(ax.get_extent()[:2])*fx*ax_width
        msizey = np.sqrt(coord2[II,2])/np.diff(ax.get_extent()[2:])*fy*ax_height
        msize = np.minimum(msizex,msizey)
        return msize
    def update_msize(*args):
#        hnan.set_sizes(get_msize(hnan,~Inan)**2/2)
        h.set_sizes(get_msize(h,Inan)**2/2)
        
    update_msize()
    ax.callbacks.connect('xlim_changed',update_msize)
    ax.callbacks.connect('ylim_changed',update_msize)
    ax.figure.canvas.mpl_connect('draw_event',update_msize)
    
    cbar_ax = plt.axes([(fig_margin[0]+axw+20)/figw,fig_margin[2]/figh,30/figw,axh/figh])
    cb = colorbar.ColorbarBase(cbar_ax,cmap=cmap,norm=norm,spacing='uniform',extend='both')
    cb.ax.tick_params(labelsize=10,size=0)
    
    title_colorbar = 'Echanges nappe-rivière [mm/yr]'
    cb.set_label(title_colorbar, fontsize=14)
    
    title_text = applis[app]
    ax.set_title(title_text, fontsize=16)
    plt.savefig('{0}/gwriver_{1}.png'.format(name_waterbalance_folder, applis[app]))
    plt.close()

plt.show()


