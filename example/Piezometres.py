#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 12 10:39:33 2021
@author: andre.mounier@ens-paris-saclay.fr

    Lecture des bases de donnée des piézomètres des applications AquiFR :
     - une base de donnée pour les piézomètres
    - une base de donnée pour les applications
"""

import os
import re
import numpy as np
import pandas as pd


def Get_pzo_data():
    """
    Récupération des données brutes "liste_piezos_L2E" si "piezometres.csv" inexistant

    Returns
    -------
    Dataframe_Piezometres : Dataframe pandas
        Caractérisation des piézomètres.

    """
    #modif Luca
    #previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    #data_path = previous_folder+'/Donnees/'
    data_path = 'Donnees_piezo/'

    if 'piezometres.csv' not in os.listdir(data_path):
        #modif Luca
        #raw_data_path = previous_folder+'/Donnees/liste_piezo_L2E/'
        raw_data_path = 'Donnees_piezo/liste_piezo_L2E/'
        files = os.listdir(raw_data_path)
        applications = [re.findall(r"_([^\s]+)_", f)[0] for f in files]
        Dataframe_pzo = pd.DataFrame()
        for a in applications:
            data_array = np.genfromtxt(raw_data_path+'piezos_'+a+'_L2E', dtype=str, delimiter = ',', autostrip=True)
            multi_index = [(a,code.replace('/','_')) for code in data_array[:,0]]
            dataset = pd.DataFrame(data=data_array[:,1:], columns=["Maille","x_L2E","y_L2E","Nappe"],index = pd.MultiIndex.from_tuples(multi_index, names=["Application","Code"]))
            Dataframe_pzo = Dataframe_pzo.append(dataset)
        print(Dataframe_pzo)
        Dataframe_pzo = Dataframe_pzo[['Maille', 'Nappe', 'x_L2E', 'y_L2E']]
        Dataframe_pzo.to_csv(data_path+'piezometres.csv')
    Dataframe_pzo = pd.read_csv(data_path+'piezometres.csv',index_col=[0,1])
    return Dataframe_pzo


def Get_appli_data():
    """
    Récupération des descriptions des applications

    Returns
    -------
    Dataframe_Applications : Dataframe pandas
        Caractérisation des applications.

    """
    previous_folder = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    data_path = previous_folder+'/Donnees_piezo/'
    Dataframe_Applications = pd.read_csv(data_path+'applications.csv',index_col=0)
    return Dataframe_Applications


def Which_appli(pzo):
    """
    Accès à ou aux applications d'une station donnée.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code d'un piézomètre (avec un "_")

    Returns
    -------
    applications : Liste
        Liste des applications utilisant le piézomètre

    """
    Dataframe_pzo = Get_pzo_data()
    List_pzo = Dataframe_pzo.index.get_level_values(1).to_list()
    idx = [i for i, x in enumerate(List_pzo) if x == pzo]
    appli_pzo = Dataframe_pzo.index.to_list()
    applications = []
    for i in idx:
        applications.append(appli_pzo[i][0])
    return applications

def Get_pzo_list():
    """
    Liste des piézomètres relatifs à AquiFR, sans répétition.

    Returns
    -------
    List_pzo : Liste
        Liste non répétée des codes des piézomètres.

    """
    Dataframe_pzo = Get_pzo_data()
    List_pzo = list(set(Dataframe_pzo.index.get_level_values(1).to_list()))
    return List_pzo

def Get_filtered_pzo_list(appli):
    """
    Liste des piézomètres faisant partie d'une application.

    Parameters
    ----------
    appli : Chaîne de caractères
        Abréviation d'une application.

    Returns
    -------
    Liste_Piezometres_filtered : Liste
        Liste des piézomètres inclus dans l'application.

    """
    Dataframe_pzo = Get_pzo_data()
    Dataframe_pzo_filtered = Dataframe_pzo[Dataframe_pzo.index.get_level_values(0)==appli]
    List_pzo_filtered = Dataframe_pzo_filtered.index.get_level_values(1).to_list()
    return List_pzo_filtered


def Get_infos(pzo):
    """
    Accès aux informations (maille, nappe, coordonnées) d'un piézomètre en particulier.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code d'un piézomètre (avec un "_")

    Returns
    -------
    infos : Dataframe
        Lignes de 'Get_pzo_data' relatives à un piézomètre.

    """
    Dataframe_pzo = Get_pzo_data()
    infos = Dataframe_pzo.iloc[[i for i, x in enumerate(Dataframe_pzo.index.get_level_values(1).to_list()) if x == pzo]]
    return infos







