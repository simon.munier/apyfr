from matplotlib import pyplot as plt
from apyfr.experiment import Experiment, default_map_applis
from apyfr.maps import map_applis

#===============================================================================
# AquiFR output for this example can be found on the AquiFR ftp server:
#   ftp.umr-cnrm.fr:/apyfr_example/REA_TR_202103.tgz
# The file should be untarred within the example directory

#exp = 'REA_V14_TR_2023_2024'
exp = 'REA_V14_TR_1958_2024'
monday = 'mon'

var = 'spli_surf'

#time_step = 0
time_step = '2024-01-15'

#applis = None # list of applications of experiment
applis = 'def' # default list of applications
#applis = 'odic_seine'

extent = None # default map extent depending on application list
#extent = (260000,680000,2025000,2435000)
#extent = (320000,640000,2330000,2580000)
#extent = (514000,740000,2490000,2700000)

vlim = None

#===============================================================================

EXP = Experiment(exp)
EXP.read_results(var,monday,ts=time_step,applis=applis)
EXP.map_applis(var,extent=extent,vlim=vlim,syrah=True,logo=False)

#===============================================================================

## Alternatively, map_applis can be used directely
#applis_data = {appli:EXP.data[var][appli] for appli in default_map_applis}
#title = 'all applis from '+exp+' on '+time_step
#map_applis(applis_data,extent=extent,vlim=vlim,cmap='spli',syrah=True,title=title,logo=False)

plt.show()
