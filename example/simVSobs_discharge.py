# Author: LG (CNRM)
# Date: 15 September 2022
# post-processing for AQUI-FR project


"""
This code compares simulated and observed streamflow from river stations.
Requirements:
    - Simulation during long periods in order to compute SPLI (SDI for
      discharge) properly
    - Note that we computed SPLI on log(Q)
    - Simulated streamflow ('q') has to be saved at a monthly time step
"""


# =============================================================================
# IMPORT LIBRARIES
# =============================================================================

from matplotlib import pyplot as plt
from apyfr.experiment import Experiment, default_map_applis
from apyfr.maps import map_applis
import numpy as np

from netCDF4 import Dataset, num2date
import os
#from os import environ
import datetime as dt
#import aquifr_modules.dirs as aquidirs
from datetime import datetime

import pandas as pd

# to compute SPLI indices
from spli_functions import compute_monthly_CDF, compute_monthly_SPLI
import cftime

# to compare observed and simulated discharge
from func_compar_obs import KGE, NS, RMSE, QMNA5, moving_average

apyfr_path = '/home/guillaumotl/AquiFR/aqui-fr/LIB/python_modules/apyfr'

# Defining figures size
plt.rcParams["figure.figsize"] = (12, 9)

# Choose a name for the folder where results are printed
name_output_folder = 'streamflowcomparison_REA_1958-2022_v2'
try:
    os.makedirs(name_output_folder)
except ValueError:
    print("The output folder ", name_output_folder, " already exists.")

# =============================================================================
# INFORMATION ABOUT THE SIMULATION
# =============================================================================

# Folder containing the simulation

# AquiFR output for this example can be found on the AquiFR ftp server:
#   ftp.umr-cnrm.fr:/apyfr_example/REA_TR_202103.tgz
# The file should be untarred within the example directory

# Directories containing the simulation:
#work_dir   = '/home/muniers/AquiFR/PSE/'
#simu_dir   = '/cnrm/surface/munier/AquiFR/Results/'
#dirs_simus = {'REA'  : simu_dir+'REA_V13_1958_2021/'}

var = 'q'
#exp = dirs_simus['REA']
exp = '/cnrm/surface/guillaumotl/REA_V13_1958_2022'
exp = '/home/guillaumotl/AquiFR/Results_Luca/REA_1958_2022'

# Time step of the outputs (monthly 'mon' or daily 'day')
monday = 'mon'

# Dates of the simulation
datesM = {}
#datesM['PARP6'] = pd.date_range('1993-{0:02}-01'.format(month_start),periods=nb_months_PS,freq='MS')+pd.DateOffset(days=14)
#datesM['PCLIM'] = pd.date_range('1993-{0:02}-01'.format(month_start),periods=nb_months_PS,freq='MS')+pd.DateOffset(days=14)
#for yy in range(1994,2017):
#  datesM['PARP6'] = datesM['PARP6'].append(pd.date_range(str(yy)+'-{0:02}-01'.format(month_start),periods=nb_months_PS,freq='MS')+pd.DateOffset(days=14))
#  datesM['PCLIM'] = datesM['PCLIM'].append(pd.date_range(str(yy)+'-{0:02}-01'.format(month_start),periods=nb_months_PS,freq='MS')+pd.DateOffset(days=14))
#datesM['REA'] = pd.date_range(start='1958-08-01',end='2019-07-01',freq='MS') + pd.DateOffset(days=14)
#datesM['OBS'] = pd.date_range(start='1958-08-01',end='2019-08-01',freq='MS')+pd.DateOffset(days=14)
#datesM['REA_TR'] = pd.date_range(start='2019-08-01',end='2019-08-01',freq='MS')+pd.DateOffset(days=14)
#datesM['PARP6_TR'] = pd.date_range('2019-{0:02}-01'.format(month_start),periods=nb_months_PS,freq='MS')+pd.DateOffset(days=14)
#datesM['PCLIM_TR'] = pd.date_range('2019-{0:02}-01'.format(month_start),periods=nb_months_PS,freq='MS')+pd.DateOffset(days=14)

# Applications where we want comparisons
applis_mart = ['mart_som','mart_poc','mart_bno','mart_npc']#,'mart_teg','mart_als',]  # no stations for teg ???
applis_odic = ['odic_somme','odic_loire','odic_seine',
               'odic_seine-eure','odic_seine-oise','odic_marne-oise','odic_marne-loing']  # 'odic_basse-normandie'
applis = applis_mart + applis_odic

extent = None # default map extent depending on application list
#extent = (260000,680000,2025000,2435000)
#extent = (320000,640000,2330000,2580000)
#extent = (514000,740000,2490000,2700000)

vlim = None

# Time information about the simulation
date_start = dt.date(1958, 8, 1)
date_end = dt.date(2022, 7, 1)

n_years = date_end.year - date_start.year
n_months = int(n_years * 12)
# Computing lag because the REA simulation started in August 1958
lag = int(date_start.year - 1958) * 12   # in month


path_list_stations_hydro = apyfr_path + '/example/stations_hydro/'

# =============================================================================
# IMPORTING GRID CELLS COORDINATES AND AREA OF THE SURFACE LAYER FOR EACH APPLI
# =============================================================================

grid_surf = {}  # for each application, it will contain X_sur, Y_sur, and associated cell area
nb_cells = 0  # total number of cells in 'h_surf'
for app in range(len(applis)):  # For each application

    if '_' in applis[app]:
        appli = applis[app].split('_')[1]  # = 'bno', 'som', 'seine',...

    # Importing cell coordinates of 'h_surf'
    with Dataset(apyfr_path+'/data/' + appli + '_L2E.nc') as nc:
        X_surf = nc['X_sur'][:]
        Y_surf = nc['Y_sur'][:]
        dx_surf = nc['D_sur'][:]

    # Initializing
    grid_surf[applis[app]] = np.zeros(((len(X_surf), 3)), dtype='float')  # area of each cell for all applications

    # Filling the dictionary
    grid_surf[applis[app]][:, 0] = X_surf  # X coordinate, in m
    grid_surf[applis[app]][:, 1] = Y_surf  # Y coordinate, in m
    grid_surf[applis[app]][:, 2] = dx_surf**2  # area of each cell, in m2

    appli_area = np.nansum(grid_surf[applis[app]][:, 2])
    print(applis[app], ' area = ', appli_area/1e6, ' km2')

    nb_cells += len(X_surf)


# =============================================================================
# EXTRACTING MONTHLY SIMULATED RIVER FLOW FOR ALL APPLICATIONS
# =============================================================================

print('Extracting simulated streamflow')
# Extracting simulated river flow ('q') for each month of the simulation and
# for each grid cell of each application.

# Simulated river flow and associated coordinates (X,Y) are saved in two
# dictionnaries (keys = applications names)
q_riv = {}
coord = {}

for mm in range(n_months):  # for each month of the simulation

    for app in range(len(applis)):  # for each application

        # Extracting river discharge ('q')
        EXP = Experiment(exp)
        EXP.read_results('q', monday, ts=mm+lag, applis=applis[app])
        coord_q_riv, values_q_riv = EXP.map_applis('q', display=False)

        if mm == 0:  # Only for the fist month, initializing dictionaries

            # Initializing each array
            # For mart models: size q_nr = size rivers
            # For odic models: size q_nr = size surface
            q_riv[applis[app]] = np.zeros((n_months, len(coord_q_riv)))
            coord[applis[app]] = coord_q_riv

        # Saving simulated data of the current month in the dictionary
        q_riv[applis[app]][mm] = values_q_riv * 24 * 3600  # in m3/d


# =============================================================================
# IMPORTING OBSERVED RIVER FLOW FOR ALL DISCHARGE STATION
# =============================================================================

# Station names, codes and indices will be saved in three dictionnaries
# (keys = applications names)
index_stations = {}
name_stations = {}
code_stations = {}

for appli in applis:  # For each application

    if '_' in appli:
        appli = appli.split('_')[1]  # = 'bno', 'som', 'seine',...
        model = appli.split('_')[0]  # = 'odic' or 'mart'

        # Name of the file containing the river station
        file_names_stations = path_list_stations_hydro + 'stations_' + appli #+ '.txt'

        code_stations[appli] = np.genfromtxt(file_names_stations, dtype='str', usecols=[0], skip_header=1, delimiter=',')
        index_stations[appli] = np.genfromtxt(file_names_stations, dtype='int', usecols=[1], skip_header=1, delimiter=',')
        name_stations[appli] = np.genfromtxt(file_names_stations, dtype='str', usecols=[6], skip_header=1, delimiter=',')


# List of simulated dates at monthly time step
dates = pd.date_range(start=str(date_start), end=str(date_end), freq='MS')

# Name of the file containing observed river flow
#filename_observedQ = '/cnrm/surface/munier/Data/Rivers/FRENCH/FRENCH_1900-2020_daily.nc'
filename_observedQ = '/home/guillaumotl/Documents/Data/Rivers_munier/FRENCH_1900-2020_daily.nc'

print('Importing and converting observed streamflow')
# Importing the data
with Dataset(filename_observedQ) as nc:
    data_obs = nc['data'][:]  # river flows
    time_obs = num2date(nc['time'][:], nc['time'].units)  # associated dates
    id_obs = nc['nat_id'][:]  # identifier of the station
    start_obs = nc['start'][:]  # first date for each station
    end_obs = nc['end'][:]  # last date for each station
    area_obs = nc['area'][:]  # upstream area of each station

# Converting observed streamflow from daily to monthly time step

time_obs_monthly = []  # Vector containing observation dates, in month since January 1st, 1900
data_obs_monthly = np.zeros((np.shape(data_obs)[0],
                             (time_obs[-1].year - time_obs[0].year + 1) * 12))  # 2D array containing monthly observed data (rows=stations, columns=time), in m3/day

cnt = 0
month = 999
for dd in range(np.shape(data_obs)[1]):  # for each day of observation

    if time_obs[dd].month != month:  # we start a new month

        if dd > 0:  # if we are not in the first day of observation
            # saving dates and monthly value
            #data_obs_monthly[:, cnt-1] = som_month
            # remove nan and negative values:
            temp_array = np.copy(data_obs[:, int(dd1):dd])  # saving daily observed data for the current month
            temp_array[temp_array < 0] = np.nan  # Replacing empty data by np.nan values
            ratio_nan = np.count_nonzero(np.isnan(temp_array), 1) / np.shape(temp_array)[1]  # Ratio between empty data and the number of days in the month
            # Saving the average daily river flow at monthly time step
            data_obs_monthly[:, cnt-1] = np.where(ratio_nan > 0.2, np.nan, np.nanmean(temp_array, 1) * 24 *3600)  # replacing by np.nan values if not enough records during the month
            time_obs_monthly.append(first_day)  # storing the first day of the asociated month

        if dd < np.shape(data_obs)[1] - 1:  # if we are not in the last day of observation
            dd1 = dd * 1  # storing the absolute day index of the first day of the month
            first_day = datetime.strptime(str(time_obs[dd]),'%Y-%m-%d %H:%M:%S')  # storing the first day of the month
            month = time_obs[dd].month  # storing the index of the month

        cnt += 1  # Update the month index

# Observed river flow for the last month need to be saved outside the loop
temp_array = np.copy(data_obs[:,int(dd1):dd])  # saving daily observed data for the current month
temp_array[temp_array < 0] = np.nan
ratio_nan = np.count_nonzero(np.isnan(temp_array), 1) / np.shape(temp_array)[1]
# Saving the average daily river flow at monthly time step
data_obs_monthly[:, cnt-1] = np.where(ratio_nan > 0.2, np.nan, np.nanmean(temp_array, 1) * 24 *3600)  # replacing by np.nan values if not enough records during the month
time_obs_monthly.append(first_day)  # storing the index of the month


# For each station, keeping only dates where there are observed and simulated data

# Observed and simulated monthly data, as well as associated dates and upstream
# areas are saved in dictionaries of dictionaries (1st key = applications name,
# 2nd key = code name of the station)
dates_comparison = {}
simulated_Q = {}
observed_Q = {}
stations_area = {}

code_stations2 = code_stations.copy()
index_stations2 = index_stations.copy()
name_stations2 = name_stations.copy()

for app in range(len(applis)):  # for each application

    n_stations = 0  # count the number of stations we keep for each application

    dates_comparison[applis[app]] = {}
    observed_Q[applis[app]] = {}
    simulated_Q[applis[app]] = {}
    stations_area[applis[app]] = {}

    if '_' in applis[app]:
        appli = applis[app].split('_')[1]  # = 'bno', 'som', 'seine',...
        model = applis[app].split('_')[0]  # = 'odic' or 'mart'

    # Converting into list codes, indices and names of all stations inside the current application
    code_stations[appli] = code_stations[appli].tolist()
    index_stations[appli] = index_stations[appli].tolist()
    name_stations[appli] = name_stations[appli].tolist()

    for stat in range(len(code_stations2[appli])):  # for each gauging station inside the current application

        if code_stations2[appli][stat] in id_obs: # test if the station is in the observation database

            # test if the station is in the 'hsurf' grid (ie. simulated with a groundwater model)
            if coord[applis[app]][index_stations2[appli][stat], 0] in grid_surf[applis[app]][:, 0] and coord[applis[app]][index_stations2[appli][stat], 1] in grid_surf[applis[app]][:, 1]:

                # Searching the first date in common between simulated and observed data
                day_start = start_obs[id_obs == code_stations2[appli][stat]][0]  # 1st day of observation for the current gauging station
                if datetime.strptime(day_start,'%Y-%m-%d') < dates[0]: # => dates[0] = first date of the simulation, will be the first date for the comparison
                    id_start_sim = 0  # saving start id for simulated data
                    id_start_obs = np.argmin(np.abs(dates[0]-np.array(time_obs_monthly)))# saving start id for observed data
                else:
                    id_start_sim = np.argmin(np.abs(datetime.strptime(day_start,'%Y-%m-%d') - dates))
                    id_start_obs = np.argmin(np.abs(datetime.strptime(day_start,'%Y-%m-%d') - np.array(time_obs_monthly)))

                # Searching the last date in common between simulated and observed data
                day_end = end_obs[id_obs == code_stations2[appli][stat]][0]  # last day of observation for the current gauging station
                if datetime.strptime(day_end,'%Y-%m-%d') < dates[-1]:
                    id_end_sim = np.argmin(np.abs(datetime.strptime(day_end,'%Y-%m-%d') - dates))
                    id_end_obs = np.argmin(np.abs(datetime.strptime(day_end,'%Y-%m-%d') - np.array(time_obs_monthly)))
                else:   # dates[-1] = last date of the simluation, will be the last date for the comparison
                    id_end_sim = len(dates)
                    id_end_obs = np.argmin(np.abs(dates[-1] - np.array(time_obs_monthly))) + 1

                # Keeping this station if it contains more than X years of data
                if id_end_obs - id_start_obs >= 10 * 12:

                    # then, keeping the station if it contains more than 75% of non nan values:
                    temp_data = data_obs_monthly[id_obs == code_stations2[appli][stat], id_start_obs:id_end_obs]
                    if np.count_nonzero(~np.isnan(temp_data)) / len(temp_data) > 0.75:

                        # Simulated and observed monthly data can be saved in dictionaries
                        # as well as associated dates and upstream areas

                        dates_comparison[applis[app]][code_stations2[appli][stat]] = {}
                        dates_comparison[applis[app]][code_stations2[appli][stat]] = dates[id_start_sim:id_end_sim]

                        simulated_Q[applis[app]][code_stations2[appli][stat]] = {}
                        simulated_Q[applis[app]][code_stations2[appli][stat]] = q_riv[applis[app]][id_start_sim:id_end_sim, index_stations2[appli][stat]]

                        observed_Q[applis[app]][code_stations2[appli][stat]] = {}
                        observed_Q[applis[app]][code_stations2[appli][stat]] = data_obs_monthly[id_obs == code_stations2[appli][stat], id_start_obs:id_end_obs]

                        simulated_Q[applis[app]][code_stations2[appli][stat]] = np.where(np.isnan(observed_Q[applis[app]][code_stations2[appli][stat]]), np.nan,
                                   simulated_Q[applis[app]][code_stations2[appli][stat]])

                        stations_area[applis[app]][code_stations2[appli][stat]] = {}
                        stations_area[applis[app]][code_stations2[appli][stat]] = area_obs[id_obs == code_stations2[appli][stat]]

                        n_stations += 1

                    else:  # code, index and name are removed from the list
                        print(' - station : ', code_stations2[appli][stat], ' for ', applis[app], ' has not enough data.')
                        code_stations[appli].remove(code_stations2[appli][stat])
                        index_stations[appli].remove(index_stations2[appli][stat])
                        name_stations[appli].remove(name_stations2[appli][stat])
                else:
                    print(' - station : ', code_stations2[appli][stat], ' for ', applis[app], ' has not enough data.')
                    code_stations[appli].remove(code_stations2[appli][stat])
                    index_stations[appli].remove(index_stations2[appli][stat])
                    name_stations[appli].remove(name_stations2[appli][stat])
            else:
                print(' - station : ', code_stations2[appli][stat], ' for ', applis[app], ' is not in the database.')
                code_stations[appli].remove(code_stations2[appli][stat])
                index_stations[appli].remove(index_stations2[appli][stat])
                name_stations[appli].remove(name_stations2[appli][stat])

        else:
            print(' - station : ', code_stations2[appli][stat], ' for ', applis[app], ' is not in the database.')
            code_stations[appli].remove(code_stations2[appli][stat])
            index_stations[appli].remove(index_stations2[appli][stat])
            name_stations[appli].remove(name_stations2[appli][stat])

    print('Number of stations used for the comparison for ', applis[app], ' : ', n_stations)


# =============================================================================
# COMPUTING SPLI ON OBSERVED AND SIMULATED STREAMFLOW
# =============================================================================

print('Computing SDI')
# SPLI indices will be saved inside dictionaries (key=station code) of dictionaries (key=application name)
simulated_spli = {}
observed_spli = {}

window_size = 1  # smoothing monthly streamflow with a moving average (no smoothing if window_size = 1)

for app in range(len(applis)):  # for each application

    appli = applis[app].split('_')[1]  # = 'bno', 'som', 'seine',...

    # Initializing dictionaries
    observed_spli[applis[app]] = {}
    simulated_spli[applis[app]] = {}

    for stat in range(len(code_stations[appli])):  # for each gauging station (because each station have different time period)

        if code_stations[appli][stat] in observed_Q[applis[app]]:

            # SPLI is computed if we have enough observed data to compute CDF
            if np.count_nonzero(~np.isnan(observed_Q[applis[app]][code_stations[appli][stat]][0])) > 12 * 10:  # if at least 10 years of data

                # Defining dates
                start_month = dates_comparison[applis[app]][code_stations[appli][stat]][0]
                end_month = dates_comparison[applis[app]][code_stations[appli][stat]][-1]
                date_spli = pd.date_range(start=str(start_month)[0:10], end=str(end_month)[0:10], freq='MS') #+pd.DateOffset(days=14)
                date_spliV2 = []

                for mm in range(len(date_spli)):
                    temp_date = str()
                    date_spliV2.append(cftime.DatetimeGregorian(date_spli[mm].year, date_spli[mm].month, date_spli[mm].day))
                date_spliV2 = np.array(date_spliV2)
                period = [start_month.year, end_month.year]

                # Computing CDFs (cumulative density function first) for observed and simulated streamflow
                [cdf_obs, min1_obs, max1_obs] = compute_monthly_CDF(date_spliV2, np.log10(moving_average(observed_Q[applis[app]][code_stations[appli][stat]][0], window_size)),
                                                              period=period, ncfile=None, use_only_one_station=True)
                [cdf_sim, min1_sim, max1_sim] = compute_monthly_CDF(date_spliV2, np.log10(moving_average(simulated_Q[applis[app]][code_stations[appli][stat]][0], window_size)),
                                                              period=period, ncfile=None, use_only_one_station=True)

                # Computing monthly SPLI indices for observed and simulated streamflow
                spli_obs = compute_monthly_SPLI(date_spliV2, np.log10(moving_average(observed_Q[applis[app]][code_stations[appli][stat]][0], window_size)),
                                                CDF_ncfile=[cdf_obs, min1_obs, max1_obs], use_netcdf=False, use_only_one_station=True)
                spli_sim = compute_monthly_SPLI(date_spliV2, np.log10(moving_average(simulated_Q[applis[app]][code_stations[appli][stat]][0], window_size)),
                                                CDF_ncfile=[cdf_sim, min1_sim, max1_sim], use_netcdf=False, use_only_one_station=True)

                # Initializing and filling dictionary of dictionary
                simulated_spli[applis[app]][code_stations[appli][stat]] = {}
                simulated_spli[applis[app]][code_stations[appli][stat]] = spli_sim

                observed_spli[applis[app]][code_stations[appli][stat]] = {}
                observed_spli[applis[app]][code_stations[appli][stat]] = spli_obs

            else:
                simulated_spli[applis[app]][code_stations[appli][stat]] = {}
                simulated_spli[applis[app]][code_stations[appli][stat]] = np.nan

                observed_spli[applis[app]][code_stations[appli][stat]] = {}
                observed_spli[applis[app]][code_stations[appli][stat]] = np.nan
        else:
            simulated_spli[applis[app]][code_stations[appli][stat]] = {}
            simulated_spli[applis[app]][code_stations[appli][stat]] = np.nan

            observed_spli[applis[app]][code_stations[appli][stat]] = {}
            observed_spli[applis[app]][code_stations[appli][stat]] = np.nan


# =============================================================================
# COMPARING OBSERVED AND SIMULATED RIVER FLOW WITH SEVERAL CRITERIA
# =============================================================================

print('Comparing observed and simulated streamflow')

# Computing several criteria, and store them in dictionnaries
KGE_values = {}
NSE_values = {}
QMNA5_values = {}  # it contains KGE of QMNA5 in fact.
RMSEspli_values = {}  # this one is to compare SPLI timeseries (not streamflow)

# they are also stored in lists
KGE_list = []
NSE_list = []
QMNA5_list = []
RMSEspli_list = []

current_app = 'xxx'
for app in range(len(applis)):  # for each application

    appli = applis[app].split('_')[1]  # = 'bno', 'som', 'seine',...

    # Initialize dictionaries
    KGE_values[applis[app]] = {}
    NSE_values[applis[app]] = {}
    QMNA5_values[applis[app]] = {}
    RMSEspli_values[applis[app]] = {}

    for stat in range(len(code_stations[appli])):  # for each gauging station inside the current application

        # Initialize sub-dictionaries
        KGE_values[applis[app]][code_stations[appli][stat]] = {}
        NSE_values[applis[app]][code_stations[appli][stat]] = {}
        QMNA5_values[applis[app]][code_stations[appli][stat]] = {}
        RMSEspli_values[applis[app]][code_stations[appli][stat]] = {}

        if code_stations[appli][stat] in observed_Q[applis[app]]:

            # Computing criteria for streamflow
            KGE_values[applis[app]][code_stations[appli][stat]] = KGE(simulated_Q[applis[app]][code_stations[appli][stat]][0],
                      observed_Q[applis[app]][code_stations[appli][stat]][0])
            NSE_values[applis[app]][code_stations[appli][stat]] = NS(simulated_Q[applis[app]][code_stations[appli][stat]][0],
                      observed_Q[applis[app]][code_stations[appli][stat]][0])
            # Computing QMNA5: low streamflow occuring 20% of the time for a given month
            QMNA5_values[applis[app]][code_stations[appli][stat]] = QMNA5(simulated_Q[applis[app]][code_stations[appli][stat]][0],
                        observed_Q[applis[app]][code_stations[appli][stat]][0], dates_comparison[applis[app]][code_stations[appli][stat]])
            # Computing criteria for SPLI, # RMSE is normalized by standard deviation of observed SPLI
            RMSEspli_values[applis[app]][code_stations[appli][stat]] = RMSE(simulated_spli[applis[app]][code_stations[appli][stat]],
                      observed_spli[applis[app]][code_stations[appli][stat]]) / np.nanstd(observed_spli[applis[app]][code_stations[appli][stat]])

        else:
            KGE_values[applis[app]][code_stations[appli][stat]] = np.nan
            NSE_values[applis[app]][code_stations[appli][stat]] = np.nan
            QMNA5_values[applis[app]][code_stations[appli][stat]] = np.nan
            RMSEspli_values[applis[app]][code_stations[appli][stat]] = np.nan

        KGE_list.append(KGE_values[applis[app]][code_stations[appli][stat]])
        NSE_list.append(NSE_values[applis[app]][code_stations[appli][stat]])
        QMNA5_list.append(QMNA5_values[applis[app]][code_stations[appli][stat]])
        RMSEspli_list.append(RMSEspli_values[applis[app]][code_stations[appli][stat]])

    print('Median of KGE values for ', applis[app], ' : ',  np.median(list(KGE_values[applis[app]].values())))


# =============================================================================
# DISPLAY RESULTS
# =============================================================================


color_list = ['firebrick', 'sienna', 'olivedrab', 'red', 'green', 'royalblue', 'navy',
              'm', 'grey', 'orange', 'fuchsia', 'chocolate', 'cyan', 'pink']  # one color per application


# Plot time-averaged simulated vs observed streamflow at each river station in m3/day

plt.figure()
plt.plot([1e4, 5e8], [1e4, 5e8], 'k') # draw the 1/1 line in black

for app in range(len(applis)):  # for each application

    appli = applis[app].split('_')[1]
    color = color_list[app]  # we change color for each application

    # Sort river stations to display the name of the biggest river stations
    mean_flow = np.zeros(len(code_stations[appli]))
    for stat in range(len(code_stations[appli])):  # for each gauging station
        if code_stations[appli][stat] in observed_Q[applis[app]]:
            mean_flow[stat] = np.nanmean(observed_Q[applis[app]][code_stations[appli][stat]][0])  # computing time-averaged observed streamflow
        else:
            mean_flow[stat] = 0
    sorted_id = np.argsort(np.where(np.isnan(mean_flow), 0, mean_flow))  # Sorted in function of the mean observed streamflow

    for stat in range(len(code_stations[appli])):  # for each gauging station

        if code_stations[appli][stat] in observed_Q[applis[app]]:

            if stat == 0:  # for the first station, we define the label = application name
                plt.scatter(np.nanmean(observed_Q[applis[app]][code_stations[appli][stat]][0]), np.nanmean(simulated_Q[applis[app]][code_stations[appli][stat]][0]),
                            30, color, label=applis[app], alpha=0.8, linewidths=0)
            else:
                plt.scatter(np.nanmean(observed_Q[applis[app]][code_stations[appli][stat]][0]), np.nanmean(simulated_Q[applis[app]][code_stations[appli][stat]][0]),
                            30, color, alpha=0.6, linewidths=0)

    # Write the name of the biggest station for the current application
    plt.text(np.nanmean(observed_Q[applis[app]][code_stations[appli][sorted_id[-1]]][0]) * 0.7,
             np.nanmean(simulated_Q[applis[app]][code_stations[appli][sorted_id[-1]]][0]) * 1.05,
             name_stations[appli][sorted_id[-1]], fontsize=6)

plt.xlabel('Observed average streamflow [m3/d]', fontsize=14)
plt.ylabel('Simulated average streamflow [m3/d]', fontsize=14)
plt.xscale('log')
plt.yscale('log')
plt.xlim(1e4,5e8)
plt.ylim(1e4,5e8)
plt.grid('both')
plt.legend()
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)

# saving figure
plt.savefig('{0}/meanObsVSmeanSim.png'.format(name_output_folder))


# Plot time-averaged simulated vs observed streamflow at each river station in mm/yr

plt.figure()
plt.plot([0, 600], [0, 600], 'k')

for app in range(len(applis)):  # for each application

    appli = applis[app].split('_')[1]
    color = color_list[app]  # we change color for each application

    # Sort river stations to display the name of the biggest river stations
    mean_flow = np.zeros(len(code_stations[appli]))
    for stat in range(len(code_stations[appli])):  # for each gauging station
        if code_stations[appli][stat] in observed_Q[applis[app]]:
            mean_flow[stat] = np.nanmean(observed_Q[applis[app]][code_stations[appli][stat]][0])  # computing time-averaged observed streamflow
        else:
            mean_flow[stat] = 0
    sorted_id = np.argsort(np.where(np.isnan(mean_flow), 0, mean_flow))  # Sorted in function of the mean observed streamflow

    for stat in range(len(code_stations[appli])):  # for each gauging station

        if code_stations[appli][stat] in observed_Q[applis[app]]:

            if stat == 0:  # for the first station, we define the label = application name
                plt.scatter(np.nanmean(observed_Q[applis[app]][code_stations[appli][stat]][0]) / stations_area[applis[app]][code_stations[appli][stat]] * 1000 / 1e6 * 365.25,
                            np.nanmean(simulated_Q[applis[app]][code_stations[appli][stat]][0]) / stations_area[applis[app]][code_stations[appli][stat]] * 1000 / 1e6 * 365.25,
                            30, color, label=applis[app], alpha=0.8, linewidths=0)
            else:
                plt.scatter(np.nanmean(observed_Q[applis[app]][code_stations[appli][stat]][0]) / stations_area[applis[app]][code_stations[appli][stat]] * 1000 / 1e6 * 365.25,
                            np.nanmean(simulated_Q[applis[app]][code_stations[appli][stat]][0]) / stations_area[applis[app]][code_stations[appli][stat]] * 1000 / 1e6 * 365.25,
                            30, color, alpha=0.6, linewidths=0)

    # Write the name of the biggest station for the current application
    plt.text(np.nanmean(observed_Q[applis[app]][code_stations[appli][sorted_id[-1]]][0]) / stations_area[applis[app]][code_stations[appli][sorted_id[-1]]] * 1000 / 1e6 * 365.25 - 20,
             np.nanmean(simulated_Q[applis[app]][code_stations[appli][sorted_id[-1]]][0]) / stations_area[applis[app]][code_stations[appli][sorted_id[-1]]] * 1000 / 1e6 * 365.25 + 10,
             name_stations[appli][sorted_id[-1]], fontsize=6)

plt.xlabel('Observed average streamflow [mm/yr]', fontsize=14)
plt.ylabel('Simulated average streamflow [mm/yr]', fontsize=14)
plt.xlim(0,600)
plt.ylim(0,600)
plt.grid()
plt.legend()
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)

# saving figure
plt.savefig('{0}/meanObsVSmeanSim2.png'.format(name_output_folder))


# Plot KGE in function of mean observed streamflow

plt.figure()
for app in range(len(applis)):  # for each application

    appli = applis[app].split('_')[1]
    color = color_list[app]  # we change color for each application

    for stat in range(len(code_stations[appli])):  # for each gauging station

        if stat == 0:  # for the first station, we define the label = application name
            plt.scatter(np.nanmean(observed_Q[applis[app]][code_stations[appli][stat]][0]),
                        KGE_values[applis[app]][code_stations[appli][stat]],
                        30, color, label=applis[app], alpha=0.8, linewidths=0)
        else:

            if code_stations[appli][stat] in observed_Q[applis[app]]:
                plt.scatter(np.nanmean(observed_Q[applis[app]][code_stations[appli][stat]][0]),
                        KGE_values[applis[app]][code_stations[appli][stat]],
                        30, color, alpha=0.6, linewidths=0)

plt.xlabel('Observed average streamflow [m3/d]', fontsize=14)
plt.ylabel('KGE', fontsize=14)
plt.xscale('log')
plt.grid()
plt.legend()
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)

# saving figure
plt.savefig('{0}/KGEVSmeanObs.png'.format(name_output_folder))

# Plot KGE of QMNA5 in function of mean observed streamflow

plt.figure()
for app in range(len(applis)):  # for each application

    appli = applis[app].split('_')[1]
    color = color_list[app]  # we change color for each application

    for stat in range(len(code_stations[appli])):  # for each gauging station

        if stat == 0:  # for the first station, we define the label = application name
            plt.scatter(np.nanmean(observed_Q[applis[app]][code_stations[appli][stat]][0]),
                        QMNA5_values[applis[app]][code_stations[appli][stat]],
                        30, color, label=applis[app], alpha=0.8, linewidths=0)
        else:

            if code_stations[appli][stat] in observed_Q[applis[app]]:
                plt.scatter(np.nanmean(observed_Q[applis[app]][code_stations[appli][stat]][0]),
                        QMNA5_values[applis[app]][code_stations[appli][stat]],
                        30, color, alpha=0.6, linewidths=0)

plt.xlabel('Observed average streamflow [m3/d]', fontsize=14)
plt.ylabel('KGE of QMNA5', fontsize=14)
plt.xscale('log')
plt.grid()
plt.legend()
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)

# saving figure
plt.savefig('{0}/QMNA5VSmeanObs.png'.format(name_output_folder))

# Plot histogram of KGE

plt.figure()
txt_label = 'mean : ' + str(round(np.nanmean(KGE_list), 2)) + '\nmedian : ' + str(round(np.nanmedian(KGE_list), 2)) + '\nstd : ' + str(round(np.nanstd(KGE_list), 2))
plt.hist(KGE_list, bins=50, label=txt_label)
plt.xlabel('KGE values', fontsize=14)
plt.ylabel('Distribution', fontsize=14)
plt.grid()
plt.legend(fontsize=13)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)

# saving figure
plt.savefig('{0}/histKGE.png'.format(name_output_folder))

# Plot histogram of KGE of QMNA5

plt.figure()
txt_label = 'mean : ' + str(round(np.nanmean(QMNA5_list), 2)) + '\nmedian : ' + str(round(np.nanmedian(QMNA5_list), 2)) + '\nstd : ' + str(round(np.nanstd(QMNA5_list), 2))
plt.hist(QMNA5_list, bins=50, label=txt_label)
plt.xlabel('KGE of QMNA5', fontsize=14)
plt.ylabel('Distribution', fontsize=14)
plt.grid()
plt.legend(fontsize=13)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)

# saving figure
plt.savefig('{0}/histQMNA5.png'.format(name_output_folder))


# Plot cumulative distributions (like in Vergnes et al., 2020)

plt.figure()
#txt_label = 'mean : ' + str(round(np.nanmean(KGE_list), 2)) + '\nmedian : ' + str(round(np.nanmedian(KGE_list), 2)) + '\nstd : ' + str(round(np.nanstd(KGE_list), 2))
KGE_list = np.array(KGE_list)
plt.plot(np.sort(KGE_list[~np.isnan(KGE_list)]),
         100 * np.arange(len(KGE_list[~np.isnan(KGE_list)]), 0, -1) / len(KGE_list[~np.isnan(KGE_list)]), label='KGE')
NSE_list = np.array(NSE_list)
plt.plot(np.sort(NSE_list[~np.isnan(NSE_list)]),
         100 * np.arange(len(NSE_list[~np.isnan(KGE_list)]), 0, -1) / len(NSE_list[~np.isnan(NSE_list)]), label='NSE')
QMNA5_list = np.array(QMNA5_list)
plt.plot(np.sort(QMNA5_list[~np.isnan(QMNA5_list)]),
         100 * np.arange(len(QMNA5_list[~np.isnan(QMNA5_list)]), 0, -1) / len(QMNA5_list[~np.isnan(QMNA5_list)]), label='KGE on QMNA5')
RMSEspli_list = 1 - np.array(RMSEspli_list)
plt.plot(np.sort(RMSEspli_list[~np.isnan(RMSEspli_list)]),
         100 * np.arange(len(RMSEspli_list[~np.isnan(RMSEspli_list)]), 0, -1) / len(RMSEspli_list[~np.isnan(RMSEspli_list)]), label='1 - nRMSE on SDI')
plt.xlabel('Values of the criteria', fontsize=14)
plt.ylabel('Number of gauging stations [%]', fontsize=14)
plt.title('Cumulative distribution of several criteria (rivers)', fontsize=16)
plt.xlim(0, 1)
plt.ylim(0, 100)
plt.grid()
plt.legend(fontsize=14)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)

# saving figure
plt.savefig('{0}/cumulativeNSE.png'.format(name_output_folder))

#  Plot streamflow and SPLI (called SDI for Standardized Discharge Index) of streamflow for one or two stations

plt.figure()
app = 'odic_seine'
reg = 'seine'
stat = 2
plt.subplot(2, 1, 1)
window_size = 1  # smoothing monthly streamflow with a moving average (no smoothing if window_size = 1)
plt.plot_date(dates_comparison[app][code_stations[reg][stat]], moving_average(observed_Q[app][code_stations[reg][stat]][0], window_size),
              label='observed', marker=None, linewidth=3, color='k', linestyle='-')
plt.plot_date(dates_comparison[app][code_stations[reg][stat]], moving_average(simulated_Q[app][code_stations[reg][stat]][0], window_size),
              label='simulated', marker=None, linewidth=2, color='r', linestyle='-')
plt.grid()
plt.legend()
plt.yscale('log')
plt.ylabel('Streamflow [m3/d]', fontsize=14)
txt_title = name_stations[reg][stat] + ' (' + app + ') : KGE = ' + str(round(KGE_values[app][code_stations[reg][stat]], 2))
plt.title(txt_title, fontsize=14)

plt.subplot(2, 1, 2)
plt.plot_date(dates_comparison[app][code_stations[reg][stat]], observed_spli[app][code_stations[reg][stat]],
              label='observed', marker=None, linewidth=3, color='k', linestyle='-')
plt.plot_date(dates_comparison[app][code_stations[reg][stat]], simulated_spli[app][code_stations[reg][stat]],
              label='simulated', marker=None, linewidth=2, color='r', linestyle='-')
plt.grid()
plt.legend()
plt.ylabel('SDI', fontsize=14)
plt.ylim(-3, 3)
txt_title = name_stations[reg][stat] + ' (' + app + ') : nRMSE(SDI) = ' + str(round(RMSEspli_values[app][code_stations[reg][stat]], 2))
plt.title(txt_title, fontsize=14)

# saving figure
plt.savefig('{0}/exampleSDI.png'.format(name_output_folder))

plt.figure()
app = 'mart_som'
reg = 'som'
stat = 0
plt.subplot(2, 1, 1)
window_size = 1  # smoothing monthly streamflow with a moving average (no smoothing if window_size = 1)
plt.plot_date(dates_comparison[app][code_stations[reg][stat]], moving_average(observed_Q[app][code_stations[reg][stat]][0], window_size),
              label='observed', marker=None, linewidth=3, color='k', linestyle='-')
plt.plot_date(dates_comparison[app][code_stations[reg][stat]], moving_average(simulated_Q[app][code_stations[reg][stat]][0], window_size),
              label='simulated', marker=None, linewidth=2, color='r', linestyle='-')
plt.grid()
plt.legend()
plt.yscale('log')
plt.ylabel('Streamflow [m3/d]', fontsize=14)
txt_title = name_stations[reg][stat] + ' (' + app + ') : KGE = ' + str(round(KGE_values[app][code_stations[reg][stat]], 2))
plt.title(txt_title, fontsize=14)

plt.subplot(2, 1, 2)
plt.plot_date(dates_comparison[app][code_stations[reg][stat]], observed_spli[app][code_stations[reg][stat]],
              label='observed', marker=None, linewidth=3, color='k', linestyle='-')
plt.plot_date(dates_comparison[app][code_stations[reg][stat]], simulated_spli[app][code_stations[reg][stat]],
              label='simulated', marker=None, linewidth=2, color='r', linestyle='-')
plt.grid()
plt.legend()
plt.ylabel('SDI', fontsize=14)
plt.ylim(-3, 3)
txt_title = name_stations[reg][stat] + ' (' + app + ') : nRMSE(SDI) = ' + str(round(RMSEspli_values[app][code_stations[reg][stat]], 2))
plt.title(txt_title, fontsize=14)

# saving figure
plt.savefig('{0}/exampleSDI2.png'.format(name_output_folder))


#  Plot KGE in function of RMSE of SPLI (called SDI for Standardized Discharge Index)

plt.figure()
for app in range(len(applis)):  # for each application

    appli = applis[app].split('_')[1]
    color = color_list[app]  # we change color for each application

    for stat in range(len(code_stations[appli])):  # for each gauging station

        if stat == 0:  # for the first station, we define the label = application name
            plt.scatter(KGE_values[applis[app]][code_stations[appli][stat]],
                        RMSEspli_values[applis[app]][code_stations[appli][stat]],
                        30, color, label=applis[app], alpha=0.8, linewidths=0)
        else:
            if code_stations[appli][stat] in observed_Q[applis[app]]:
                plt.scatter(KGE_values[applis[app]][code_stations[appli][stat]],
                        RMSEspli_values[applis[app]][code_stations[appli][stat]],
                        30, color, alpha=0.6, linewidths=0)

plt.xlabel('KGE (on streamflow)', fontsize=14)
plt.ylabel('normalized RMSE (on SDI)', fontsize=14)
plt.grid()
plt.legend()
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)

# saving figure
plt.savefig('{0}/KGEVSRMSEofSDI.png'.format(name_output_folder))


# Plot histogram of RMSE of SPLI (called SDI for Standardized Discharge Index)

plt.figure()
txt_label = 'mean : ' + str(round(np.nanmean(RMSEspli_list), 2)) + '\nmedian : ' + str(round(np.nanmedian(RMSEspli_list), 2)) + '\nstd : ' + str(round(np.nanstd(RMSEspli_list), 2))
plt.hist(RMSEspli_list, bins=50, label=txt_label)
plt.xlabel('normalized RMSE on SDI', fontsize=14)
plt.ylabel('Distribution', fontsize=14)
plt.grid()
plt.legend(fontsize=14)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)

# saving figure
plt.savefig('{0}/histRMSEofSDI.png'.format(name_output_folder))

# Plot river stations on the map

# Default figure size and axes margins
figw_max,figh_max = 1000,1000
fig_margin = (20,20,20,30) # (left,right,bottom,top)
colorbar_width = 160
fig_dpi = 100

from apyfr.maps import blank_map
#from apyfr.maps import available_kwargs
from apyfr.maps import L2E_Proj
from matplotlib import colors
from matplotlib import colorbar

L2E = L2E_Proj()

ax = blank_map(applis=applis,extent=extent,zone=None,syrah=None,
                       draw_colorbar=True)

for app in range(len(applis)):  # for each application

    appli = applis[app].split('_')[1]

    values = np.nanmean(q_riv[applis[app]], 0) * 365.25  # simulated mean streamflow in m3/yr
    # to display we need to normalized a bit simulated streamflow
    norm_values = (np.where(values < np.percentile(values, 5), np.percentile(values, 5),
                           np.where(values > np.percentile(values,80), np.percentile(values, 80), values)))
    norm_values = 10 * ((norm_values - np.min(norm_values)) / (np.max(norm_values) - np.min(norm_values)))

    Inan = np.isfinite(values)
    #values[~Inan] = 0
    values = np.where(Inan, values, 0)
    values[values == 0] = np.nan

    vlim = np.nanmin(values), np.nanmax(values)

    cmap  = plt.cm.viridis
    norm_plot = colors.Normalize(vmin=vlim[0],vmax=vlim[1])

    hnan = ax.scatter(coord[applis[app]][~Inan,0], coord[applis[app]][~Inan,1], 10, [.5,.5,.5],
                      marker='s', lw=0, transform=L2E)
    # Display rivers (size in function of simulated mean streamflow)
    h = ax.scatter(coord[applis[app]][Inan,0], coord[applis[app]][Inan,1], norm_values, values[Inan], marker='s',
                   lw=0, transform=L2E, cmap=cmap, norm=norm_plot)
    # Display river stations
    h_stations = ax.scatter(coord[applis[app]][index_stations[appli],0], coord[applis[app]][index_stations[appli],1], 10, 'r', marker='o',
                   lw=0, transform=L2E, cmap=cmap, norm=norm_plot)
#h = ax.scatter(coord_q_deb[:,0],coord_q_deb[:,1],5, values,marker='s',lw=0,transform=L2E,cmap=cmap,norm=norm)

figw,figh = ax.figure.get_size_inches()*ax.figure.get_dpi()
axw,axh = figw*ax.get_position().width,figh*ax.get_position().height
cbar_ax = plt.axes([(fig_margin[0]+axw+20)/figw,fig_margin[2]/figh,30/figw,axh/figh])
cb = colorbar.ColorbarBase(cbar_ax,cmap=cmap,norm=norm_plot,spacing='uniform',extend='both')
cb.ax.tick_params(labelsize=10,size=0)
title_colorbar = 'Streamflow [m3/yr]'
cb.set_label(title_colorbar, fontsize=14)

# saving figure
plt.savefig('{0}/map_riverstations.png'.format(name_output_folder))

plt.show()















#===============================================================================

## Alternatively, map_applis can be used directely
#applis_data = {appli:EXP.data[var][appli] for appli in default_map_applis}
#title = 'all applis from '+exp+' on '+time_step
#map_applis(applis_data,extent=extent,vlim=vlim,cmap='spli',syrah=True,title=title,logo=False)


