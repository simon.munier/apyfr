#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  3 09:33:38 2023
Récupération des observations d'Adès sur la plateforme hub'eau : téléchargement et mise à jour
@author: Alexis Jeantet, à partir des codes d'André Mounier et des apports de Luca Guillaumot'
"""
#%% Extraction des packages d'intérêt
import requests
import warnings
import pandas as pd
import json
import os
import numpy as np
from glob import glob
from tqdm import tqdm

from .misc import apyfr_path, to_date, days_in_month

# Le répertoire par défaut des observations piézométriques peut être changé
piezos_dir = apyfr_path+"/data_for_apyfr/Donnees_piezo/"
if not os.path.exists(piezos_dir):
    os.makedirs(piezos_dir)
    print('Le dossier de téléchargement des observations piézométriques a été créé:')
    print(piezos_dir)
    print('Vous pouvez le changer en modifiant le fichier apyfr/piezometre.py')


warnings.filterwarnings(action='ignore', message='Mean of empty slice')

def create_liste_piezos():
    """
    Création du fichier "liste_piezos.csv"
    Nécessite la plateforme AquiFR
    """
    raw_data_path = os.environ['AQUIFRDIR']+'/Data/piezos/'
    files = glob(raw_data_path+'piezos_*_L2E')
    df_piezos = pd.DataFrame()
    for f in files:
        app = f.split('_')[1]
        data_array    = np.genfromtxt(f,dtype=str,delimiter = ',',autostrip=True)
        multi_index   = [(app,code.replace('/','_')) for code in data_array[:,0]]
        dataset       = pd.DataFrame(data=data_array[:,1:],columns=["Maille","x_L2E","y_L2E","Aquifere"],
                                     index=pd.MultiIndex.from_tuples(multi_index,names=["Application","Code"]))
        df_piezos = df_piezos.append(dataset)
    df_piezos = df_piezos[['Maille','Aquifere','x_L2E','y_L2E']]
    df_piezos.to_csv(apyfr_path+'/data/liste_piezos.csv')
    return


#   Extraire l'URL d'une station précise
def get_URL(pzo, start_date, stop_date):
    """
    Obtention de l'URL de téléchargement via l'API de hub'eau.

    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    start_date : Date
        Date minimale des données demandées.
    stop_date : Date
        Date maximale des données demandées.

    Returns
    -------
    URL_data : Chaîne de caractères
        URL de téléchargement.

    """
    pzo = pzo.replace('_','%2F')
    URL_data_uncompleted = 'http://hubeau.eaufrance.fr/api/v1/niveaux_nappes/chroniques?'+\
                           'code_bss={}&date_debut_mesure={}&date_fin_mesure={}&size={}'
    URL_data = URL_data_uncompleted.format(pzo, start_date.strftime('%Y-%m-%d'),
                                           stop_date.strftime('%Y-%m-%d'),1)
    analyzes_number = json.loads(requests.get(URL_data).text)['count']
    if analyzes_number > 0:
        URL_data  = URL_data_uncompleted.format(pzo, start_date.strftime('%Y-%m-%d'),
                                                stop_date.strftime('%Y-%m-%d'), analyzes_number+1)
        return URL_data
    else:
        print('\nStation {} : Pas de données'.format(pzo.replace('%2F','_')))
        return

#   Fonction pour exclure certaines stations qui ne fonctionnent pas
def is_available(pzo):
    """
    Gestion des quelques piézomètres non disponibles sur hub'eau.
    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").

    Returns
    -------
    Booléen
        Vrai si les données du piézomètre sont téléchargeables, faux sinon.
    """
    not_in_hubeau = ['000000_TOURY','00663X0124_F_2008','00633X0132_FRPAC','00331X0051_FSEP',
                     '00197X0279_PZ2','0000000_TOURY','00624X0085_FR296','00205X0042_F1','07092X077_F']
    # not_in_hubeau   = ['000000_TOURY','00663X0124_F',     '00633X0132_FRPAC','00331X0051_FSEP',
    #                    '00197X0279_PZ2','0000000_TOURY','00624X0085_FR296','00205X0042_F1','07092X077_F']
    return not pzo in not_in_hubeau


#   Télécharger la chronique associée à une station précise, avec son URL
def download_chronicle(pzo, start_date=to_date('1899-01-01'), stop_date=pd.Timestamp.today(),
                       update=True, infos=False):
    """
    Création du fichier csv relatif à un piézomètre.
    Parameters
    ----------
    pzo : Chaîne de caractères
        Code du piézomètre (avec un "_").
    start_date : Date, optionnel
        Date minimale des données demandées. Par défaut : to_date('1899-01-01').
    stop_date : Date, optionnel
        Date maximale des données demandées. Par défaut : pd.Timestamp.today().
    update : Booléen, optionnel
        Choisit le mode, si update ou téléchargement complet. Par défaut : True.
    infos : Booléen, optionnel
        Affiche les nouvelles mesures trouvées si Vrai. Par défaut : False.
    piezos_dir : Chaine de caractère ou None
        Chemin de stockage des données observées téléchargées

    Returns
    -------
    None, il s'agit d'une fonction dédiée au téléchargement uniquement
    """

    if not is_available(pzo):
        print("{} : Station non disponible".format(pzo))
        return

    data_path = piezos_dir+'Hauteur/'
    if not os.path.exists(data_path): os.makedirs(data_path)

    # Préparation de l'update : actualisation des dates de début et de fin
    if update and os.path.exists(data_path+pzo+".csv"):
        df_exist   = pd.read_csv(data_path+pzo+'.csv',index_col=0,parse_dates=True)
        start_date = df_exist.index[-1]
        stop_date  = pd.Timestamp.today()

    # Boucle sur les dates pour extraire les piezos
    URL  = get_URL(pzo, start_date, stop_date)
    data = json.loads(requests.get(URL).text)['data']
    index,hauteur = np.zeros(len(data),dtype=pd.Timestamp),np.zeros(len(data))
    for i,mesure in enumerate(data):
        index[i] = pd.to_datetime(mesure['date_mesure'])
        hauteur[i] = mesure['niveau_nappe_eau']
    df = pd.DataFrame(data=hauteur,index=index,columns=['Hauteur'])
    df.index.name = 'Date'

    # Si "update", on complète avec les données précédentes
    if update and os.path.exists(data_path+pzo+".csv"):
        if df.index[-1] != start_date:
            if infos: print('Nouvelles mesures :\n', df[1:])
            df = df_exist.append(df)
            df = df[~df.index.duplicated(keep='first')]
        else: # Cas de figure où il n'y a pas de valeurs supplémentaires
            return

    # Sauvegarde de la chronique complète
    df.to_csv(data_path + pzo + '.csv')

    return


def download_all(list_pzo=None, **kwargs):
    """
    Appel de la fonction 'download_chronicle' pour tous les piézomètres d'AquiFR. C'est long...

    Parameters
    ----------
    list_pzo : liste de chaine de caractère ou None
        Liste des piezomètres qu'il faut télécharger. None par défaut pour la liste complète.

    Returns
    -------
    None.

    """

    df_piezos = pd.read_csv(apyfr_path+'/data/liste_piezos.csv',index_col=[0,1])
    if list_pzo is None:
        list_pzo = list(set(df_piezos.index.get_level_values(1).to_list()))
    elif type(list_pzo) is not list or type(list_pzo[0]) is not str:
        raise Exception("'list_pzo' doit être une liste de codes de piezometres")
    elif not all(np.isin(list_pzo,df_piezos.index.get_level_values(1).to_list())):
        raise Exception(pzo+" n'est pas dans la liste des piezos étudiés")

    pzos_issues = []
    for pzo in tqdm(list_pzo):
        try:
            download_chronicle(pzo, piezos_dir=piezos_dir, **kwargs)
        except:
            print("Data du piezo "+pzo+" ne peuvent pas être téléchargées")
            pzos_issues.append(pzo)

    if pzos_issues != []:
        pzos_issues = pd.Series(pzos_issues)
        pzos_issues = pd.DataFrame(pzos_issues, columns = ['Code'])
        pzos_issues.to_csv(piezos_dir+'Piezos_problemes_telechargement.csv')
    return


def read_piezos(pzo_list, date_range=['1899-01-01','now'], dt="mon",
                force=False, pmin=0, **kwargs):

    """
    Accès à la chronique d'un piézomètre

    Parameters
    ----------
    pzo_list : Chaîne de caractères ou liste de str
        Code du/des piézomètre(s) (avec un "_").
    date_range : Liste, optionnel
        Liste des dates minimale et maximale de la chronique. Par défaut : ['1899-01-01','now'].
    dt : Chaîne de caractère
        Désigne si nous sommes en journalier ("day") ou en mensuel ("mon").
    force : Booléen, optionnel
        Force le recalcul de la moyenne si Vrai, sinon, utilise le fichier enregistré
        (accélération du temps de calcul). Par défaut : False.
    pmin : Réel, optionnel
        Pourcentage minimum de valeurs non NaN dans le mois (entre 0 et 100)

    Returns
    -------
    df_pzo : Dataframe
        Dataframe des dates de mesures et des hauteurs d'eau correspondantes.

    """

    if isinstance(pzo_list,str): pzo_list = [pzo_list]
    date_range = to_date(date_range)

    if dt=="day":
        dat = pd.date_range(*date_range)
    else:
        dat_range_day = [date_range[0].replace(day=1),date_range[-1].replace(day=days_in_month(date_range[-1]))]
        dat = pd.date_range(*dat_range_day,freq='1M')
        dat = pd.DatetimeIndex([d.replace(day=15) for d in dat])
    df_pzo = pd.DataFrame(index=dat,columns=pzo_list,dtype='f4')
    for pzo in tqdm(pzo_list):
        file_name = pzo+'.csv'
        file_name_jour = piezos_dir+'Hauteur/'+file_name
        if dt == "day":
            if not os.path.isfile(file_name_jour): continue
            df = pd.read_csv(file_name_jour,index_col=0,parse_dates=True)
        elif dt == 'mon':
            folder_save = piezos_dir+'Hauteur_mensuelle_{:.2f}p/'.format(pmin/100)
            if not os.path.exists(folder_save): os.makedirs(folder_save)
            if file_name not in os.listdir(folder_save) or force:
                if not os.path.isfile(file_name_jour): continue
                df_jour = pd.read_csv(file_name_jour,index_col=0,parse_dates=True)
                dat_range_day = [df_jour.index[0].replace(day=1),
                                 df_jour.index[-1].replace(day=days_in_month(df_jour.index[-1]))]
                df_jour = df_jour.reindex(pd.date_range(*dat_range_day))
                def resampler(data):
                    return np.nanmean(data) if (~np.isnan(data)).sum()/len(data)>=pmin/100 else np.nan
                df = df_jour.resample('1M').apply(resampler)
                df.index = pd.DatetimeIndex([d.replace(day=15) for d in df.index])
                df.index.name = 'Date'
                df.to_csv(folder_save+file_name)
            else:
                df = pd.read_csv(folder_save+file_name,index_col=0,parse_dates=True)
        df_pzo[pzo] = df.reindex(dat)

    return df_pzo
